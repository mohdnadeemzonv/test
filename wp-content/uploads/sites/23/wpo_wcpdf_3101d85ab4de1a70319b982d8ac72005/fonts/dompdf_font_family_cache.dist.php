<?php return array (
  'sans-serif' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica-Oblique',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Roman',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Italic',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Roman',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Italic',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-BoldItalic',
  ),
  'courier' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-Oblique',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica-Oblique',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/ZapfDingbats',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/ZapfDingbats',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/ZapfDingbats',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/ZapfDingbats',
  ),
  'symbol' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Symbol',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Symbol',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Symbol',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Symbol',
  ),
  'serif' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Roman',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-Italic',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-Oblique',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-Oblique',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => 
  array (
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSans-Oblique',
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => 
  array (
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => 
  array (
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSerif-Italic',
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/DejaVuSerif',
  ),
  'open sans' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/OpenSans-Normal',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/OpenSans-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/OpenSans-Italic',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/OpenSans-BoldItalic',
  ),
  'segoe' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Segoe-Normal',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Segoe-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Segoe-Italic',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/Segoe-BoldItalic',
  ),
  'roboto slab' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/RobotoSlab-Normal',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/RobotoSlab-Bold',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/RobotoSlab-Italic',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/RobotoSlab-BoldItalic',
  ),
  'currencies' => 
  array (
    'normal' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/currencies',
    'bold' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/currencies',
    'italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/currencies',
    'bold_italic' => '/bitnami/wordpress/wp-content/plugins/woocommerce-pdf-invoices-packing-slips/vendor/dompdf/dompdf/lib/fonts/currencies',
  ),
);
?>