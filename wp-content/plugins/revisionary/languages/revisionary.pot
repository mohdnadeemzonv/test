#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PublishPress Revisions Pro\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-23 13:22-0400\n"
"PO-Revision-Date: \n"
"Last-Translator: Kevin Behrens <kevin@publishpress.com>\n"
"Language-Team: PublishPress <help@publishpress.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_c;__ngettext;_n;rvy_po_trigger;__awp\n"
"X-Poedit-Basepath: ..\n"
"Language: en_US\n"
"X-Generator: Poedit 2.4.3\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPath-1: admin\n"
"X-Poedit-SearchPath-2: classes\n"
"X-Poedit-SearchPath-3: hardway\n"

#: admin/PostEditSubmitMetabox.php:62
#, php-format
msgid "Revision Edits: %s"
msgstr ""

#: admin/PostEditSubmitMetabox.php:62
#, php-format
msgid "Revisions: %s"
msgstr ""

#: admin/PostEditSubmitMetabox.php:65
#, php-format
msgid "Your site is configured to keep only the last %s revisions."
msgstr ""

#: admin/PostEditSubmitMetabox.php:103 admin/class-list-table_rvy.php:800
msgid "Delete Permanently"
msgstr ""

#: admin/PostEditSubmitMetabox.php:105
msgid "Move to Trash"
msgstr ""

#: admin/PostEditSubmitMetabox.php:136 admin/edit-revision-ui_rvy.php:16
#: admin/edit-revision-ui_rvy.php:17 admin/edit-revision-ui_rvy.php:21
#: admin/edit-revision-ui_rvy.php:22 admin/post-edit-block-ui_rvy.php:117
#: rvy_init.php:354 rvy_init.php:365
msgid "Update Revision"
msgstr ""

#: admin/PostEditSubmitMetabox.php:142
#, php-format
msgid "Save as %s"
msgstr ""

#: admin/PostEditSubmitMetabox.php:180 admin/post-edit-block-ui_rvy.php:78
#: admin/post-edit_rvy.php:120 admin/post-edit_rvy.php:285
msgid "View / Publish"
msgstr ""

#: admin/PostEditSubmitMetabox.php:180 admin/history_rvy.php:1094
#: admin/post-edit-block-ui_rvy.php:78 admin/post-edit_rvy.php:120
#: admin/post-edit_rvy.php:285
msgid "View / Approve"
msgstr ""

#: admin/PostEditSubmitMetabox.php:181 admin/post-edit-block-ui_rvy.php:79
#: admin/post-edit_rvy.php:121 admin/post-edit_rvy.php:304
msgid "View / moderate saved revision"
msgstr ""

#: admin/PostEditSubmitMetabox.php:183 admin/class-list-table_rvy.php:516
#: admin/history_rvy.php:1096 admin/post-edit-block-ui_rvy.php:81
#: admin/post-edit_rvy.php:123 admin/post-edit_rvy.php:127
#: admin/post-edit_rvy.php:287
msgid "View"
msgstr ""

#: admin/PostEditSubmitMetabox.php:184 admin/post-edit-block-ui_rvy.php:82
#: admin/post-edit_rvy.php:124 admin/post-edit_rvy.php:306
msgid "View saved revision"
msgstr ""

#: admin/PostEditSubmitMetabox.php:196
msgid "Preview Changes"
msgstr ""

#: admin/PostEditSubmitMetabox.php:220
msgid "Status:"
msgstr ""

#: admin/PostEditSubmitMetabox.php:244 admin/PostEditSubmitMetabox.php:320
#: admin/PostEditSubmitMetabox.php:372 admin/admin_rvy.php:530
#: admin/class-list-table_rvy.php:492 admin/class-list-table_rvy.php:1111
#: admin/history_rvy.php:1138 front_rvy.php:226
msgid "Edit"
msgstr ""

#: admin/PostEditSubmitMetabox.php:262 admin/PostEditSubmitMetabox.php:329
msgid "OK"
msgstr ""

#: admin/PostEditSubmitMetabox.php:263 admin/PostEditSubmitMetabox.php:330
msgid "Cancel"
msgstr ""

#: admin/PostEditSubmitMetabox.php:280
msgid "Visibility:"
msgstr ""

#: admin/PostEditSubmitMetabox.php:298
msgid "Public, Sticky"
msgstr ""

#: admin/PostEditSubmitMetabox.php:300 admin/PostEditSubmitMetabox.php:313
#: admin/PostEditSubmitMetabox.php:326
msgid "Public"
msgstr ""

#: admin/PostEditSubmitMetabox.php:307
#, php-format
msgid "%s, Sticky"
msgstr ""

#: admin/PostEditSubmitMetabox.php:350
msgid "M j, Y @ G:i"
msgstr ""

#: admin/PostEditSubmitMetabox.php:356 admin/admin_rvy.php:511
#, php-format
msgid "Scheduled for: %s"
msgstr ""

#: admin/PostEditSubmitMetabox.php:358 admin/admin_rvy.php:513
#, php-format
msgid "Published on: %s"
msgstr ""

#: admin/PostEditSubmitMetabox.php:360 admin/PostEditSubmitMetabox.php:368
#: admin/admin_rvy.php:515 admin/admin_rvy.php:523
msgid "Publish <b>immediately</b>"
msgstr ""

#: admin/PostEditSubmitMetabox.php:362 admin/admin_rvy.php:517
#, php-format
msgid "Schedule for: %s"
msgstr ""

#: admin/PostEditSubmitMetabox.php:364 admin/admin_rvy.php:519
#, php-format
msgid "Publish on: %s"
msgstr ""

#: admin/admin-dashboard_rvy.php:28
#, php-format
msgid "%1$s %2$s Revision"
msgstr ""

#: admin/admin-dashboard_rvy.php:30
#, php-format
msgid "%1$s %2$s Revisions"
msgstr ""

#: admin/admin-dashboard_rvy.php:37
#, php-format
msgid "View %s"
msgstr ""

#: admin/admin-init_rvy.php:117 admin/admin-init_rvy.php:160
msgid "Sorry, you are not allowed to approve this revision."
msgstr ""

#: admin/admin-init_rvy.php:189
msgid "Sorry, you are not allowed to delete this revision."
msgstr ""

#: admin/admin-init_rvy.php:194
msgid "Error in deleting."
msgstr ""

#: admin/admin-init_rvy.php:266
#, php-format
msgid "For more details on setting up PublishPress Revisions, %sread this guide%s."
msgstr ""

#: admin/admin-init_rvy.php:272
#, php-format
msgid "<strong>Welcome to PublishPress Revisions!</strong> Here&apos;s how it works:%s<li>\"Contributors\" can submit revisions to their published posts.</li><li>\"Revisors\" can submit revisions to posts and pages published by others.</li><li>\"Authors\", \"Editors\" and \"Administrators\" can approve revisions or schedule their own revisions.</li>%s%s%s"
msgstr ""

#: admin/admin-init_rvy.php:282
#, php-format
msgid "<strong>Revisionary is now PublishPress Revisions!</strong> Note the new Revisions menu and %sRevision Queue%s screen, where Pending and Scheduled Revisions are listed. %s"
msgstr ""

#: admin/admin-init_rvy.php:308
msgid "Dismiss"
msgstr ""

#: admin/admin_lib-mu_rvy.php:12 admin/options.php:209
msgid "PublishPress Revisions Network Settings"
msgstr ""

#: admin/admin_lib-mu_rvy.php:12
msgid "Network Settings"
msgstr ""

#: admin/admin_lib-mu_rvy.php:23 admin/options.php:211
msgid "PublishPress Revisions Network Defaults"
msgstr ""

#: admin/admin_lib-mu_rvy.php:23
msgid "Network Defaults"
msgstr ""

#: admin/admin_rvy.php:329 admin/post-edit-block-ui_rvy.php:157
#: rvy_init.php:353
msgid "Pending Revision"
msgstr ""

#: admin/admin_rvy.php:346
msgid "Has Revision"
msgstr ""

#: admin/admin_rvy.php:360 admin/admin_rvy.php:725 admin/options.php:101
msgid "Revision Queue"
msgstr ""

#: admin/admin_rvy.php:368
msgid "The revision was restored."
msgstr ""

#: admin/admin_rvy.php:371
msgid "The revision was scheduled for publication."
msgstr ""

#: admin/admin_rvy.php:374
msgid "The revision was published."
msgstr ""

#: admin/admin_rvy.php:508 admin/history_rvy.php:962
#: admin/revision-action_rvy.php:166 admin/revision-action_rvy.php:248
#: admin/revision-ui_rvy.php:285 front_rvy.php:172
msgid "M j, Y @ g:i a"
msgstr ""

#: admin/admin_rvy.php:553
msgid "Save as Pending Revision"
msgstr ""

#: admin/admin_rvy.php:556 admin/post-edit-block-ui_rvy.php:158
msgid "Do not publish current changes yet, but save to Revision Queue"
msgstr ""

#: admin/admin_rvy.php:576 admin/admin_rvy.php:739 admin/options.php:94
msgid "Settings"
msgstr ""

#: admin/admin_rvy.php:634
msgid "Enable public preview"
msgstr ""

#: admin/admin_rvy.php:652 admin/filters-admin-ui-item_rvy.php:134
msgid "Current Time"
msgstr ""

#: admin/admin_rvy.php:706 admin/admin_rvy.php:722
msgid "Revisions"
msgstr ""

#: admin/admin_rvy.php:739 admin/options.php:215
msgid "PublishPress Revisions Settings"
msgstr ""

#: admin/admin_rvy.php:746 admin/admin_rvy.php:747
msgid "Upgrade to Pro"
msgstr ""

#: admin/admin_rvy.php:836 admin/class-list-table_rvy.php:1121
#: admin/revision-ui_rvy.php:305 admin/revision-ui_rvy.php:418
msgid "Delete"
msgstr ""

#: admin/admin_rvy.php:847
#, php-format
msgid "%s (revision)"
msgstr ""

#: admin/admin_rvy.php:881
#, php-format
msgid "If you like %s, please leave us a %s rating. Thank you!"
msgstr ""

#: admin/admin_rvy.php:892
msgid "About PublishPress Revisions"
msgstr ""

#: admin/admin_rvy.php:892
msgid "About"
msgstr ""

#: admin/admin_rvy.php:894
msgid "PublishPress Revisions Documentation"
msgstr ""

#: admin/admin_rvy.php:894 admin/options.php:748
msgid "Documentation"
msgstr ""

#: admin/admin_rvy.php:896
msgid "Contact the PublishPress team"
msgstr ""

#: admin/admin_rvy.php:896
msgid "Contact"
msgstr ""

#: admin/agents_checklist_rvy.php:43
msgid "Enter additional User Names or IDs (comma-separate)"
msgstr ""

#: admin/agents_checklist_rvy.php:98
#, php-format
msgid "show current users (%d)"
msgstr ""

#: admin/agents_checklist_rvy.php:98
#, php-format
msgid "show eligible users (%d)"
msgstr ""

#: admin/agents_checklist_rvy.php:116
msgid "filter:"
msgstr ""

#: admin/agents_checklist_rvy.php:125
msgid "select"
msgstr ""

#: admin/agents_checklist_rvy.php:130
msgid "unselect"
msgstr ""

#: admin/class-list-table_rvy.php:335
msgid "Revision"
msgstr ""

#: admin/class-list-table_rvy.php:336
msgid "Status"
msgstr ""

#: admin/class-list-table_rvy.php:337
msgid "Post Type"
msgstr ""

#: admin/class-list-table_rvy.php:338
msgid "Revised By"
msgstr ""

#: admin/class-list-table_rvy.php:339
msgid "Submission"
msgstr ""

#: admin/class-list-table_rvy.php:347
msgid "Schedule"
msgstr ""

#: admin/class-list-table_rvy.php:350
msgid "Published Post"
msgstr ""

#: admin/class-list-table_rvy.php:352
msgid "Post Author"
msgstr ""

#: admin/class-list-table_rvy.php:376 admin/revisions.php:64 rvy_init.php:354
msgid "Pending"
msgstr ""

#: admin/class-list-table_rvy.php:379 admin/revisions.php:65 rvy_init.php:365
msgid "Scheduled"
msgstr ""

#: admin/class-list-table_rvy.php:395 admin/class-list-table_rvy.php:1039
msgid "Y/m/d g:i:s a"
msgstr ""

#: admin/class-list-table_rvy.php:403 admin/class-list-table_rvy.php:1044
#: admin/history_rvy.php:1009
#, php-format
msgid "%s ago"
msgstr ""

#: admin/class-list-table_rvy.php:406 admin/class-list-table_rvy.php:1047
msgid "Y/m/d g:i a"
msgstr ""

#: admin/class-list-table_rvy.php:413
#, php-format
msgid "Scheduled publication: %s"
msgstr ""

#: admin/class-list-table_rvy.php:416
#, php-format
msgid "Requested publication: %s"
msgstr ""

#: admin/class-list-table_rvy.php:420
msgid "Missed schedule"
msgstr ""

#: admin/class-list-table_rvy.php:463 admin/history_rvy.php:807
msgid "No author"
msgstr ""

#: admin/class-list-table_rvy.php:503
#, php-format
msgid "View only revisions of %s"
msgstr ""

#: admin/class-list-table_rvy.php:504 admin/class-list-table_rvy.php:842
msgid "Filter"
msgstr ""

#: admin/class-list-table_rvy.php:515 admin/class-list-table_rvy.php:523
msgid "View published post"
msgstr ""

#: admin/class-list-table_rvy.php:524 admin/class-list-table_rvy.php:1137
#: admin/history_rvy.php:1191 admin/revision-ui_rvy.php:295
#: admin/revision-ui_rvy.php:317
msgid "Preview"
msgstr ""

#: admin/class-list-table_rvy.php:547
msgid "Compare Past Revisions"
msgstr ""

#: admin/class-list-table_rvy.php:548
msgid "History"
msgstr ""

#: admin/class-list-table_rvy.php:700
#, php-format
msgid "%sMy Revisions%s(%s)"
msgstr ""

#: admin/class-list-table_rvy.php:732
#, php-format
msgid "%sMy Published Posts%s(%s)"
msgstr ""

#: admin/class-list-table_rvy.php:767
#, php-format
msgid "All %s"
msgstr ""

#: admin/class-list-table_rvy.php:792 admin/history_rvy.php:1094
#: admin/post-edit_rvy.php:237 front_rvy.php:260
msgid "Approve"
msgstr ""

#: admin/class-list-table_rvy.php:793
msgid "Publish"
msgstr ""

#: admin/class-list-table_rvy.php:796 admin/revision-ui_rvy.php:300
msgid "Unschedule"
msgstr ""

#: admin/class-list-table_rvy.php:821
msgid "Filter by category"
msgstr ""

#: admin/class-list-table_rvy.php:884
msgid "Filter by date"
msgstr ""

#: admin/class-list-table_rvy.php:886
msgid "All dates"
msgstr ""

#: admin/class-list-table_rvy.php:945
msgid "Select All"
msgstr ""

#: admin/class-list-table_rvy.php:1027
#, php-format
msgid "&#8220;%s&#8221; (Edit)"
msgstr ""

#: admin/class-list-table_rvy.php:1120
msgid "Delete Revision"
msgstr ""

#: admin/class-list-table_rvy.php:1136
msgid "Preview Revision"
msgstr ""

#: admin/class-list-table_rvy.php:1148
msgid "Compare Changes"
msgstr ""

#: admin/filters-admin-ui-item_rvy.php:36 admin/post-edit-block-ui_rvy.php:153
msgid "Schedule Revision"
msgstr ""

#: admin/filters-admin-ui-item_rvy.php:118
#: admin/filters-admin-ui-item_rvy.php:121
#: admin/filters-admin-ui-item_rvy.php:127
#: admin/filters-admin-ui-item_rvy.php:128 admin/post-edit-block-ui_rvy.php:218
#: admin/post-edit-block-ui_rvy.php:219
msgid "Submit Revision"
msgstr ""

#: admin/filters-admin-ui-item_rvy.php:122
msgid "Submit Scheduled Revision"
msgstr ""

#: admin/filters-admin-ui-item_rvy.php:168 admin/options.php:100
#: admin/revisions.php:219 rvy_init.php:354
msgid "Pending Revisions"
msgstr ""

#: admin/filters-admin-ui-item_rvy.php:174
msgid "Publishers to Notify of Your Revision"
msgstr ""

#: admin/filters-admin-ui-item_rvy.php:181 admin/options.php:99
#: admin/revisions.php:222 rvy_init.php:365
msgid "Scheduled Revisions"
msgstr ""

#: admin/history_rvy.php:158
#, php-format
msgid "Compare %s of &#8220;%s&#8221;"
msgstr ""

#: admin/history_rvy.php:159
msgid "&larr; Return to editor"
msgstr ""

#: admin/history_rvy.php:470 admin/history_rvy.php:473
msgid "(no title)"
msgstr ""

#: admin/history_rvy.php:561 admin/options.php:512
msgid "Post Date"
msgstr ""

#: admin/history_rvy.php:562
msgid "Post Parent"
msgstr ""

#: admin/history_rvy.php:563
msgid "Menu Order"
msgstr ""

#: admin/history_rvy.php:564
msgid "Comment Status"
msgstr ""

#: admin/history_rvy.php:565
msgid "Ping Status"
msgstr ""

#: admin/history_rvy.php:690
msgid "Page Template"
msgstr ""

#: admin/history_rvy.php:693
msgid "Featured Image"
msgstr ""

#: admin/history_rvy.php:702
msgid "Beaver Builder Data"
msgstr ""

#: admin/history_rvy.php:703
msgid "Beaver Builder Settings"
msgstr ""

#: admin/history_rvy.php:936
msgid "Scheduled for "
msgstr ""

#: admin/history_rvy.php:941
msgid "Requested for "
msgstr ""

#: admin/history_rvy.php:946
msgid "Submitted "
msgstr ""

#: admin/history_rvy.php:951
#, php-format
msgid "%s%s ago"
msgstr ""

#: admin/history_rvy.php:951
#, php-format
msgid "%s%s from now"
msgstr ""

#: admin/history_rvy.php:1007
msgid "M j, Y @ H:i"
msgstr ""

#: admin/history_rvy.php:1190
msgid "Preview / Restore"
msgstr ""

#: admin/history_rvy.php:1197
msgid "Manage"
msgstr ""

#: admin/history_rvy.php:1198
msgid "List"
msgstr ""

#: admin/options.php:81 submittee_rvy.php:11 submittee_rvy.php:14
msgid "Cheatin&#8217; uh?"
msgstr ""

#: admin/options.php:94
msgid "Setting Scope"
msgstr ""

#: admin/options.php:98
msgid "Role Definition"
msgstr ""

#: admin/options.php:102
msgid "Preview / Approval"
msgstr ""

#: admin/options.php:103
msgid "Revision Options"
msgstr ""

#: admin/options.php:104
msgid "Email Notification"
msgstr ""

#: admin/options.php:111
msgid "Enable Pending Revisions"
msgstr ""

#: admin/options.php:112
msgid "Enable Scheduled Revisions"
msgstr ""

#: admin/options.php:113
msgid "Editing others&apos; revisions requires role capability"
msgstr ""

#: admin/options.php:114
msgid "Listing others&apos; revisions requires role capability"
msgstr ""

#: admin/options.php:115
msgid "Compatibility Mode"
msgstr ""

#: admin/options.php:116
msgid "Confirmation redirect on Revision Update"
msgstr ""

#: admin/options.php:117
msgid "Also notify on Revision Update"
msgstr ""

#: admin/options.php:118
msgid "Revision Publication: API actions to mimic Post Update"
msgstr ""

#: admin/options.php:119
msgid "Hide html tags on Compare Revisions screen"
msgstr ""

#: admin/options.php:120
msgid "Asynchronous Publishing"
msgstr ""

#: admin/options.php:121 admin/options.php:122
msgid "Update Publish Date"
msgstr ""

#: admin/options.php:123 admin/options.php:124
msgid "Update Modified Date"
msgstr ""

#: admin/options.php:125
msgid "Email original Author when a Pending Revision is submitted"
msgstr ""

#: admin/options.php:126
msgid "Email the original Author when a Pending Revision is approved"
msgstr ""

#: admin/options.php:127
msgid "Email the Revisor when a Pending Revision is approved"
msgstr ""

#: admin/options.php:128
msgid "Email the original Author when a Scheduled Revision is published"
msgstr ""

#: admin/options.php:129
msgid "Email the Revisor when a Scheduled Revision is published"
msgstr ""

#: admin/options.php:130
msgid "Enable notification buffer"
msgstr ""

#: admin/options.php:131
msgid "All custom post types available to Revisors"
msgstr ""

#: admin/options.php:132
msgid "Prevent Revisors from editing other user&apos;s drafts"
msgstr ""

#: admin/options.php:133
msgid "Display Hints"
msgstr ""

#: admin/options.php:134
msgid "Show Preview Links"
msgstr ""

#: admin/options.php:135
msgid "Preview Link Type"
msgstr ""

#: admin/options.php:136
msgid "Approve Button on Compare Revisions screen"
msgstr ""

#: admin/options.php:137
msgid "Copy revision comments to published post"
msgstr ""

#: admin/options.php:138
msgid "Compare Past Revisions ordering:"
msgstr ""

#: admin/options.php:144
msgid "Email designated Publishers when a Pending Revision is submitted"
msgstr ""

#: admin/options.php:145
msgid "Email designated Publishers when a Scheduled Revision is published"
msgstr ""

#: admin/options.php:146
msgid "Email designated Publishers when a Pending Revision is approved"
msgstr ""

#: admin/options.php:148
msgid "Email Editors and Administrators when a Pending Revision is submitted"
msgstr ""

#: admin/options.php:149
msgid "Email Editors and Administrators when a Scheduled Revision is published"
msgstr ""

#: admin/options.php:150
msgid "Email Editors and Administrators when a Pending Revision is approved"
msgstr ""

#: admin/options.php:213
msgid "PublishPress Revisions Site Settings"
msgstr ""

#: admin/options.php:221 admin/options.php:866
msgid "Update &raquo;"
msgstr ""

#: admin/options.php:235
msgid "These are the <strong>default</strong> settings for options which can be adjusted per-site."
msgstr ""

#: admin/options.php:272
#, php-format
msgid "You can also specify %1$sdefaults for site-specific settings%2$s."
msgstr ""

#: admin/options.php:273
#, php-format
msgid "Use this tab to make <strong>NETWORK-WIDE</strong> changes to PublishPress Revisions settings. %s"
msgstr ""

#: admin/options.php:275
msgid "Here you can change the default value for settings which are controlled separately on each site."
msgstr ""

#: admin/options.php:289
#, php-format
msgid "Note that %1$s network-wide settings%2$s may also be available."
msgstr ""

#: admin/options.php:321
msgid "The user role \"Revisor\" role is now available. Include capabilities for all custom post types in this role?"
msgstr ""

#: admin/options.php:326
msgid "If checked, users lacking site-wide publishing capabilities will also be checked for the edit_others_drafts capability"
msgstr ""

#: admin/options.php:347
msgid "If a currently published post or page is edited and a future date set, the change will not be applied until the selected date."
msgstr ""

#: admin/options.php:350
msgid "When a scheduled revision is published, update post publish date to current time."
msgstr ""

#: admin/options.php:353
msgid "When a scheduled revision is published, update post modified date to current time."
msgstr ""

#: admin/options.php:356
msgid "Publish scheduled revisions asynchronously, via a secondary http request from the server.  This is usually best since it eliminates delay, but some servers may not support it."
msgstr ""

#: admin/options.php:374
#, php-format
msgid "Enable Contributors to submit revisions to their own published content. Revisors and users who have the edit_others (but not edit_published) capability for the post type can submit revisions to other user's content. These Pending Revisions are listed in %sRevision Queue%s."
msgstr ""

#: admin/options.php:380
msgid "When a pending revision is published, update post publish date to current time."
msgstr ""

#: admin/options.php:383
msgid "When a pending revision is published, update post modified date to current time."
msgstr ""

#: admin/options.php:404
msgid "This restriction applies to users who are not full editors for the post type. To enable a role, give it the edit_others_revisions capability."
msgstr ""

#: admin/options.php:407
msgid "This restriction applies to users who are not full editors for the post type. To enable a role, give it the list_others_revisions capability."
msgstr ""

#: admin/options.php:410
msgid "If some revisions are missing from the queue, disable a performance enhancement for better compatibility with themes and plugins."
msgstr ""

#: admin/options.php:415
msgid "Regenerate \"post has revision\" flags"
msgstr ""

#: admin/options.php:431
msgid "For themes that block revision preview, hide preview links from non-Administrators"
msgstr ""

#: admin/options.php:443
msgid "Published Post Slug"
msgstr ""

#: admin/options.php:443
msgid "Revision Slug"
msgstr ""

#: admin/options.php:443
msgid "Revision ID only"
msgstr ""

#: admin/options.php:454
msgid "Some themes or plugins may require Revision Slug or Revision ID link type for proper template loading and field display."
msgstr ""

#: admin/options.php:464
msgid "If disabled, Compare screen links to Revision Preview for approval"
msgstr ""

#: admin/options.php:484
#, php-format
msgid "For compatibility with Advanced Custom Fields, Beaver Builder and WPML, upgrade to <a href=\"%s\" target=\"_blank\">PublishPress Revisions Pro</a>."
msgstr ""

#: admin/options.php:493
msgid "This may improve compatibility with some plugins."
msgstr ""

#: admin/options.php:512
msgid "Modification Date"
msgstr ""

#: admin/options.php:522
msgid "Show descriptive captions for PublishPress Revisions settings"
msgstr ""

#: admin/options.php:542 admin/options.php:607
msgid "select recipients"
msgstr ""

#: admin/options.php:551 admin/options.php:569
msgid "Never"
msgstr ""

#: admin/options.php:551 admin/options.php:569
msgid "By default"
msgstr ""

#: admin/options.php:551 admin/options.php:569
msgid "Always"
msgstr ""

#: admin/options.php:624
msgid "Note: \"by default\" means Pending Revision creators can customize email notification recipients before submitting.  Eligibile \"Publisher\" email recipients are members of the Pending Revision Monitors group who <strong>also</strong> have the ability to publish the revision.  If not explicitly defined, the Monitors group is all users with a primary WP role of Administrator or Editor."
msgstr ""

#: admin/options.php:626
#, php-format
msgid "Note: \"by default\" means Pending Revision creators can customize email notification recipients before submitting.  For more flexibility in moderation and notification, install the %1$s PublishPress Permissions Pro%2$s plugin."
msgstr ""

#: admin/options.php:634
msgid "To avoid notification failures, buffer emails for delayed sending once minute, hour or day limits are exceeded"
msgstr ""

#: admin/options.php:651
msgid "Notification Buffer"
msgstr ""

#: admin/options.php:679
msgid "Notification Log"
msgstr ""

#: admin/options.php:708
msgid "Purge Notification Buffer"
msgstr ""

#: admin/options.php:714
msgid "Truncate Notification Log"
msgstr ""

#: admin/options.php:720
#, php-format
msgid "Sent in last minute: %d / %d"
msgstr ""

#: admin/options.php:721
#, php-format
msgid "Sent in last hour: %d / %d"
msgstr ""

#: admin/options.php:722
#, php-format
msgid "Sent in last day: %d / %d"
msgstr ""

#: admin/options.php:729
#, php-format
msgid "Seconds until next buffer processing time: %d"
msgstr ""

#: admin/options.php:738
msgid "Show Notification Log / Buffer"
msgstr ""

#: admin/options.php:740
msgid "Show with message content"
msgstr ""

#: admin/options.php:781
#, php-format
msgid "network-wide control of \"%s\""
msgstr ""

#: admin/options.php:788
msgid "Specify which PublishPress Revisions Settings to control network-wide. Unselected settings are controlled separately on each site."
msgstr ""

#: admin/options.php:870
msgid "All settings in this form (including those on unselected tabs) will be reset to DEFAULTS.  Are you sure?"
msgstr ""

#: admin/options.php:875
msgid "Revert to Defaults"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:90 admin/post-edit_rvy.php:128
msgid "View unsaved changes"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:94
#, php-format
msgid "<span class=\"dashicons dashicons-backup\"></span>&nbsp;%s Revision Edit"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:123
msgid "Approve Revision"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:126 admin/post-edit_rvy.php:237
msgid "Approve saved changes"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:154
msgid "Update"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:160
msgid "Do not schedule current changes yet, but save to Revision Queue"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:162 rvy_init.php:354 rvy_init.php:365
msgid "Save Revision"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:176
#, php-format
msgid "<span class=\"dashicons dashicons-edit\"></span>&nbsp;%s Pending Revision"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:187
#, php-format
msgid "<span class=\"dashicons dashicons-clock\"></span>&nbsp;%s Scheduled Revision"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:210
#, php-format
msgid "You've already submitted a revision%s"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:211
msgid "Edit the Revision"
msgstr ""

#: admin/post-edit-block-ui_rvy.php:220
msgid "Workflow&hellip;"
msgstr ""

#: admin/post-edit_rvy.php:42
#, php-format
msgid "You have already submitted a revision for this %s. %sEdit the revision%s."
msgstr ""

#: admin/post-edit_rvy.php:53
#, php-format
msgid "Revision updated. %sView Preview%s"
msgstr ""

#: admin/post-edit_rvy.php:55
msgid "Revision updated."
msgstr ""

#: admin/post-edit_rvy.php:133
#, php-format
msgid "Edit %s Revision"
msgstr ""

#: admin/post-edit_rvy.php:247
msgid "Compare this revision to published copy, or to other revisions"
msgstr ""

#: admin/post-edit_rvy.php:345
#, php-format
msgid "%sScheduled Revisions: %s"
msgstr ""

#: admin/post-edit_rvy.php:366
#, php-format
msgid "%sPending Revisions: %s"
msgstr ""

#: admin/revision-action_rvy.php:159 admin/revision-action_rvy.php:244
#, php-format
msgid "[%s] Revision Approval Notice"
msgstr ""

#: admin/revision-action_rvy.php:160
#, php-format
msgid "A revision to the %1$s \"%2$s\" has been approved."
msgstr ""

#: admin/revision-action_rvy.php:163
#, php-format
msgid "The submitter was %1$s."
msgstr ""

#: admin/revision-action_rvy.php:167 admin/revision-action_rvy.php:249
#, php-format
msgid "It will be published on %s"
msgstr ""

#: admin/revision-action_rvy.php:171 admin/revision-action_rvy.php:253
msgid "Preview it here: "
msgstr ""

#: admin/revision-action_rvy.php:174 admin/revision-action_rvy.php:256
msgid "Editor: "
msgstr ""

#: admin/revision-action_rvy.php:176 admin/revision-action_rvy.php:258
#: admin/revision-action_rvy.php:1000 admin/revision-action_rvy.php:1025
#: admin/revision-action_rvy.php:1094
msgid "View it online: "
msgstr ""

#: admin/revision-action_rvy.php:245
#, php-format
msgid "The revision you submitted for the %1$s \"%2$s\" has been approved."
msgstr ""

#: admin/revision-action_rvy.php:996 admin/revision-action_rvy.php:1018
#, php-format
msgid "[%s] Scheduled Revision Publication Notice"
msgstr ""

#: admin/revision-action_rvy.php:997
#, php-format
msgid "The scheduled revision you submitted for the %1$s \"%2$s\" has been published."
msgstr ""

#: admin/revision-action_rvy.php:1019
#, php-format
msgid "A scheduled revision to your %1$s \"%2$s\" has been published."
msgstr ""

#: admin/revision-action_rvy.php:1022 admin/revision-action_rvy.php:1091
#: revision-workflow_rvy.php:166
#, php-format
msgid "It was submitted by %1$s."
msgstr ""

#: admin/revision-action_rvy.php:1086
#, php-format
msgid "[%s] Scheduled Revision Publication"
msgstr ""

#: admin/revision-action_rvy.php:1088
#, php-format
msgid "A scheduled revision to the %1$s \"%2$s\" has been published."
msgstr ""

#: admin/revision-queue_rvy.php:9
msgid "You are not allowed to manage revisions."
msgstr ""

#: admin/revision-queue_rvy.php:13
msgid "Pending Revisions and Scheduled Revisions are both disabled. See Revisions > Settings."
msgstr ""

#: admin/revision-queue_rvy.php:40
#, php-format
msgid "%s revision approved."
msgstr ""

#: admin/revision-queue_rvy.php:41
#, php-format
msgid "%s revision unscheduled."
msgstr ""

#: admin/revision-queue_rvy.php:42
#, php-format
msgid "%s revision published."
msgstr ""

#: admin/revision-queue_rvy.php:43
#, php-format
msgid "%s revision permanently deleted."
msgstr ""

#: admin/revision-queue_rvy.php:105 admin/revision-queue_rvy.php:106
#, php-format
msgid "%sPost Author: %s"
msgstr ""

#: admin/revision-queue_rvy.php:115
#, php-format
msgid "Revision Queue %s"
msgstr ""

#: admin/revision-queue_rvy.php:121
#, php-format
msgid "Search results for &#8220;%s&#8221;"
msgstr ""

#: admin/revision-queue_rvy.php:134
msgid "Undo"
msgstr ""

#: admin/revision-ui_rvy.php:36
msgid "Publishers will be notified (but cannot be selected here)."
msgstr ""

#: admin/revision-ui_rvy.php:38
msgid "No email notifications will be sent."
msgstr ""

#: admin/revision-ui_rvy.php:110
#, php-format
msgid "%1$s (Current)"
msgstr ""

#: admin/revision-ui_rvy.php:114
#, php-format
msgid "%1$s (Autosave)"
msgstr ""

#: admin/revision-ui_rvy.php:124
#, php-format
msgid "%1$s <span class=\"rvy-revision-pubish-date\">(Requested publication: %2$s)</span>"
msgstr ""

#: admin/revision-ui_rvy.php:126
#, php-format
msgid "%1$s <span class=\"rvy-revision-pubish-date\">(Publish date: %2$s)</span>"
msgstr ""

#: admin/revision-ui_rvy.php:226
msgid "The revision will be deleted. Are you sure?"
msgstr ""

#: admin/revision-ui_rvy.php:295
#, php-format
msgid "Preview &#8220;%s&#8221;"
msgstr ""

#: admin/revision-ui_rvy.php:398
msgid "Modified Date"
msgstr ""

#: admin/revision-ui_rvy.php:401
msgid "Author"
msgstr ""

#: admin/revision-ui_rvy.php:402
msgid "Actions"
msgstr ""

#: admin/revision-ui_rvy.php:417
msgid "Bulk Actions"
msgstr ""

#: admin/revision-ui_rvy.php:420
msgid "Apply"
msgstr ""

#: admin/revisions.php:20
msgid "<strong>Note:</strong> For visual display of revisions, add the following code to foliopress-wysiwyg.php:<br />&nbsp;&nbsp;if ( strpos( $_SERVER['REQUEST_URI'], 'admin.php?page=rvy-revisions' ) ) return;"
msgstr ""

#: admin/revisions.php:57
msgid "No revision specified."
msgstr ""

#: admin/revisions.php:63
msgid "Past"
msgstr ""

#: admin/revisions.php:125
#, php-format
msgid "Revisions of %s"
msgstr ""

#: admin/revisions.php:151
msgid "The requested revision does not exist."
msgstr ""

#: admin/revisions.php:216
msgid "Past Revisions"
msgstr ""

#: admin/revisions.php:228
#, php-format
msgid "%1$s <span class=\"count\"> (%2$s)</span>"
msgstr ""

#: admin/revisions.php:243
#, php-format
msgid "no %s revisions available."
msgstr ""

#: front_rvy.php:201
#, php-format
msgid "%sCompare%s%sView&nbsp;Published&nbsp;Post%s"
msgstr ""

#: front_rvy.php:215
#, php-format
msgid "%sView&nbsp;Published&nbsp;Post%s"
msgstr ""

#: front_rvy.php:265
#, php-format
msgid "This is a Pending Revision (requested publish date: %s). %s %s %s"
msgstr ""

#: front_rvy.php:269 front_rvy.php:288
msgid "Publish now"
msgstr ""

#: front_rvy.php:271
#, php-format
msgid "This is a Pending Revision. %s %s %s"
msgstr ""

#: front_rvy.php:281
msgid "This revision is very new, preview may not be synchronized with theme."
msgstr ""

#: front_rvy.php:282
msgid "Reload"
msgstr ""

#: front_rvy.php:290
#, php-format
msgid "This is a Scheduled Revision (for publication on %s). %s %s %s"
msgstr ""

#: front_rvy.php:297
msgid "Restore"
msgstr ""

#: front_rvy.php:298
#, php-format
msgid "This is a Past Revision (from %s). %s %s"
msgstr ""

#: front_rvy.php:310
#, php-format
msgid "This is the Current Revision. %s"
msgstr ""

#: lib/debug.php:187
#, php-format
msgid "%1$s queries in %2$s seconds. %3$s MB used."
msgstr ""

#: revision-creation_rvy.php:55
msgid "Autosave disabled when editing a published post/page to create a pending revision."
msgstr ""

#: revision-creation_rvy.php:345
msgid "Sorry, an error occurred while attempting to submit your revision!"
msgstr ""

#: revision-creation_rvy.php:346
msgid "Revision Submission Error"
msgstr ""

#: revision-creation_rvy.php:453
msgid "Pending Revision Created"
msgstr ""

#: revision-creation_rvy.php:619
msgid "Sorry, an error occurred while attempting to schedule your revision!"
msgstr ""

#: revision-creation_rvy.php:620
msgid "Revision Scheduling Error"
msgstr ""

#: revision-creation_rvy.php:667
msgid "Scheduled Revision Created"
msgstr ""

#: revision-creation_rvy.php:691
msgid "Could not insert revision into the database"
msgstr ""

#: revision-creation_rvy.php:743
#, php-format
msgid "Invalid taxonomy: %s"
msgstr ""

#: revision-creation_rvy.php:822
msgid "Invalid page template."
msgstr ""

#: revision-workflow_rvy.php:157
#, php-format
msgid "[%s] Pending Revision Update"
msgstr ""

#: revision-workflow_rvy.php:159
#, php-format
msgid "A pending revision to the %1$s \"%2$s\" has been updated."
msgstr ""

#: revision-workflow_rvy.php:161
#, php-format
msgid "[%s] Pending Revision Notification"
msgstr ""

#: revision-workflow_rvy.php:163
#, php-format
msgid "A pending revision to the %1$s \"%2$s\" has been submitted."
msgstr ""

#: revision-workflow_rvy.php:173
msgid "Preview and Approval: "
msgstr ""

#: revision-workflow_rvy.php:176
msgid "Revision Queue: "
msgstr ""

#: revision-workflow_rvy.php:178
msgid "Edit Revision: "
msgstr ""

#: revision-workflow_rvy.php:266
msgid "Sorry, an error occurred while attempting to save your revision."
msgstr ""

#: revision-workflow_rvy.php:297
msgid "Your modification was saved as a Scheduled Revision."
msgstr ""

#: revision-workflow_rvy.php:302 revision-workflow_rvy.php:377
msgid "Keep editing the revision"
msgstr ""

#: revision-workflow_rvy.php:303
msgid "Go back to schedule another revision"
msgstr ""

#: revision-workflow_rvy.php:304 revision-workflow_rvy.php:379
msgid "View Revision Queue"
msgstr ""

#: revision-workflow_rvy.php:312 revision-workflow_rvy.php:388
msgid "Preview it"
msgstr ""

#: revision-workflow_rvy.php:359
msgid "Your modification has been saved."
msgstr ""

#: revision-workflow_rvy.php:361
msgid "Your modification has been saved for editorial review."
msgstr ""

#: revision-workflow_rvy.php:364
msgid "If approved by an editor, it will be published on the date you specified."
msgstr ""

#: revision-workflow_rvy.php:366
msgid "It will be published when an editor approves it."
msgstr ""

#: revision-workflow_rvy.php:378
msgid "Go back to submit another revision"
msgstr ""

#: revision-workflow_rvy.php:414
msgid "Return to Edit Posts"
msgstr ""

#: revision-workflow_rvy.php:417
msgid "Return to Edit Pages"
msgstr ""

#: revision-workflow_rvy.php:421
#, php-format
msgid "Return to Edit %s"
msgstr ""

#: revisionary.php:67
msgid "<strong>This plugin can be deleted.</strong>"
msgstr ""

#: revisionary.php:83 revisionary.php:142
#, php-format
msgid "Another copy of PublishPress Revisions (or Revisionary) is already activated (version %1$s: \"%2$s\")"
msgstr ""

#: revisionary.php:85 revisionary.php:144
#, php-format
msgid "Another copy of PublishPress Revisions (or Revisionary) is already activated (version %1$s)"
msgstr ""

#: revisionary.php:163
#, php-format
msgid "PublishPress Revisions requires PHP version %s or higher."
msgstr ""

#: revisionary.php:170
#, php-format
msgid "PublishPress Revisions requires WordPress version %s or higher."
msgstr ""

#: revisionary_main.php:583
msgid "Pending Revision Updated"
msgstr ""

#: revisionary_main.php:665
msgid "Invalid featured media ID."
msgstr ""

#: rvy_init.php:354 rvy_init.php:365
msgid "Publish Revision"
msgstr ""

#: rvy_init.php:364
msgid "Scheduled Revision"
msgstr ""

#: rvy_init.php:537
msgid "Revisor"
msgstr ""

#: rvy_init.php:993
msgid "Revision Workflow"
msgstr ""

#: vendor/publishpress/wordpress-version-notices/src/Module/MenuLink/Module.php:129
msgid "Amazing! We are redirecting you to our site..."
msgstr ""
