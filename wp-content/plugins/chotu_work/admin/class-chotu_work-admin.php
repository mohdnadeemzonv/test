<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/admin
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_work_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_work_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_work_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_work-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'chotu_work_c', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );	

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_work_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_work_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'chotu_work-select2', plugin_dir_url( __FILE__ ) . 'js/select2.full.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_work-admin.js', array( 'jquery' ), $this->version, false );

	}
	/*
	Add addres syntax to locality	
	*/


	public function chotu_meta_box_for_locality( $post ){
		    add_meta_box( 'address_syntax', __( 'Gated Locality Address Syntax', 'textdomain' ), array( $this, 'chotu_meta_box_for_locality_meta_box_html_output' ), 'locality' );
		}

	public function chotu_meta_box_for_locality_meta_box_html_output( $post ) {
		    wp_nonce_field( basename( __FILE__ ), 'meta_box_for_locality_meta_box_nonce' ); //used later for 
		    require_once plugin_dir_path( __FILE__ ) . 'locality/chotu_work_locality_address_syntax_fields.php';
		}

	/*
	Save addres syntax to locality	
	*/
		
	public function chotu_meta_box_for_locality_meta_box_save( $post_id ){
		    // check for nonce to top xss

		    if ( !isset( $_POST['meta_box_for_locality_meta_box_nonce'] ) ){
		        return;
		    }
		    if(empty(term_exists( $_POST['acf']['field_60f54fb4f09d9'], 'pincode' ))){
		    	session_start();
		    	//$errors->add("locality_pincode","Pincode not exist.");
		    	 $_SESSION['my_admin_notices'] = '<div class="error"><p>Pincode not exist.</p></div>';
		    	 update_post_meta($post_id,'locality_pincode','');
		    	 header('Location: '.get_edit_post_link($post_id, 'redirect'));
            	exit;
		    }
		    // check for correct user capabilities - stop internal xss from customers
		    if ( ! current_user_can( 'edit_post', $post_id ) ){
		        return;
		    }
		    
		     $address_syntax = array();
			   for ($i=1; $i < 4; $i++) {
			        if(isset($_REQUEST['level_'.$i.'_name']) && $_REQUEST['level_'.$i.'_name'] !='' && $_REQUEST['level_'.$i.'_type'] !=''){
			            $address_syntax['level_'.$i]['level'] = $_REQUEST['level_'.$i.'_name'];
			           $address_syntax['level_'.$i]['type'] = $_REQUEST['level_'.$i.'_type'];
			           $address_syntax['level_'.$i]['options'] = $_REQUEST['level_'.$i.'_values'];
			        }
			    }
		  $geography ='';
		   update_post_meta($post_id,'locality_address_syntax',json_encode($address_syntax));
		   if(isset($_REQUEST['acf']['field_60f55058876b7'])){
		   	if(isset($_POST['tax_input']['locality_geography'][1])){
		   		$geography = $_POST['tax_input']['locality_geography'][1];
		   	}
		   	chotu_update_location_data($post_id,'locality',$_POST['acf']['field_60f55058876b7'],$_POST['acf']['field_60f54fb4f09d9'],$_POST['acf']['field_60f54f7eea1bf'],$geography);
		   	//chotu_update_location_data($post_id,$_REQUEST['acf']['field_60f55058876b7'],'gated_locality');
		   }
			
		}

	
	

	public function chotu_add_new_blog_field($data) {
		$blog_id = $data->blog_id;
		 global $wpdb;
	    switch_to_blog($blog_id);
	    $new_field_value = '';
	    update_option('gmt_offset','Asia/Kolkata');
	    update_option('timezone_string','Asia/Kolkata');
	    
	   //  $posts = $wpdb->base_prefix.$blog_id.'_posts';
	   //  if($blog_id){
	   //       $wpdb->insert( $posts, array(
	   //          'post_author' => get_current_user_id(), 
	   //          'post_date' => date('Y-m-d H:i:s'),
	   //          'post_date_gmt' => date('Y-m-d H:i:s'), 
	   //          'post_content' => '',
	   //          'post_title' => 'Home', 
	   //          'post_excerpt' => '', 
	   //          'post_type' => 'page', 
	   //          'post_status' => 'publish'
	   //      ));
	     
	   //      $post_id = $wpdb->insert_id;
	   //      update_custom_post_meta( $post_id,$blog_id, '_wp_page_template', 'vendor-home-page.php');
		  //   update_option('page_on_front',$post_id);
		  // }
	   
	   //  if(!empty($_POST['route_weekdays'])){
	   //  	add_site_meta( $blog_id, 'route_weekdays',serialize($_POST['route_weekdays']), '' );
	   //  }
	    
	   //  if(isset($_POST['blog']['order_cut_off_time'])){
	   //  	add_site_meta( $blog_id, 'order_cut_off_time',$_POST['blog']['order_cut_off_time'], '' );
	   //  }
	   //  if(isset($_POST['blog']['lead_time'])){
	   //  	add_site_meta( $blog_id, 'lead_time', $_POST['blog']['lead_time'], '' );
	   //  }if(isset($_POST['blog']['lead_time_custom']) && $_POST['blog']['lead_time_custom'] !=''){
	   //  	add_site_meta( $blog_id, 'order_cut_off_time', $_POST['blog']['lead_time_custom'], '' );
	   //  }if(isset($_POST['blog']['brand_tag']) && $_POST['blog']['brand_tag'] !=''){
	   //  	add_site_meta( $blog_id, 'brand_tag', $_POST['blog']['brand_tag'], '' );
	    // }
	    restore_current_blog();
	}

	
	/*
	public function chotu_wp_update_site_meta($new_site, $old_site){
		if(isset($new_site) && !empty($new_site)){
			
			if(!empty($_POST['dpl_weekdays'])){
		    	update_site_meta( $blog_id, 'dpl_weekdays',serialize($_POST['dpl_weekdays']), '' );
		    }
			if(isset($_POST['blog']['order_cut_off_time'])){
	    		update_site_meta( $blog_id, 'order_cut_off_time',$_POST['blog']['order_cut_off_time'], '' );
		    }
		    if(isset($_POST['blog']['lead_time'])){
		    	update_site_meta( $blog_id, 'order_cut_off_time', $_POST['blog']['lead_time'], '' );
		    }if(isset($_POST['blog']['lead_time_custom']) && $_POST['blog']['lead_time_custom'] !=''){
		    	update_site_meta( $blog_id, 'order_cut_off_time', $_POST['blog']['lead_time_custom'], '' );
		    }if(isset($_POST['blog']['brand_tag']) && $_POST['blog']['brand_tag'] !=''){
		    	update_site_meta( $blog_id, 'brand_tag', $_POST['blog']['brand_tag'], '' );
		    }
			
		}
	}
	*/

	/*
		Add custom profile fields to captain
	*/
    public function chotu_work_user_profile_field($profileuser) {
    	require_once plugin_dir_path( __FILE__ ) . 'captain/chotu_captain_fields_display.php';
       // echo '<script>document.getElementById("email").value = "user_'.$profileuser->ID.'@chotu.app";</script>';
    }
    public function chotu_save_extra_user_profile_fields($user_id)
    {

    	if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
    		return;
    	}

    	if ( !current_user_can( 'edit_user', $user_id ) ) { 
    		return false; 
    	}
    	if ( !empty( $_POST['phone_number'] ) ) {
	    	update_user_meta( $user_id, 'phone_number', $_POST['phone_number'] );
	    }
	    // if ( !empty( $_POST['geography'] ) ) {
	    // 	update_user_meta( $user_id, 'geography', $_POST['geography'] );
	    // }
		$user = new WP_user($user_id);
		if ( in_array( 'captain',$user->roles ) ) {
			session_start();
				if(empty(term_exists( $_POST['acf']['field_610a2a6f1f561'], 'pincode' ))){
				$_SESSION['my_admin_notices'] = '<div class="error"><p>Pincode not exist.</p></div>';
					update_post_meta($user_id,'captain_pincode','');
					header('Location: '.get_edit_user_link($user_id, 'redirect'));
					exit;
				return false; 
			}
		}
	    
	    if(!empty($_POST['captain_brand'])){
	    	update_captain_brand_data($user_id,$_POST['captain_brand']);
	    	//update_user_meta( $user_id, 'captain_brand', json_encode($_POST['captain_brand']) );
	    }else{
	    	//update_user_meta( $user_id, 'captain_brand','' );
	    	delete_captain_data($user_id,'brand');
	    }
	    if(!empty($_POST['captain_locality'])){
	    	update_captain_locality_data($user_id,$_POST['captain_locality']);
	    	//update_user_meta( $user_id, 'captain_locality', json_encode($_POST['captain_locality']) );
	    }else{
	    	//update_user_meta( $user_id, 'captain_locality','' );
	    	delete_captain_data($user_id,'locality');
	    }
	    chotu_update_location_data($user_id,'captain',$_POST['acf']['field_610a2a871f562'],$_POST['acf']['field_610a2a6f1f561'],$_POST['acf']['field_60febafb71302'],$_POST['geography']);

    }

    // public function chotu_save_extra_user_profile_fields_errors($errors, $update, $user){
    // 	if(empty(term_exists( $_POST['acf']['field_610a2a6f1f561'], 'pincode' ))){
	   //  	$errors->add( 'captain_pin_code_error', __( '<strong>ERROR</strong>: Pincode not exist.', 'crf' ) );
	   //  }
    	
    // }
    /*
		Create locality_concat = geo_breadcrumb + '>' + locality name
    */
	public function chotu_set_post_default_category( $post_id, $post, $update ) {
		global $post;
		$locality_geography = wp_get_object_terms( $post_id, 'geography');
		if(!empty($locality_geography)){
			foreach ($locality_geography as $key => $value) {
				update_post_meta($post_id,'geo_locality',$value->name.' > '.$post->post_title);
				$term_taxonomy_ids = wp_set_object_terms( $post_id, $value->name.' > '.$post->post_title, 'geo_locality_concat');
				$suggestion_term = $value->name.' > '.$post->post_title;

					  $taxonomy = 'geo_locality_concat'; // The name of the taxonomy the term belongs in
					  wp_set_post_terms( $post_id, array($suggestion_term), $taxonomy );
			}
			
		}
		
	}

}

