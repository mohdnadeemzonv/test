<?php

/*
Create new tabs for Brand Page
*/

add_filter( 'network_edit_site_nav_links', 'chotu_work_new_siteinfo_tab' );
function chotu_work_new_siteinfo_tab( $tabs ){

	$tabs['brand-settings'] = array(
		'label' => 'Brand Settings',
		'url' => 'sites.php?page=brand_settings',
		'cap' => 'manage_sites'
	);
	$tabs['brand-order-cycle'] = array(
		'label' => 'Brand Order Cycle',
		'url' => 'sites.php?page=brand_order_cycle',
		'cap' => 'manage_sites'
	);
	$tabs['brand-payment-cycle'] = array(
		'label' => 'Brand Payment',
		'url' => 'sites.php?page=brand_payment',
		'cap' => 'manage_sites'
	);
	// $tabs['brand-cycle'] = array(
	// 	'label' => 'Brand Cycle',
	// 	'url' => 'sites.php?page=brand_cycle',
	// 	'cap' => 'manage_sites'
	// );
	return $tabs;

}

/*
	Respective pages for above tabs
*/
add_action( 'network_admin_menu', 'chotu_work_new_page' );
function chotu_work_new_page(){
	add_submenu_page(
		'sites.php',
		'Edit website', // will be displayed in <title>
		'Edit website', // doesn't matter
		'manage_network_options', // capabilities
		'brand_settings',
		'chotu_handle_admin_page' // the name of the function which displays the page
	);
	add_submenu_page(
		'sites.php',
		'Edit website', // will be displayed in <title>
		'Edit website', // doesn't matter
		'manage_network_options', // capabilities
		'brand_order_cycle',
		'chotu_brand_new_handle_admin_page' // the name of the function which displays the page
	);
	add_submenu_page(
		'sites.php',
		'Edit website', // will be displayed in <title>
		'Edit website', // doesn't matter
		'manage_network_options', // capabilities
		'brand_payment',
		'chotu_brand_payment_handle_admin_page' // the name of the function which displays the page
	);
	// add_submenu_page(
	// 	'sites.php',
	// 	'Edit website', // will be displayed in <title>
	// 	'Edit website', // doesn't matter
	// 	'manage_network_options', // capabilities
	// 	'brand_cycle',
	// 	'chotu_brand_cycle_handle_admin_page' // the name of the function which displays the page
	// );

}

/*
add_action('admin_head','chotu_work_trick');
function chotu_work_trick(){
	
	echo '<style>
	#menu-site .wp-submenu li:last-child{
	display:none;
}

</style>';

}
*/

/*
Edit page for brand settings
*/
function chotu_handle_admin_page(){

	// do not worry about that, we will check it too
	$blog_id = $_REQUEST['id'];
	$blog_details = get_blog_details($blog_id);
	$hidden = 'hidden';
	// you can use $details = get_site( $id ) to add website specific detailes to the title
	$title = 'Edit Brand: ';
	$weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	
	echo '<div class="wrap"><h1 id="edit-site">' . $title .$blog_details->blogname.'</h1>
	<p class="edit-site-actions"><a href="' . esc_url( get_home_url( $blog_id, '/' ) ) . '">Visit</a> | <a href="' . esc_url( get_admin_url( $blog_id ) ) . '">Dashboard</a></p>';
	$brand_lat_long=$brand_address=$brand_pincode='';
	
	$location_table_data = get_location_object_data_by_type($blog_id,'brand');
	if(!empty($location_table_data)){
		$brand_lat_long = $location_table_data->lat_long;
		$brand_address = $location_table_data->full_address;
		$brand_pincode = $location_table_data->pincode;
	}
	$brand_category = get_site_meta( $blog_id, 'brand_category','single');
	$brand_geography = '';
	$brand_geography_data = get_location_object_data_by_type($blog_id,'brand');
	if(!empty($brand_geography_data)){
		$brand_geography = $brand_geography_data->geography_breadcrumb;
	}
	
		// navigation tabs
	network_edit_site_nav( array(
		'blog_id'  => $blog_id,
			'selected' => 'brand-settings' // current tab
		) );

		// more CSS tricks :)
		?>
		<style>
			#menu-site .wp-submenu li.wp-first-item{
				font-weight:600;
			}
			#menu-site .wp-submenu li.wp-first-item a{
				color:#fff;
			}
		</style>
		<form method="post" action="edit.php?action=chotu_workupdate">
			<?php 
			wp_nonce_field( 'brand-check' . $blog_id );
			?>
			<input type="hidden" name="id" value="<?php echo $blog_id ?>" />
			<table class="form-table">
				<tr class="form-field form-required">
					<th scope="row">Lat Long</th>
					<td><input class="regular-text " type="text" id="lat_long_blog" title="Lat Long" name="brand_lat_long" value="<?php echo $brand_lat_long;?>">
						<p>Explanation about your new Lat long</p>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row">Brand Pincode</th>
					<td>
						<input class="regular-text pincode_validation" type="number" id="brand_pincode" title="brand pincode" name="brand_pincode" value="<?php echo $brand_pincode;?>" pattern="[1-9]{1}[0-9]{9}" maxlength="6" required>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row">Brand Address</th>
					<td><textarea name="brand_address" id="brand_address" placeholder="address"><?php echo $brand_address;?></textarea>
					</td>
				</tr>
				<tr>
				<th><label for="brand_category"><?php _e("Brand Category Subcategory"); ?></label></th>
				<td>
					<!-- <select class="" name="brand_category" id="brand_category"> -->
					<?php 
					//$brand_category = chotu_get_taxonomy_options('brand_category',$brand_category);
					$dropdown = wp_dropdown_categories(array('taxonomy' => 'brand_category', 'name' => 'brand_category', 'echo' => 0, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => $brand_category, 'hide_empty' => 0, 'orderby' => 'name', 'hierarchical' => 1));
					     // Hackily add in the data link parameter.
					 $dropdown = str_replace('&nbsp;', '-', $dropdown);
					echo $dropdown;
					?>
					<!-- </select> -->
				</td>
			</tr>
			<tr>
				<th><label for="brand_geography"><?php _e("Brand Geography"); ?></label></th>
				<td>
					<!-- <select class="" name="brand_geography" id="brand_geography"> -->

					<?php 
					//$locality_geography = chotu_get_taxonomy_options('locality_geography',$brand_geography);
					$dropdown = wp_dropdown_categories(array('taxonomy' => 'geography', 'name' => 'brand_geography', 'echo' => 0, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => $brand_geography, 'hide_empty' => 0, 'orderby' => 'name', 'hierarchical' => 1));
					     // Hackily add in the data link parameter.
					 $dropdown = str_replace('&nbsp;', '-', $dropdown);
					echo $dropdown;
					?>
					<!-- </select> -->
				</td>
			</tr>
				
		</table>
		<?php
		submit_button();
		?>
	</form></div>


<?php }
/*
	Brand setting field save to database
*/
add_action('network_admin_edit_chotu_workupdate',  'chotu_work_save_options');
function chotu_work_save_options() {
	$blog_id = $_POST['id'];
		check_admin_referer('brand-check'.$blog_id); // security check
	    
	    // if(isset($_POST['brand_lat_long']) && $_POST['brand_lat_long'] !=''){
	    // 	update_site_meta( $blog_id, 'brand_lat_long', $_POST['brand_lat_long'], '' );
	    // }else{
	    // 	update_site_meta( $blog_id, 'brand_lat_long', '', '' );
	    // }
	    // if(isset($_POST['brand_pincode']) && $_POST['brand_pincode'] !=''){
	    // 	update_site_meta( $blog_id, 'brand_pincode', $_POST['brand_pincode'], '' );
	    // }else{
	    // 	update_site_meta( $blog_id, 'brand_pincode', '', '' );
	    // }
	    // if(isset($_POST['brand_address']) && $_POST['brand_address'] !=''){
	    // 	update_site_meta( $blog_id, 'brand_address', $_POST['brand_address'], '' );
	    // }else{
	    // 	update_site_meta( $blog_id, 'brand_address', '', '' );
	    // }
	    if(isset($_POST['brand_pincode']) && $_POST['brand_pincode'] !=''){
	    	if(empty(term_exists( $_POST['brand_pincode'], 'pincode' ))){
	    		session_start();
	    		$_SESSION['my_admin_notices'] = '<div class="error"><p>Pincode not exist</p></div>';
	    		
	    		header('Location: '.network_admin_url('sites.php?page=brand_settings&id='.$blog_id));
            exit;
	    	}
	    }
	    if(isset($_POST['brand_category'])){
	    	update_site_meta( $blog_id, 'brand_category',$_POST['brand_category'], '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_category',0, '' );
	    }
	    if(isset($_POST['brand_geography'])){
	    	chotu_update_location_data($blog_id,'brand',$_POST['brand_lat_long'],$_POST['brand_pincode'],$_POST['brand_address'],$_POST['brand_geography']);
	    	//update_site_meta( $blog_id, 'brand_geography',$_POST['brand_geography'], '' );
	    }else{
	    	chotu_update_location_data($blog_id,'brand',$_POST['brand_lat_long'],$_POST['brand_pincode'],$_POST['brand_address'],'');
	    }
	     $widget = unserialize('a:8:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:0:{}s:8:"header-1";a:1:{i:0;s:7:"block-2";}s:8:"footer-1";a:0:{}s:8:"footer-2";a:0:{}s:8:"footer-3";a:0:{}s:8:"footer-4";a:0:{}s:13:"array_version";i:3;}');
	     // dd($widget);
	    update_blog_option($blog_id,'sidebars_widgets',$widget);
		wp_redirect( add_query_arg( array(
				'page' => 'brand_settings',
				'id' => $blog_id,
				'updated' => 'true'), network_admin_url('sites.php?page=brand_settings&id='.$blog_id)
		));
	// redirect to /wp-admin/sites.php?page=mishapage&blog_id=ID&updated=true

	exit;

}

/*
	On successful creation of brand, this message pops out
*/
add_action( 'network_admin_notices', 'chotu_work_notice' );
function chotu_work_notice() {

	if(isset($_SESSION['my_admin_notices'])){
		echo $_SESSION['my_admin_notices'];
		unset($_SESSION['my_admin_notices']);
	}else if( isset( $_GET['updated'] ) && isset( $_GET['page'] ) && $_GET['page'] == 'brand_settings' ) {

		echo '<div id="message" class="updated notice is-dismissible">
		<p>Congratulations!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
		</div>';

	}

}

/*
check if the site is valid or NOT
*/
add_action( 'current_screen', 'chotu_redirects' );
function chotu_redirects(){

	// do nothing if we are on another page
	$screen = get_current_screen();
	if( $screen->id !== 'sites_page_brand_settings-network' ) {
		return;
	}

	// $id is a blog ID
	$id = isset( $_REQUEST['id'] ) ? intval( $_REQUEST['id'] ) : 0;

	if ( ! $id ) {
		wp_die( __('Invalid site ID.') );
	}

	$details = get_site( $id );
	if ( ! $details ) {
		wp_die( __( 'The requested site does not exist.' ) );
	}


	if ( ! can_edit_network( $details->site_id ) ) {
		wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
	}

}

/*
	Functions within tab brand brand-order-cycle
*/

function chotu_brand_new_handle_admin_page(){
	// do not worry about that, we will check it too
	$blog_id = $_REQUEST['id'];
	$blog_details = get_blog_details($blog_id);
	$hidden = 'hidden';
	// you can use $details = get_site( $id ) to add website specific detailes to the title
	$title = 'Edit Brand: ';
	
	echo '<div class="wrap"><h1 id="edit-site">' . $title.$blog_details->blogname. '</h1>
	<p class="edit-site-actions"><a href="' . esc_url( get_home_url( $blog_id, '/' ) ) . '">Visit</a> | <a href="' . esc_url( get_admin_url( $blog_id ) ) . '">Dashboard</a></p>';
	$banner_url = '';
	$square_url ='';
	$brand_weekday_0 = get_site_meta( $blog_id, 'brand_weekday_0','single');
	$brand_weekday_1 = get_site_meta( $blog_id, 'brand_weekday_1','single');
	$brand_weekday_2 = get_site_meta( $blog_id, 'brand_weekday_2','single');
	$brand_weekday_3 = get_site_meta( $blog_id, 'brand_weekday_3','single');
	$brand_weekday_4 = get_site_meta( $blog_id, 'brand_weekday_4','single');
	$brand_weekday_5 = get_site_meta( $blog_id, 'brand_weekday_5','single');
	$brand_weekday_6 = get_site_meta( $blog_id, 'brand_weekday_6','single');

	$brand_cut_off_time = get_site_meta( $blog_id, 'brand_cut_off_time','single');
	$brand_lead_time = get_site_meta( $blog_id, 'brand_lead_time','single');
	$brand_cut_off_time = get_site_meta( $blog_id, 'brand_cut_off_time','single');
	//$brand_number_of_order_cycles = get_site_meta( $blog_id, 'brand_number_of_order_cycles','single');
	// if($brand_number_of_order_cycles =='' || $brand_number_of_order_cycles == NULL){
	// 	$brand_number_of_order_cycles = 12;
	// }

	
	// $brand_banner_pic = get_site_meta( $blog_id, 'brand_banner_pic','single');
	// $brand_square_pic = get_site_meta( $blog_id, 'brand_square_pic','single');
	// if(isset($brand_banner_pic)){
	// 	$banner_url = wp_get_attachment_image_src($brand_banner_pic);
	// 	dd($banner_url);
	// }
	// if(isset($brand_square_pic)){
	// 	$square_url = wp_get_attachment_image_src($brand_square_pic);
	// }
	
		// navigation tabs
	network_edit_site_nav( array(
		'blog_id'  => $blog_id,
			'selected' => 'brand-order-cycle' // current tab
		) );

		// more CSS tricks :)
		?>
		<style>
			#menu-site .wp-submenu li.wp-first-item{
				font-weight:600;
			}
			#menu-site .wp-submenu li.wp-first-item a{
				color:#fff;
			}
		</style>
		<link href="<?php echo plugin_dir_url( __DIR__ )?>css/select2.min.css" rel="stylesheet" />
		<form method="post" action="edit.php?action=brand_order_cycle" enctype="multipart/form-data">
			<?php 
			wp_nonce_field( 'brand-order-cycle' . $blog_id );
			?>
			<input type="hidden" name="id" value="<?php echo $blog_id ?>" />
			<table class="form-table">
			<tr>
					<th><label for="route_weekdays"><?php _e("Weekdays Name"); ?></label></th>
					<td class="route_weekdays_field_spam">
						<input type="checkbox" name="brand_weekday_0" value="0" <?php if(!empty($brand_weekday_0) && $brand_weekday_0 == 1){ echo 'checked';}?>><span>Sunday</span>

						<input type="checkbox" name="brand_weekday_1" value="1" <?php if(!empty($brand_weekday_1) && $brand_weekday_1 == 1){ echo 'checked';}?>><span>Monday</span>          
					<input type="checkbox" name="brand_weekday_2" value="2" <?php if(!empty($brand_weekday_2) && $brand_weekday_2 == 1){ echo 'checked';}?>><span>Tuesday</span>

						<input type="checkbox" name="brand_weekday_3" value="3" <?php if(!empty($brand_weekday_3) && $brand_weekday_3 == 1){ echo 'checked';}?>><span>Wednesday</span>
						<input type="checkbox" name="brand_weekday_4" value="4" <?php if(!empty($brand_weekday_4) && $brand_weekday_4 == 1){ echo 'checked';}?>><span>Thursday</span>
						<input type="checkbox" name="brand_weekday_5" value="5" <?php if(!empty($brand_weekday_5) && $brand_weekday_5 == 1){ echo 'checked';}?>><span>Friday</span>

						<input type="checkbox" name="brand_weekday_6" value="6" <?php if(!empty($brand_weekday_6) && $brand_weekday_6 == 1){ echo 'checked';}?>><span>Saturday</span>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row">Number Of Order Cycles</th>
					<td><input type="number" name="brand_number_of_order_cycles" id="brand_number_of_order_cycles" placeholder="12" value="12">
					</td>
				</tr>
				<tr>
					<th><label for="brand_cut_off_time"><?php _e("Order cut-off time"); ?></label></th>
					<td>

						<select name="brand_cut_off_time" id="brand_cut_off_time">
							<option value="01:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '01:00'){ echo 'selected';}?>>01:00 AM</option>
							<option value="02:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '02:00'){ echo 'selected';}?>>02:00 AM</option>
							<option value="03:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '03:00'){ echo 'selected';}?>>03:00 AM</option>
							<option value="04:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '04:00'){ echo 'selected';}?>>04:00 AM</option>
							<option value="05:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '05:00'){ echo 'selected';}?>>05:00 AM</option>
							<option value="06:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '06:00'){ echo 'selected';}?>>06:00 AM</option>
							<option value="07:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '07:00'){ echo 'selected';}?>>07:00 AM</option>
							<option value="08:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '08:00'){ echo 'selected';}?>>08:00 AM</option>
							<option value="09:00"<?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '09:00'){ echo 'selected';}?>>09:00 AM</option>
							<option value="10:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '10:00'){ echo 'selected';}?>>10:00 AM</option>
							<option value="11:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '11:00'){ echo 'selected';}?>>11:00 AM</option>
							<option value="12:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '12:00'){ echo 'selected';}?>>12:00 AM</option>
							<option value="13:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '13:00'){ echo 'selected';}?>>01:00 PM</option>
							<option value="14:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '14:00'){ echo 'selected';}?>>02:00 PM</option>
							<option value="15:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '15:00'){ echo 'selected';}?>>03:00 PM</option>
							<option value="16:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '16:00'){ echo 'selected';}?>>04:00 PM</option>
							<option value="17:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '17:00'){ echo 'selected';}?>>05:00 PM</option>
							<option value="18:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '18:00'){ echo 'selected';}?>>06:00 PM</option>
							<option value="19:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '19:00'){ echo 'selected';}?>>07:00 PM</option>
							<option value="20:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '20:00'){ echo 'selected';}?>>08:00 PM</option>
							<option value="21:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '21:00'){ echo 'selected';}?>>09:00 PM</option>
							<option value="22:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '22:00'){ echo 'selected';}?>>10:00 PM</option>
							<option value="23:00" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '23:00'){ echo 'selected';}?>>11:00 PM</option>
							<option value="23:59" <?php if(!empty($brand_cut_off_time) && $brand_cut_off_time == '23:59'){ echo 'selected';}?>>12:00 AM</option>
						</select>
					</td>
				</tr>
				<tr>
					<th><label for="brand_lead_time"><?php _e("Lead Time"); ?></label></th>
					<td>
						<input type="number" name="brand_lead_time" value="<?php echo $brand_lead_time?>">
						<p><strong>0:</strong> Same day, <strong>1:</strong> Next day, <strong>2:</strong> 2 Days later and so on...</p>
					</td>
				</tr>
		</table>
		<?php
		submit_button();
		?>
	</form></div>
<?php }

/*
	Save the meta fields in brand-order-cycle new
*/
add_action('network_admin_edit_brand_order_cycle',  'chotu_brand_new_save_options');
function chotu_brand_new_save_options() {
	
	$blog_id = $_POST['id'];
	check_admin_referer('brand-order-cycle'.$blog_id); // security check
	   	if(isset($_POST['brand_weekday_0'])){
	    	update_site_meta( $blog_id, 'brand_weekday_0',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_0',0, '' );
	    }
	    if(isset($_POST['brand_weekday_1'])){
	    	update_site_meta( $blog_id, 'brand_weekday_1',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_1',0, '' );
	    }
	    if(isset($_POST['brand_weekday_2'])){
	    	update_site_meta( $blog_id, 'brand_weekday_2',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_2',0, '' );
	    }
	    if(isset($_POST['brand_weekday_3'])){
	    	update_site_meta( $blog_id, 'brand_weekday_3',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_3',0, '' );
	    }
	    if(isset($_POST['brand_weekday_4'])){
	    	update_site_meta( $blog_id, 'brand_weekday_4',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_4',0, '' );
	    }
	    if(isset($_POST['brand_weekday_5'])){
	    	update_site_meta( $blog_id, 'brand_weekday_5',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_5',0, '' );
	    }
	    if(isset($_POST['brand_weekday_6'])){
	    	update_site_meta( $blog_id, 'brand_weekday_6',1, '' );
	    }else{
	    	update_site_meta( $blog_id, 'brand_weekday_6',0, '' );
	    }
	    if(isset($_POST['brand_cut_off_time'])){
	    	update_site_meta( $blog_id, 'brand_cut_off_time',$_POST['brand_cut_off_time'], '' );
	    }
	    if(isset($_POST['brand_lead_time'])){
	    	update_site_meta( $blog_id, 'brand_lead_time', $_POST['brand_lead_time'], '' );
	    }
	    if(isset($_POST['brand_number_of_order_cycles'])){
	    	//update_site_meta( $blog_id, 'brand_number_of_order_cycles', $_POST['brand_number_of_order_cycles'], '' );
	    	create_brand_order_cycle($blog_id,$_POST['brand_number_of_order_cycles']);
	    }

	    wp_redirect( add_query_arg( array(
				'page' => 'brand_order_cycle',
				'id' => $blog_id,
				'updated' => 'true'), network_admin_url('sites.php?page=brand_order_cycle&id='.$blog_id)
		));
		exit;
 
	}


function chotu_brand_payment_handle_admin_page(){
		// more CSS tricks :)
		$blog_id = $_REQUEST['id'];
		$blog_details = get_blog_details($blog_id);
		$mycred_balance = get_site_meta( $blog_id, 'mycred_balance','single' );
		$prepaid_razorpay = get_site_meta( $blog_id, 'prepaid_razorpay', 'single');
		if(empty($prepaid_razorpay) || $prepaid_razorpay == ''){
			$prepaid_razorpay = 1;
		}
		$cod = get_site_meta( $blog_id, 'cod','single');
		$hidden = 'hidden';
		// you can use $details = get_site( $id ) to add website specific detailes to the title
		$title = 'Edit Payment Type: ';
		
		echo '<div class="wrap"><h1 id="edit-site">' . $title.$blog_details->blogname. '</h1>
		<p class="edit-site-actions"><a href="' . esc_url( get_home_url( $blog_id, '/' ) ) . '">Visit</a> | <a href="' . esc_url( get_admin_url( $blog_id ) ) . '">Dashboard</a></p>';
		network_edit_site_nav( array(
		'blog_id'  => $blog_id,
			'selected' => 'brand-payment-cycle' // current tab
		) );
		?>

		<form method="post" action="edit.php?action=brand_payment" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $blog_id ?>" />
			<?php 
			wp_nonce_field( 'brand-payment-cycle' . $blog_id );
			?>
	       <table class="form-table">
			<tr>
					<th><label for="payment_type"><?php _e("Payment Type"); ?></label></th>
					<!-- <td class="payment_type_field_spam"> -->
					
									
					<!-- </td> -->
					 <table class="acf-table">
			         <thead>
			            <tr>
			               <th class="acf-th" data-name="mycred_balance" data-type="radio" data-key="mycred_balance" style="width: 25%;">
			                  <label for="mycred_balance">myCRED balance</label>							
			               </th>
			               <th class="acf-th" data-name="prepaid_razorpay" data-type="radio" data-key="prepaid_razorpay" style="width: 25%;">
			                  <label for="prepaid_razorpay">Prepaid (Razorpay)</label>							
			               </th>
			               <th class="acf-th" data-name="cod" data-type="radio" data-key="cod" style="width: 25%;">
			                  <label for="cod">COD</label>							
			               </th>
			            </tr>
			         </thead>
         	<tbody>
         	 <tr class="acf-row">
         	 			<td>
							<input type="radio" name="mycred_balance" value="0" <?php if($mycred_balance == 0){ echo 'checked';}?>>No<br/><br/>
							<input type="radio" name="mycred_balance" value="1" <?php if($mycred_balance == 1){ echo 'checked';}?>>Yes
							</td>
						<td>
							
							<input type="radio" name="prepaid_razorpay" value="0" <?php if($prepaid_razorpay == 0){ echo 'checked';}?>>No<br/><br/>
							<input type="radio" name="prepaid_razorpay" value="1" <?php if($prepaid_razorpay == 1){ echo 'checked';}?>>Yes
						</td>
						<td>
							
							<input type="radio" name="cod" value="0" <?php if($cod == 0){ echo 'checked';}?>>No<br/><br/>
							<input type="radio" name="cod" value="1" <?php if($cod == 1){ echo 'checked';}?>>Yes
						</td>	
         	 </tr>
         	</tbody>
         </table>
			</tr>
		</table>
		<?php
		submit_button();
		?>
		</form>
<?php 
}

/*
	Save the meta fields in brand-order-cycle new
*/
add_action('network_admin_edit_brand_payment',  'chotu_brand_payment_save_options');
function chotu_brand_payment_save_options() {
	$blog_id = $_POST['id'];
	check_admin_referer('brand-payment-cycle'.$blog_id); // security check
	   	if(isset($_POST['mycred_balance'])){
	    	update_site_meta( $blog_id, 'mycred_balance',$_POST['mycred_balance'] );
	    }
	    
	    if(isset($_POST['prepaid_razorpay'])){
	    	update_site_meta( $blog_id, 'prepaid_razorpay',$_POST['prepaid_razorpay']);
	    }
	    if(isset($_POST['cod'])){
	    	update_site_meta( $blog_id, 'cod',$_POST['cod']);
	    }
	    wp_redirect( add_query_arg( array(
				'page' => 'brand_payment',
				'id' => $blog_id,
				'updated' => 'true'), network_admin_url('sites.php?page=brand_payment&id='.$blog_id)
		));
		exit;
 
	}
