<?php


/* Add banner pic to the brand website*/
function chotu_work_customize_register( $wp_customize ) {
 // Add Section
$wp_customize->add_section('brand_banner_pic', array(
      'title'             => __('Brand Banner Pic', 'name-theme'), 
      'priority'          => 70,
  )); 
$wp_customize->add_setting( 'banner_pic', array(
        'default' => get_bloginfo('template_directory') . '/images/logo.png',
    ) );
$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'logo',
           array(
               'label'      => __( 'Upload a Banner Pic', 'theme_name' ),
               'section'    => 'brand_banner_pic',
               'settings'   => 'banner_pic',
               'height'=>200, // cropper Height
               'width'=>600, // Cropper Width
               'flex_width'=>true, //Flexible Width
               'flex_height'=>true, //
               'context'    => 'your_setting_context'
           )
       )
   );
}
add_action('customize_register', 'chotu_work_customize_register');


/* As super-admin, when you add a user as network-admin, this will replace the default username/email ID with just phone number. Phonenumber@chotu.app is the default email ID and username=phone-number.*/
add_action('network_user_new_form','network_user_new_form_fields');
function network_user_new_form_fields(){?>
  <style type="text/css">
    #adduser .form-table{
      display: none;
    }
  </style>
  <table class="form-tables" role="presentation">
    <tbody><tr class="form-field form-required">
      <th scope="row"><label for="username">Phone Number</label></th>
      <td><input type="text" class="regular-text" name="user[phone_number]" id="phone_number" autocapitalize="none" autocorrect="off" maxlength="60"></td>
    </tr>
  </tbody></table>
  <tr class="form-field">
      <td colspan="2" class="td-full">Please enter the user email ID as 10-digit Indian phone number @chotu.app. Example: if phone number is +91 98660 11883, enter it as 9866011883@chotu.app</td>
    </tr>
    <script type="text/javascript">
            jQuery(document).on('click','#add-user',function(e){
            e.preventDefault();
            jQuery('#username').val(jQuery('#phone_number').val());
            jQuery('#email').val(jQuery('#phone_number').val()+'@chotu.app');
            jQuery('#adduser').submit();
              
            });
    </script>
<?php }

add_filter('wpmu_validate_user_signup', 'chotu_remove_user_name_error' );
/**
 * Allow very short user names.
 *
 * @wp-hook wpmu_validate_user_signup
 * @param   array $result
 * @return  array
 */
function chotu_remove_user_name_error( $result )
{
    $error_name = $result[ 'errors' ]->get_error_message( 'user_name' );
    if ( empty ( $error_name ))
    {
        return $result;
    }
    unset ( $result[ 'errors' ]->errors[ 'user_name' ] );
    return $result;
}

/*Rename the admin menu from Sites to Brans*/
add_action("network_admin_menu", function () {
    foreach ($GLOBALS["menu"] as $position => $tab) {
        if ("Sites" === $tab["0"]) {
            $GLOBALS["menu"][$position][0] = "Brands";
            break;
        }
    }
});
/* we replace email with phone number completely. The unique identifier is phone number instead of email. A user who enters 9866011883 as a phone number, we create a dummy email called 9866011883@chotu.app. For this dummy email, we should not verify by email or not send any notifications. The following code does this.

skip email notification to new user */
add_filter( 'wpmu_signup_user_notification', '__return_false' ); // Disable confirmation email
add_filter( 'wpmu_welcome_user_notification', '__return_false' );
add_action( 'user_new_form', 'chotu_work_admin_registration_form' );
add_action( 'user_new_form', 'chotu_work_admin_registration_form' );
function chotu_work_admin_registration_form( $operation ) {
  if ( 'add-new-user' !== $operation ) {
    // $operation may also be 'add-existing-user'
    return;
  }

  $phone_number = ! empty( $_POST['phone_number'] ) ? intval( $_POST['phone_number'] ) : '';

  ?>
  <h3><?php esc_html_e( 'Personal Information', 'crf' ); ?></h3>

  <table class="form-table">
    <tr class="form-field form-required">
      <th><label for="phone_number"><?php esc_html_e( 'Phone Number', 'crf' ); ?></label> <span class="description"><?php esc_html_e( '(required)', 'crf' ); ?></span></th>
      <td>
        <input type="number"min="10" max="10"step="1"id="phone_number"name="phone_number"value="<?php echo $phone_number;?>"class="regular-text"/>
      </td>
    </tr>
  </table>
  <?php
}

add_action('user_new_form', 'chotu_work_user_new_form', 10, 1);
add_action('show_user_profile', 'chotu_work_user_new_form', 10, 1);
add_action('edit_user_profile', 'chotu_work_user_new_form', 10, 1);

/*creates the email and removes the notification*/
function chotu_work_user_new_form($form_type) {
    ?>
    <script type="text/javascript">
        jQuery('#email').closest('tr').removeClass('form-required').find('.description').remove();
        // Uncheck send new user email option by default
        <?php if (isset($form_type) && $form_type === 'add-new-user') : ?>
            jQuery('#send_user_notification').removeAttr('checked');
            jQuery(document).on('click','#createusersub',function(e){
            event.preventDefault();
            jQuery('#email').val(jQuery('#phone_number').val()+'@chotu.app');
            jQuery("#noconfirmation").attr('checked',true);
            jQuery('#createuser').submit();
              
            });
        <?php endif; ?>
    </script>
    <?php
}

/*updates all custom meta fields*/
add_action( 'personal_options_update', 'save_extra_profile_fields');
add_action( 'edit_user_profile_update', 'save_extra_profile_fields');
add_action( 'user_register', 'save_extra_profile_fields' );
function save_extra_profile_fields( $user_id ) {
  if (!current_user_can('manage_options'))
      return false;
    if(isset($_POST['phone_number'])){
      update_user_meta($user_id, 'phone_number', $_POST['phone_number']);
    }
  //
    if(isset($_POST['field_61079f717fcaa'])){
      $email = esc_attr( $_POST['field_61079f717fcaa'] ).'@chotu.app';
          $args = array(
          'ID'         => $user_id,
          'user_email' => $email
      );
      wp_update_user( $args );
    }
    if(isset($_POST['brand_geography'])){
      update_user_meta($user_id, 'brand_geography', $_POST['brand_geography']);
    }
}

/* updates location data (lat_long, pincode, address & geo_breadcrumb) for any object in in location customtable*/
function chotu_update_location_data($reference_id,$reference_type,$lat_long,$pincode,$full_address,$geography_breadcrumb){
  global $wpdb;
  $chotu_master_locations = 'chotu_master_locations';
  // $lat_long = str_replace(" ", '', $lat_long);
  $lat_long = str_replace(",", ' ', $lat_long);
  if(chotu_checkExist($reference_id,$reference_type) > 0){
    if(isset($lat_long)){
      $wpdb->query("UPDATE $chotu_master_locations SET lat_long=ST_GeomFromText('POINT($lat_long)'), pincode='$pincode', full_address='$full_address',geography_breadcrumb ='$geography_breadcrumb' WHERE reference_id='$reference_id' AND reference_type='$reference_type'" );
    }else{
      $wpdb->query("UPDATE $chotu_master_locations SET geography_breadcrumb='$geography_breadcrumb' WHERE reference_id='$reference_id' AND reference_type='$reference_type'" );
    }
  }else{
    if(isset($lat_long)){
      echo 'dsdsd';
      $wpdb->query("INSERT INTO $chotu_master_locations (`reference_id`, `reference_type`, `lat_long`,`pincode`,`full_address`,`geography_breadcrumb`) VALUES ('$reference_id', '$reference_type', ST_GeomFromText('POINT($lat_long)'),'$pincode','$full_address','$geography_breadcrumb')" );
    }
  }
  return true;
}

/* to fetch the location data for a given object with type and id. (locality, captain, brand)*/
function get_location_object_data_by_type($reference_id,$reference_type){
  global $wpdb;
  $chotu_master_locations = 'chotu_master_locations';
  return $wpdb->get_row("SELECT location_ID,reference_ID,reference_type,pincode,full_address, geography_breadcrumb, GROUP_CONCAT(ST_X(`lat_long`),',',ST_Y(`lat_long`)) AS lat_long FROM $chotu_master_locations WHERE reference_id = '$reference_id' AND reference_type='$reference_type'");
}

/* Add custom image size of 600px by 200px - banner pic*/
add_action( 'after_setup_theme', 'chotu_theme_setup' );
function chotu_theme_setup() {
    add_image_size( 'banner_pic', 600,200,true ); // 300 pixels wide (and unlimited height)
    // add_image_size( 'homepage-thumb', 100, 300, true ); // (cropped)
}

/* We made geography as category. category by default is mult-select. the following function restricts this to a radio button */
add_filter( 'wp_terms_checklist_args', 'chotu_work_term_radio_checklist' );
function chotu_work_term_radio_checklist( $args ) {
    if ( ! empty( $args['taxonomy'] ) && $args['taxonomy'] === 'geography' /* <== Change to your required taxonomy */ ) {
        if ( empty( $args['walker'] ) || is_a( $args['walker'], 'Walker' ) ) { // Don't override 3rd party walkers.
            if ( ! class_exists( 'Chotu_work_Walker_Category_Radio_Checklist' ) ) {
                /**
                 * Custom walker for switching checkbox inputs to radio.
                 *
                 * @see Walker_Category_Checklist
                 */
                class Chotu_work_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
                    function walk( $elements, $max_depth, ...$args ) {
                        $output = parent::walk( $elements, $max_depth, ...$args );
                        $output = str_replace(
                            array( 'type="checkbox"', "type='checkbox'" ),
                            array( 'type="radio"', "type='radio'" ),
                            $output
                        );

                        return $output;
                    }
                }
            }

            $args['walker'] = new Chotu_work_Walker_Category_Radio_Checklist;
        }
    }

    return $args;
}


/*creates a delivery cycle for a brand basis the input in Brand Settings. This is the production cycle of a brand.*/
function create_brand_order_cycle($blog_id,$number_of_time){
  global $wpdb;
  $days = get_all_available_weekday($blog_id);
  $brand_order_cycle = get_site_meta($blog_id,'brand_order_cycle','single');
  $output_dates = array();
  foreach ($days as $key => $value) {
    if($value->meta_value == 1){
      $dayname = getDay($key);
      $output_dates[] = date('Y-m-d', strtotime("next ".$dayname.""));
      if(empty($brand_order_cycle)){
        add_site_meta($blog_id,'brand_order_cycle',date('Y-m-d', strtotime("next ".$dayname."")));
      }
    }else{
      $output_dates[] = '';
    }
  }
  $week_after_date = date('Y-m-d',strtotime("+7 day"));
  
  $data = $wpdb->get_results("DELETE FROM wp_blogmeta WHERE meta_value > '$week_after_date' AND blog_id=$blog_id AND meta_key='brand_order_cycle'");
  $counter = 7;
  if(!empty($output_dates)){
    for ($i = 1; $i < $number_of_time; $i++){
      foreach ($output_dates as $key => $output_date) {
       if($output_date !='' && isset($output_date)){
        add_site_meta($blog_id,'brand_order_cycle',date('Y-m-d',strtotime($output_date." +".$counter." day")));
       }
      }
      $counter += 7;
    }
  }
  return true;
}

function chotu_work_geography_dropdown($geography) { 

$dropdown = wp_dropdown_categories(array('taxonomy' => 'geography', 'name' => 'geography', 'echo' => 0, 'show_option_none' => __('&mdash; Select &mdash;'), 'option_none_value' => '0', 'selected' => $geography, 'hide_empty' => 0, 'orderby' => 'name', 'hierarchical' => 1));
     // Hackily add in the data link parameter.
 $dropdown = str_replace('&nbsp;', '-', $dropdown);
echo $dropdown;
}
function chotu_work_admin_notices(){
  session_start();
 
  if(isset($_SESSION['my_admin_notices'])){
    
    echo $_SESSION['my_admin_notices'];
    unset ($_SESSION['my_admin_notices']);
  }
  
}
add_action( 'admin_notices', 'chotu_work_admin_notices' );
add_action( 'network_admin_notices', 'chotu_work_admin_notices' );

add_filter( 'gettext', 'chotu_work_rename_first_from_user_page', 10, 2 );
function chotu_work_rename_first_from_user_page( $translation, $original )
{
    if ( 'First Name' == $original ) {
        return 'Shop Name';
    }
    return $translation;
}


function get_captain_locality($captain_id){
global $wpdb;
return $wpdb->get_results("SELECT locality_id FROM `chotu_captain_locality` WHERE `captain_ID` =$captain_id");
}
function get_captain_brand($captain_id){
global $wpdb;
return $wpdb->get_results("SELECT brand_id FROM `chotu_captain_brand` WHERE `captain_ID` = $captain_id");
}

if(!is_network_admin()){
  add_action('init','chotu_add_admin_menu_brand_locality');
}

function chotu_add_admin_menu_brand_locality(){

  add_menu_page(
      'brands',
      'Brands',
      'manage_options',
      'brands',
      'show_page_brand',
      'dashicons-store',
      8
    );
  add_menu_page(
      'captains',
      'Captains',
      'manage_options',
      'captains',
      'show_page_captain',
      'dashicons-buddicons-groups',
      7
    );
   $current_user_id = get_current_user_id();
   if($current_user_id){
      $author = get_user_by( 'ID', $current_user_id );

      if(in_array('administrator',$author->roles)){?>
      
    <?php }
   }
      
}

function show_page_brand(){
  return wp_redirect(home_url().'/wp-admin/network/sites.php');
}
function show_page_captain(){
   return wp_redirect(home_url().'/wp-admin/users.php?role=captain');
}
