<?php 
/*
  Add meta fields to captain user profile
*/
if ( in_array( 'captain',$profileuser->roles ) ) {
    // $geography = get_the_author_meta( 'geography', $profileuser->ID );
    $captain_locality = array_column(get_captain_locality($profileuser->ID), 'locality_id');
    $captain_brand = array_column(get_captain_brand($profileuser->ID), 'brand_id');
    $geography = get_location_object_data_by_type($profileuser->ID,'captain');
    // $captain_brand = json_decode(get_user_meta($profileuser->ID, 'captain_brand', 'single' ),true);
    // $captain_locality = json_decode(get_user_meta($profileuser->ID, 'captain_locality', 'single'),true);
?>
<table class="form-table">
   <tr>
    <th><label for="geography"><?php _e("Captain Geography"); ?></label></th>
    <td>
        <!-- <select class="" name="geography" id="geography"> -->
            <?php 
            chotu_work_geography_dropdown($geography->geography_breadcrumb)
             //echo chotu_work_geography_dropdown(array('selected' => $geography));
            //$locality_geography = chotu_get_taxonomy_options('geography',$geography);
            ?>
        <!-- </select> -->
    </td>
</tr>
<tr>
    <th><label for="captain_brand"><?php _e("Brands"); ?></label></th>
    <td>
        <select class="" name="captain_brand[]" id="brand_id" multiple="multiple">
            <?php 
            $sites = get_sites();
            if(!empty($sites)){
             foreach($sites as $site){
                if($site->blog_id !=1 ){
                   $blog_details = get_blog_details($site->blog_id);
                   $term_id = get_site_meta($site->blog_id,'brand_category_subcategory','single');
                   $term = get_term( $term_id, 'brand_category' );
                   if(in_array($site->blog_id, $captain_brand)){
                      echo '<option value="'.$site->blog_id.'" selected>'.$term->name.' > '.$blog_details->blogname.'</option>';
                  }else{
                    echo '<option value="'.$site->blog_id.'">'.$term->name.' > '.$blog_details->blogname.'</option>';
                }
            }
        }
    }

    ?>
</select>
</td>
</tr>
<tr>
    <th><label for="captain_locality"><?php _e("Locality"); ?></label></th>
    <td>
        <select class="" name="captain_locality[]" id="captain_locality" multiple="multiple">
            <?php 
            $localities = get_localities('locality');
            if(!empty($localities)){
                $cat_name='';
                foreach ($localities as $key => $locality) {
                    $term_list = wp_get_object_terms( $locality->ID, 'geography', array( 'fields' => 'names' ));
                    if(!empty($term_list)){
                        $cat_name = $term_list[0];
                    }
                     if(in_array($locality->ID, $captain_locality)){
                        echo '<option value="'.$locality->ID.'" selected>'.$cat_name.' &gt; '.$locality->post_title.'</option>';
                      }else{
                        echo '<option value="'.$locality->ID.'">'.$cat_name.' &gt; '.$locality->post_title.'</option>';
                      }
                }
            }
           
            ?>
        </select>
    </td>
</tr>
<script type="text/javascript">
        jQuery( document ).ready(function( $ ){
            $( '.user-last-name-wrap').remove();
        } );
    </script>
</table>
<?php 
//categoryTree($parent_id = 0, $sub_mark = '');

} 

// function categoryTree($parent_id = 0, $sub_mark = ''){
//     global $db;
//     $taxonomies = array( 
//     'geography',
//       );

//       $args = array(
//           'parent'         => $parent_id,
//           // 'child_of'      => $parent_term_id, 
//       ); 

//       $terms = wp_get_object_terms($taxonomies, $args);
//       dd($terms);
   
//     if($query->num_rows > 0){
//         while($row = $query->fetch_assoc()){
//             echo '<option value="'.$row['id'].'">'.$sub_mark.$row['name'].'</option>';
//             categoryTree($row['id'], $sub_mark.'---');
//         }
//     }
// }
?>