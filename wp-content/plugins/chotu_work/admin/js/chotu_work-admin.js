(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 $(document).ready(function() {
	 		$('#menu-site .wp-first-item > a').text('All Brands');
	 		$("#gated_locality_ID").select2( {
			 	placeholder: "Select gated locality",
			 	allowClear: true
			 } );
			 $("#brand_id").select2( {
			 	placeholder: "Select brand",
			 	allowClear: true
			 } );
			 $("#team_ids").select2( {
			 	placeholder: "Select locality",
			 	allowClear: true
			 } );
			 $("#geography").select2( {
			 	placeholder: "Select geography",
			 	dropdownCssClass : 'bigdrop',
			 	allowClear: true
			 } );
			 $("#brand_category").select2({
			 	dropdownCssClass : 'bigdrop',
			 	dropdownAutoWidth : true
			 	} 
			 	);
		
			 $("#brand_geography").select2( {
			 	placeholder: "Select geography",
			 	dropdownCssClass : 'bigdrop',
			 	allowClear: true
			 } );
			  $("#captain_locality").select2( {
			 	placeholder: "Select Locality",
			 	allowClear: true
			 } );

			  jQuery('#lead_time').on('change',function(){
			 	console.log(jQuery(this).val());
			 	if(jQuery(this).val() == 'other'){
			 		jQuery('.lead_time_custom').removeClass('hidden');
			 	}else{
			 		jQuery('.lead_time_custom').addClass('hidden')
			 	}
			 });

			  jQuery('#btnRight_code').on('click',function (e) {
				 	jQuery(this).prev('select').find('option:selected').remove().appendTo('#isselect_code');
				 });
				 jQuery('#btnLeft_code').on('click',function (e) {
				 	jQuery(this).next('select').find('option:selected').remove().appendTo('#canselect_code');
				 });
	 	});
	 jQuery('#phone_number').on('keypress', function(e) {
	 	var $this = jQuery(this);
	 	var regex = new RegExp("^[0-9\b]+$");
	 	var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	     // for 10 digit number only
	     if ($this.val().length > 9) {
	     	e.preventDefault();
	     	return false;
	     }
	     if (e.charCode < 54 && e.charCode > 47) {
	     	if ($this.val().length == 0) {
	     		e.preventDefault();
	     		return false;
	     	} else {
	     		return true;
	     	}
	     }
	     if (regex.test(str)) {
	     	return true;
	     }
	     e.preventDefault();
	     return false;
	 });

	
	 $(document).on('click', 'form input[type=submit]', function(e) {
	 	if($('#brand_pincode').val()){
	 		var isValid = $('#brand_pincode').val().length;
		    if(isValid !=6 ) {
		      e.preventDefault(); //prevent the default action
		      $('#brand_pincode').focus();
		      return false;
			}
	 	}
    	
	});

	 // function validate_pincode(pincode){
	 	
	 // 	var isValid = $('#'+pincode).val().length;
	 //    if(isValid < 6) {
	 //      e.preventDefault(); //prevent the default action
	 //      $('#'+pincode).focus();
	 //      return false;
		// }
	 // }
	 
	})( jQuery );
