<?php 
/*
Add 3-level address syntax to a locality. All other fields are added through ACF.
*/

$full_postal_address=$pincode=$lat_long=$level_1_name=$level_1_type=$level_1_options=$level_2_name=$level_2_type=$level_2_options=$level_3_name=$level_3_type=$level_3_options='';
$ad_syntax = get_post_meta($post->ID,'locality_address_syntax',true);
$address_syntax = isset($ad_syntax) ? json_decode($ad_syntax,true) : '';
if(!empty($address_syntax)){
    $level_1_name =  isset($address_syntax['level_1']['level']) ? $address_syntax['level_1']['level'] : '';
    $level_1_type =  isset($address_syntax['level_1']['type']) ? $address_syntax['level_1']['type'] : '';
    $level_1_options =  isset($address_syntax['level_1']['options']) ? $address_syntax['level_1']['options']: '';
    $level_2_name =  isset($address_syntax['level_2']['level']) ? $address_syntax['level_2']['level'] : '';
    $level_2_type =  isset($address_syntax['level_2']['type']) ? $address_syntax['level_2']['type'] : '';
    $level_2_options =  isset($address_syntax['level_2']['options']) ? $address_syntax['level_2']['options'] : '';
    $level_3_name =  isset($address_syntax['level_3']['level']) ? $address_syntax['level_3']['level'] : '';
    $level_3_type =  isset($address_syntax['level_3']['type']) ? $address_syntax['level_3']['type'] : '';
    $level_3_options =  isset($address_syntax['level_3']['options']) ? $address_syntax['level_3']['options'] : '';
}
?> 
<div class="acf-field acf-field-text custom_width">
   
    <h3>Level 1 (* Mandatory)</h3>
    <label for="level_1_name">Label Name</label>
    <div class="acf-inputs">
        <div class="acf-input-wrap"><input type="text" placeholder="Label Name" title="lable name" name="level_1_name" required="" value="<?php echo $level_1_name;?>">
        </div>
    </div>
    
    <label for="level_1_type"></label>
    <div class="acf-inputs">
        <div class="acf-input-wrap">
            <select name="level_1_type" onchange="append_option_box(this,'level_1');" required="">
                <option>Select option</option>
                <option value="text" <?php if($level_1_type == 'text'){ echo 'selected'; }?>>Text</option>
                <option value="option" <?php if($level_1_type == 'option'){ echo 'selected';}?>>Option</option>
                <option value="textarea" <?php if($level_1_type == 'textarea'){ echo 'selected';}?>>Textarea</option>
            </select>
        </div>
    </div>
    <div class="acf-field">
    <?php 

    if($level_1_type == 'option'){?>
        <textarea class="option_values" rows="2"  name="level_1_values" placeholder="add options by comma separated"><?php echo $level_1_options;?></textarea>
    <?php }else{?>
        <textarea class="option_values" rows="2" style="display: none;" name="level_1_values" placeholder="add options by comma separated"></textarea>
    <?php }?>
    </div>
    </div>
    <div class="acf-field acf-field-text custom_width">
    <h3>Level 2 (optional)</h3>
    <label for="lat_long">Label Name</label>
    <div class="acf-inputs">
    <input type="text" placeholder="Label Name" title="lable name" name="level_2_name" value="<?php echo $level_2_name?>">
    </div>
    <div class="acf-inputs">
    <!-- <label for="lat_long">Input type</label> -->
    <select name="level_2_type" class="short" onchange="append_option_box(this,'level_2');">
        <option>Select option</option>
        <option value="text" <?php if($level_2_type == 'text') echo 'selected';?>>Text</option>
        <option value="option" <?php if($level_2_type == 'option') echo 'selected';?>>Option</option>
        <option value="textarea" <?php if($level_2_type == 'textarea') echo 'selected';?>>Textarea</option>
    </select>
    </div>
     <div class="acf-field">
    <?php if($level_2_type == 'option'){?>
        <textarea class="option_values" rows="2"  name="level_2_values" placeholder="add options by comma separated"><?php echo $level_2_options;?></textarea>
    <?php }else{?>
        <textarea class="option_values" rows="2" style="display: none;" name="level_2_values" placeholder="add options by comma separated"></textarea>
        <?php }?>
    </div>
    </div>
    <div class="acf-field acf-field-text custom_width">
   
        <h3>Level 3 (optional)</h3>
        <label for="lat_long">Label Name</label>
        <div class="acf-inputs">
        <input type="text" placeholder="Label Name" title="lable name" name="level_3_name" value="<?php echo $level_3_name?>">
    </div>
    <div class="acf-inputs">
        <!-- <label for="lat_long">Input type</label> -->
        <select name="level_3_type" class="short" onchange="append_option_box(this,'level_3');">
            <?php 
            ?>
            <option>Select option</option>
            <option value="text" <?php if($level_3_type == 'text') echo 'selected';?>>Text</option>
            <option value="option" <?php if($level_3_type == 'option') echo 'selected';?>>Option</option>
            <option value="textarea" <?php if($level_3_type == 'textarea') echo 'selected';?>>Textarea</option>
        </select>
        <div class="acf-field">
        <?php if($level_3_type == 'option'){?>
           
            <textarea class="option_values" rows="2" name="level_3_values" placeholder="add options by comma separated"><?php echo $level_3_options;?></textarea>
        <?php }else{?>
            <textarea class="option_values" rows="2"style="display: none;" name="level_3_values" placeholder="add options by comma separated"></textarea>
            <?php }?>
        </div>
    </div>
</div>
<script type="text/javascript">
    function append_option_box(e,level){
    if(jQuery(e).val() == 'option'){
        jQuery('textarea[name="'+level+'_values"]').css('display','block');
    }else{
        jQuery('textarea[name="'+level+'_values"]').css('display','none');
    }
    
}
</script>