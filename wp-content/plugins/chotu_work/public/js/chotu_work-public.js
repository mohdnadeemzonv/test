(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 $( document ).on( 'change', 'input[name="locality_id"]', update_locality_id_to_url_param );

	  $(document).on('blur','#login_username',function(e) {
        if (validatePhone('login_username')) {
            $('#phone-error').html('Valid Mobile Number');
            $('#phone-error').css('color', 'green');
            $('#login_btn').attr('disabled',false);
        }
        else {
            $('#phone-error').html('Invalid Mobile Number');
            $('#phone-error').css('color', 'red');
            $('#login_username').focus();
            $('#register_btn').attr('disabled',true);
            
        }
    });
	  $(document).on('blur','#register_username',function(e) {
        if (validatePhone('register_username')) {
            $('#register-error').html('Valid Mobile Number');
            $('#register-error').css('color', 'green');
            $('#register_btn').attr('disabled',false);
        }
        else {
            $('#register-error').html('Invalid Mobile Number');
            $('#register-error').css('color', 'red');
            $('#register_username').focus();
            $('#register_btn').attr('disabled',true);
            
        }
    });
	  function validatePhone(txtPhone) {
		    var a = document.getElementById(txtPhone).value;
		    var filter = /^[0]?[7869]\d{9}$/;
		    if (filter.test(a)) {
		        return true;
		    }
		    else {
		        return false;
		    }
		}
	 
	 function update_locality_id_to_url_param(locality_id){

	 	var locality_id = ($(this).val()) ? $(this).val() : locality_id;
	 	console.log(locality_id);
	 	$.each($('.url_param a'), function(index, value) {
		  var url =  new URL($(value).attr('href'));
		  url.searchParams.set('l', locality_id);
		  $(value).attr('href',url.href);
		  $(value).removeClass('disabled');
		});
	 	//
	 }
	 $(document).ready(function(){
	 	if(location.search.split('l=')[1]){
	 		var locality_id = location.search.split('l=')[1];
	 		$.each($('input[name="locality_id"]'), function(index, value) {
	 			if($(value).val() == locality_id){
	 				$(this).attr('checked',true);
	 				update_locality_id_to_url_param(locality_id);
	 			}
			});
	 	}

		

/************* start timer for lest order time******************/
			$(".order_before_time_card").each(function(index,value){
				var last_date_of_order = jQuery('.order_before_time_card').eq(index).attr('data-card');
				console.log('last_date_of_order'+last_date_of_order)
				if(last_date_of_order != ''){
					var countDownDate = new Date(last_date_of_order).getTime();

		          // Update the count down every 1 second
		          var x = setInterval(function() {

		            // Get today's date and time
		            var now = new Date().getTime();
		              
		            // Find the distance between now and the count down date
		            var distance = countDownDate - now;
		              // console.log('countDownDate '+countDownDate);
		              // console.log('now '+now  );
		              // console.log('distance'+distance);
		            // Time calculations for days, hours, minutes and seconds
		            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		              
		            // Output the result in an element with id="demo"
		            jQuery('.timer').eq(index).html(' '+ days + "d " + hours + "h "
		            + minutes + "m " + seconds + "s ");
		              
		            // If the count down is over, write some text 
		            if (distance < 0) {
		              clearInterval(x);
		              jQuery('.timer').eq(index).html(": Expired");
		            }
		          }, 1000);
		      }else{
		      	jQuery('.timer').eq(index).html(": N/A");
		      }
		            
			});
            


/**************************************************/
	$(document).on('click','#register_btn',function(){
        console.log('act 1');
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(idehweb_lwp.loadingmessage);
        var action = 'lwp_ajax_login';
        var username = $('#login_username').val();
        username = username.replace(/^0+/, '');
        var lwp_country_codes = 91;
        username = lwp_country_codes + username;
        var ctrl = $(this);
        $.ajax({
            // type: 'GET',
            dataType: 'json',
            url: idehweb_lwp.ajaxurl,
            data: {
                'action': action,
                'username': username,
                // 'password': password,
                // 'email': email,
                // 'security': security
            },
            success: function (data) {
                if (data && data.ID)
                    idehweb_lwp.UserId = data.ID;

                // $('p.status', ctrl).text(data.message);
                if (data.success == true) {
                    if (data.authWithPass && data.showPass) {
                        $('#lwp_login_email').fadeOut(10);
                        $('#lwp_login').fadeOut(10);
                        $('#lwp_enter_password').fadeIn(500);

                    } else {

                        runFBase("+" + username);


                    }
                    //     document.location.href = idehweb_lwp.redirecturl;
                }
            }
        });
        e.preventDefault();
	 });
	});
	
})( jQuery );
