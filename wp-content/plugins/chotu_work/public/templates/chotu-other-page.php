<?php
/*
 * Template Name: Chotu default Page
 * description: >-
  Page template without sidebar
 */

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>HomePage</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Inter&display=swap" rel="stylesheet" />

  <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.css" />
  <link rel="stylesheet" href="/path/to/flickity.css" media="screen" />
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" />

  <!-- My Stylesheets -->
  <link href="./home.css" rel="stylesheet" />
  <style>
    @media only screen and (min-width: 620px ) and (max-width:1000px){
    .mobileView{
        width: 60%;
    } 
}
@media only screen and (min-width: 1001px){
         .mobileView{
        width: 30%;
      } 
}

.navbar{
  background: rgba(249, 249, 249, 0.5);
  backdrop-filter: blur(5px);
}

.iconify{
  position: absolute;
  height:20px;
  width:20px;
  margin-left: 34px;
  margin-top:34px;
  color:#737373;
}

  </style>
</head>

<body>
  <section class="mobileView m-auto">
    <nav class="navbar navbar-dark sticky-top">
      <!-- toggler button -->

      <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#Toggler"
        aria-controls="Toggler" aria-expanded="true" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "><img src="./images/drawer.png" alt="" /></span>
      </button>

      <a class="navbar-brand ms-4" href="#"><img height="80" width="80" src="http://tiktik.in/wp-content/uploads/2021/07/chotu-logo.png" /></a>

      <!-- Login Button-->
      <a href="#" class="btn login font-family-inter font-weight-normal btn-primary">Login</a>
      <div class="collapse bg-white navbar-collapse" id="Toggler">
        <ul class="navbar-nav mx-auto text-center">
          <li class="nav_item d-flex justify-content-center align-items-center border-bottom">
            <a class="nav-link   text-dark" href="#">Add a Brand</a>
          </li>
          <li class="nav_item">
            <a class="nav-link text-dark" href="#">Become a Captain</a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- Heading Description -->
    <div style="background-color: #f9f9f9; height:14rem;"  class="text-center font-family-karla">
      <p class="font-weight-bold fs-3">Let's Buy Together</p>
      <p class="font-family-inter">Finest quality | better prices | safer deliveries</p>

      <!-- search Bar -->
      <form class="d-flex">
        <input class="form-control w-100 ms-3 my-3" type="search"
          placeholder="&ensp;&ensp;&ensp;&ensp; Search Community" aria-label="Search">
        <span class="iconify" data-icon="akar-icons:search" data-inline="false"></span>

        </input>
        <!-- search Button -->
        <button style="width: 56px; height: 56px; background-color: #1d6aff" class="btn btn-primary mx-3 my-3"
          type="submit">
          <!-- <span  class="iconify" data-icon="carbon:location-filled" data-inline="false"></span> -->
          <span>
            <img src="./images/locaIcon.svg" alt="">
          </span>
        </button>
      </form>
      <p class="fs-6">
        for better result give location access, please
        <a class="allow" href="">Allow</a>
      </p>
    </div>
    <!-- COMMUNITY CARD-->
    <!-- card with DP -->
    <div class="mt-5 ms-3 fs-5 font-family-karla font-weight-bold">
      Communities near me
    </div>

    <div class="carousel overflow-hidden" data-flickity='{"freeScroll":true, "autoPlay":false}'>
      <div style="height:300px; width:100%" class="carousel-cell  d-flex justify-content-center align-items-center">
        <div class="w-100 bottom-0 h-75 position-absolute" style="background: #FAFFDC"></div>
        <div class="card shadow" style="width: 19.938rem; height:12.625rem;">
          <img class="position-absolute" src="./images/image3.png" alt="...">
        
          <div type="button" class="btn btn-sm end-0 no-border position-absolute" onclick="alert('Share On Whatsapp!')">
            <img src="./images/Share.png" alt="">
          </div>
          <div class="position-absolute" style="top: 84px; left: 22px;">
            <img height="58" width="58" src="./images/captain.png" alt="">
          </div>
          <span class="position-absolute font-family-karla font-weight-bold fs-5" style="top: 113px; left: 96px;">Safal
            Vegetable...</span>
          <div class="card-body">


            <a href="#"
              class="btn btn-primary btn-sm  px-3 border-0 position-absolute end-0 bottom-0 mb-2 mx-3">View</a>
            <div class="position-absolute bottom-0 my-3">
              <img src="./images/truck.svg" alt="">
            </div>
            <p class="position-absolute bottom-0 mx-4 my-3 text-muted"><small>07 Aug, Sat | min: 40 orders</small>
            </p>
          </div>

        </div>
        <p class="position-absolute bottom-0 fs-5">
          <small> didn't find your locality? please
            <a href="">Request Here</a>
          </small>

        </p>
      </div>
      
      
      
    </div>


    <div class="mt-5 ms-3 fs-5 font-family-karla font-weight-bold">
      Communities near me
    </div>
   <div class="carousel overflow-hidden" data-flickity='{"freeScroll":true, "autoPlay":false}'>
    
        <div style="height:300px; width:100%" class="carousel-cell  d-flex justify-content-center align-items-center active" >
          <div class="w-100 bottom-0 h-75 position-absolute" style="background: #FFF3DC"></div>
          <div class="card shadow" style="width: 19.938rem; height:12.625rem;">
            <img class="position-absolute" src="./images/image3.png" alt="...">
            
            <div type="button" class="btn btn-sm end-0 no-border position-absolute"
              onclick="alert('Share On Whatsapp!')">
              <img src="./images/Share.png" alt="">
            </div>
            <div class="position-absolute" style="top: 84px; left: 22px;">
              <img height="58" width="58" src="./images/captain.png" alt="">
            </div>
            <span class="position-absolute font-family-karla font-weight-bold fs-5"
              style="top: 113px; left: 96px;">Safal Vegetable..</span>
            <div class="card-body">


              <a href="#"
                class="btn btn-primary btn-sm  px-3 border-0 position-absolute end-0 bottom-0 mb-2 mx-3">View</a>
              <div class="position-absolute bottom-0 my-3">
                <img src="./images/truck.svg" alt="">
              </div>
              <p class="position-absolute bottom-0 mx-4 my-3 text-muted"><small>07 Aug, Sat | min: 40 orders</small>
              </p>
            </div>
          </div>
          <p class="position-absolute bottom-0 ">
            <small> didn't find your locality? please
              <a href="">Request Here</a>
            </small>

          </p>

      </div>
       <div style="height:300px; width:100%" class="carousel-cell  d-flex justify-content-center align-items-center active" >
          <div class="w-100 bottom-0 h-75 position-absolute" style="background: #FFF3DC"></div>
          <div class="card shadow" style="width: 19.938rem; height:12.625rem;">
            <img class="position-absolute" src="./images/image3.png" alt="...">
            
            <div type="button" class="btn btn-sm end-0 no-border position-absolute"
              onclick="alert('Share On Whatsapp!')">
              <img src="./images/Share.png" alt="">
            </div>
            <div class="position-absolute" style="top: 84px; left: 22px;">
              <img height="58" width="58" src="./images/captain.png" alt="">
            </div>
            <span class="position-absolute font-family-karla font-weight-bold fs-5"
              style="top: 113px; left: 96px;">Safal Vegetable..</span>
            <div class="card-body">


              <a href="#"
                class="btn btn-primary btn-sm  px-3 border-0 position-absolute end-0 bottom-0 mb-2 mx-3">View</a>
              <div class="position-absolute bottom-0 my-3">
                <img src="./images/truck.svg" alt="">
              </div>
              <p class="position-absolute bottom-0 mx-4 my-3 text-muted"><small>07 Aug, Sat | min: 40 orders</small>
              </p>
            </div>
          </div>
          <p class="position-absolute bottom-0 ">
            <small> didn't find your locality? please
              <a href="">Request Here</a>
            </small>

          </p>

      </div>
      
    </div>

    <!-- Normal Card -->
    <p class="mt-5 ms-3 fs-5 font-family-karla font-weight-bold">Why Buy on Chotu</p>
    <div class="carousel overflow-hidden" data-flickity='{"freeScroll":true, "autoPlay":false}'>
      <div style="height:300px; width:100%" class="carousel-item  d-flex justify-content-center align-items-center">
        <div class="w-100 bottom-0 h-75 position-absolute" style="background: #dce8ff"></div>
        <div class="card shadow" style="width: 19.938rem; height:12.625rem;">

          <div class="font-family-karla font-weight-bold mt-3 ms-4 fs-5">
            Best Quality
          </div>
          <div class="mx-4 w-50 ">
            <small> We understand that while you had a choice to stay elsewhere,</small>
            <div class="my-3 top-0 end-0 position-absolute">
              <img src="./images/normalCardImg.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <p class="mt-5 ms-3 fs-5 font-family-karla font-weight-bold">Why Sell on Chotu</p>
    <div class="carousel overflow-hidden" data-flickity='{"freeScroll":true, "autoPlay":false}'>
      <div style="height:300px; width:100%" class="carousel-item  d-flex justify-content-center align-items-center">
        <div class="w-100 bottom-0 h-75 position-absolute" style="background: #FFE9DC"></div>
        <div class="card shadow" style="width: 19.938rem; height:12.625rem;">

          <div class="font-family-karla font-weight-bold mt-3 ms-4 fs-5">
            Quality Customers
          </div>
          <div class="mx-4 w-50">
            Get access to discerning consumers located on one place.
            <div class=" bottom-0 end-0 position-absolute">
              <img src="./images/normalCardImg2.png" alt="">
            </div>
          </div>
        </div>
        <p class="position-absolute bottom-0">
          Become a
          <a href="">Captain Now</a>
        </p>
      </div>
    </div>
    
    <!-- Footer -->
    <footer class="py-4" style="background-color: #f9f9f9">
      <div class="container sticky-bottom">
        <nav class="d-flex justify-content-center flex-column align-items-center pt-5">
          <img src="./images/chotu.svg" alt="Logo" />
          <div class="mt-3">

            <p class=" text-color-dark d-flex justify-content-center">
              <a href="" class="text-reset"> About us&nbsp; </a>&nbsp; |
              &nbsp;<a href="" class="text-reset">Our Story</a>&nbsp; | &nbsp;<a href=""
                class="text-reset">Team</a>&nbsp; | &nbsp;<a href="" class="text-reset">Privacy</a>
            </p>
            <p class="text-color-dark d-flex justify-content-center">
              <a href="" class="text-reset">Refunds</a>&nbsp; | &nbsp;<a href="" class="text-reset">Add a
                Brand</a>&nbsp; | &nbsp;<a href="" class="text-reset">Become a Captain</a>
            </p>
          </div>
        </nav>
      </div>
    </footer>
    
    <!-- BOTTOM COPYRIGHT FOOTER -->
    <nav class="navbar navbar-expand-lg" style="background-color: #0e4c75">
      <div class="container-fluid justify-content-center">
        <a class="navbar-brand" href="#"
          style="font-family: inter; font-size:12px;font-weight: medium; color: #ffffff">Copyright &copy; 2021 Chotu</a>
      </div>
    </nav>
  </section>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.pkgd.min.js">
  </script>
  <script src="/path/to/flickity.pkgd.min.js"></script>
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
  </script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
  </script>
 
</body>

</html>