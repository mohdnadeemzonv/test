<?php 
/* Template Name: Dashboard Central Page
*/
if(!is_user_logged_in()){
	wp_redirect(home_url());
}
get_header();
global $wpdb;

## ==> Define HERE the customer ID
$customer_user_id = get_current_user_id(); // current user ID here for example
$orders = $wpdb->get_results("SELECT * FROM chotu_orders WHERE customer_ID='$customer_user_id'");

// Getting current customer orders


?>

<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
	<div class="table-responsive">	
	<table id="customer_orders" class="table table-striped">
		<thead>
			<tr>
				<!-- <th> Order ID </th> -->
				<!-- <th> Order Date </th> -->
				<!-- <th> Logo </th> -->
				<!-- <th> Vendor Name </th> -->
				<th> Order Total</th>
				<!-- <th> Mode Of Payment</th> -->
				<th> DOD</th>
				<th> Download invoice</th>
				<!-- <th> Actions</th> -->
			</tr>
		</thead>
		<tbody>
			<?php 
			if(!empty($orders)){
				foreach ($orders as $key => $order) {
						// $url = site_url().'/'.trim($order->vendor_name).'/my-account/view-order/'.$order->order_id;
					$blog_id =  $order->brand_ID;
					$blog_details = get_blog_details($blog_id);
					$pdf_url = wp_nonce_url($blog_details->siteurl.$blog_details->path.'wp-admin/admin-ajax.php?action=generate_wpo_wcpdf&document_type=invoice&order_ids=' . $order->order_ID . '&my-account', 'generate_wpo_wcpdf' );
					?>
					<tr>
						 <!-- <td> #<?php echo $order->order_ID;?></td> -->
						 <!-- <td> <?php echo esc_html( date_i18n( $order->order_date ) ); ?> </td> -->
						<!-- <td> <?php echo get_blog_option( $blog_details->blog_id, 'blogname' );?> </td> -->
						 <!-- <td> <?php echo $order->vendor_name;?> </td> -->
						<!-- <td> <?php echo $order->order_status;?> </td> -->
						<td> <?php echo $order->order_total;?> </td>
						<!-- <td> <?php echo get_brand_post_meta($order->order_ID,'_payment_method_title',$blog_id)?> </td> -->
						<td> <?php echo date('F j, D',strtotime(get_brand_post_meta($order->order_ID,'date_of_delivery',$blog_details->blog_id)))?> </td>
						<!-- <td> <a  href="<?php echo home_url().'/'.trim($order->vendor_name)?>/my-account/orders/" class="btn btn-info">View Orders</button> </td> -->
						<td><a href="<?php echo esc_attr($pdf_url); ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
					</tr>
				<?php }
			}

			?>
		</tbody>
	</tbody>
	<tfoot>
		<tr>
			<!-- <th> Order ID </th> -->
			<!-- <th> Order Date </th> -->
			<!-- <th> Logo </th> -->
			<!-- <th> Vendor Name </th> -->
			<th> Order Total</th>
			<!-- <th> Mode Of Payment</th> -->
			<th> DOD</th>
			<th> Download invoice</th>
		</tr>
	</tfoot>
</table>
</div>
<div id="myModals" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="order_detail">
      
    </div>

  </div>
</div>
<?php
get_footer();

?>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#customer_orders').DataTable();
		// $('#customer_order').DataTable();

		 // $( document ).on( 'click', '#manage_gallery', upload_gallery_button );
	} );


	function view_orders(blog_id){
	$.ajax({
             type : "POST",
             url : ajaxurl,
             data : {'blog_id':blog_id,action: "get_shop_orders_by_blog"},
             success: function(response) {
             	$('#order_detail').html(response);
             	$('#myModals').modal('show');
                }
        });
}
</script>

				