<?php //get_header();
/*
    Author Page (CAPTAIN) front-end
*/
    global $wp;
    $current_url = home_url( add_query_arg( array(), $wp->request ) );
    get_header();
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    $captain_marquee = get_the_author_meta( 'captain_marquee', $curauth->ID );
    $captain_address = get_the_author_meta( 'captain_address', $curauth->ID );
    $attachment_id = get_the_author_meta('captain_banner_pic',$curauth->ID );
    $captain_square_pic = get_custom_attachment_image(get_the_author_meta('captain_square_pic',$curauth->ID ));
    $banner_image = get_custom_attachment_image($attachment_id);
    if(empty($banner_image) && $banner_image == ''){
      $banner_image = plugins_url().'/chotu_work/assets/images/teamscreen.png';
    }
    if($captain_square_pic == ''){
      $captain_square_pic = plugins_url().'/chotu_work/assets/images/chotu_logo.png';
    }
  $brands = get_brands_by_captains($curauth->ID);
  $localities = get_localities_by_captains($curauth->ID);
?>
    <!-- <div >  </div>  -->
    <div class="position-relative w-100">
      <div type="button"><a href="https://api.whatsapp.com/send/?phone&text=<?php echo $current_url;?>" target="_blank"><img src="<?php echo plugins_url()?>/chotu_work/assets/images/Share.png"class="position-absolute end-0 border-0 share-button-banner" alt=""></a></div>
      
      <img class="card-img-top" style="aspect-ratio:3/1; object-fit:cover;filter: brightness(0.5);" src="<?php echo $banner_image; ?>">
      <div class="position-absolute" style="top: 73px; left: 139px;">
        <img class="img-circle d-flex m-auto" height="90" width="90" style="border-radius:50%;border: 1px solid;" src="<?php echo $captain_square_pic?>" alt="">
      </div>
      <div class="caption_banner"><p><?php echo strtoupper($curauth->nickname)?></p></div>
    </div>
    <div class="text-center pt-4" style="height:auto;background-color:#f9f9f9;">
      <h2>
        <div class="fs-6 pt-3">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-geo-alt-fill"
            viewBox="0 0 16 16">
            <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z" />
          </svg>
          <span class="ml-3"><?php echo $captain_address;?></span>
        </div>
      </h2>
      <div style="" class="w-75 fs-6 text-center mt-3 m-auto mb-3">
          <?php echo $curauth->user_description; ?>
      </div>
    </div>
    <?php
        if(isset($captain_marquee) && $captain_marquee !=''){
            echo '<marquee width="100%" direction="left">'.$captain_marquee.'</marquee>';
        }?>
      <div class="all_radio_data_wrap">
        <h5>Please choose a locality to shop from</h5>
        <div class="row">
<?php
        if(!empty($localities)){
            foreach ($localities as $key => $locality) {
              $s_locality = get_post($locality->locality_id);
              if(!empty($s_locality)){
                echo ' <div class="col-4"><label class="radio-img"><input type="radio" name="locality_id" value="'.$locality->locality_id.'"> <div class="image_radio"><img class="img-fluid" src="'.wp_get_attachment_image_url(get_post_meta($locality->locality_id,'locality_banner_pic',true),'banner_pic').'" alt="image"> <div class="caption_r"><p class="mx-auto">'.strtoupper(get_the_title($locality->locality_id)).'</p></div></div></label></div>';
              }
        //      echo '<input type="radio" name="locality_id" value="'.$locality->locality_id.'" id="locality_'.$locality->locality_id.'">&nbsp;&nbsp;'.get_the_title($locality->locality_id).'&nbsp;&nbsp;';
            }
          }
    ?>
     </div>
  </div>
    <div class="mt-5 ms-3 fs-3 mb-3 font-family-karla font-weight-bold">
      Top Brands
    </div>
    <!-- cards -->
        <?php
        $box_start = '<div class="carousel overflow-hidden "><div style="height:100%; width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 position-absolute" style="background: #FAFFDC; min-height:100%; width:100%; "></div>';// '';
       $box_end = '</div></div>'; //</div></div>
        echo $box_start;
       echo chotu_work_make_brand_card($brands,'','','',$curauth->ID,'disabled');
        echo $box_end;

get_footer();