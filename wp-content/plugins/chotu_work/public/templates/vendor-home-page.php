<?php
/* Template name: Vendor Home Page*/

//include('functions.php');
get_header();
global $wpdb;
$wp_posts = $wpdb->prefix . "posts";
$current_date = date('Y-m-d');
$expiry_date = strtotime("now");
$wp_postmeta = $wpdb->prefix . "postmeta";
$blog_id = get_current_blog_id();
    // Get an array of all existing coupon codes
$coupon_codes = $wpdb->get_results("SELECT * FROM `wp_central_coupon` WHERE `expiry_date` >= $current_date AND blog_id = $blog_id ORDER BY `expiry_date` DESC"); 
    // Display available coupon codes
?>
<main id="content">
	<div class="container" id="custom-a-tag">
		<div id="primary" class="content-area entry-content">
			<main id="main" class="site-main" role="main">
				<div class="wc-block-grid wp-block-product-new wc-block-product-new has-3-columns has-multiple-rows">
					<div class="row">
						<h1>Active Pins</h1>
					</div>
					<ul class="wc-block-grid__products">
						<?php 
							if(!empty($coupon_codes)){
								foreach ($coupon_codes as $key => $coupon_code) {
								 $team_id = get_post_meta($coupon_code->post_id,'team_id',true);

								 $team_name = '';
								 $discount_type = get_post_meta($coupon_code->post_id,'discount_type',true);
								 if($discount_type == 'percent'){
								 	$type = '%';
								 }else if($discount_type == 'fixed_cart'){
								 	$type = 'Rs';
								 }
								?>
								<li class="wc-block-grid__product">
								<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>?coupon_code=<?php echo get_the_title($coupon_code->post_id);?>" class="wc-block-grid__product-link">
									<div class="wc-block-grid__product-image">
										<img width="300" height="200" src="http://chotu.shop/shop1/wp-content/uploads/sites/2/2021/05/6-Ways-to-Improve-your-Coupon-Marketing-Strategy-and-Increase-Sales.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="<?php echo $team_name;?>"></div>
										
									</a>
									<strong>Code:</strong>
										<div class="wc-block-grid__product-title">
										  <?php echo get_the_title($coupon_code->post_id);?></div>
									<div class="wc-block-grid__product-title"><span class=""><span class=""></span><?php echo get_post_meta($coupon_code->post_id,'coupon_amount',true).$type;?></span></div>
									<div class=""><span class=""><span class=""></span><a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>?coupon_code=<?php echo get_the_title($coupon_code->post_id);?>"><?php echo $team_name;?></a></span></div>

								</li>
								<?php }
							}

						?>
						
						</ul></div>
					</main>
				</div>
			</div>
		</main>

		<?php
		get_footer();
		?>