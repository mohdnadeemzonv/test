  <!-- Footer -->
    <footer class="py-4" style="background-color: #f9f9f9">
      <div class="container sticky-bottom">
        <nav class="d-flex justify-content-center flex-column align-items-center pt-5">
          <img src="<?php echo plugins_url()?>/chotu_work/assets/images/chotu.svg" alt="Logo" />
          <div class="mt-3">
            <p class=" text-color-dark d-flex justify-content-center">
              <a href="" class="text-reset"> About us&nbsp; </a>&nbsp; |
              &nbsp;<a href="" class="text-reset">Our Story</a>&nbsp; | &nbsp;<a href=""
                class="text-reset">Team</a>&nbsp; | &nbsp;<a href="" class="text-reset">Privacy</a>
            </p>
            <p class="text-color-dark d-flex justify-content-center">
              <a href="" class="text-reset">Refunds</a>&nbsp; | &nbsp;<a href="" class="text-reset">Add a
                Brand</a>&nbsp; | &nbsp;<a href="" class="text-reset">Become a Captain</a>
            </p>
          </div>
        </nav>
      </div>
    </footer>
    
    <!-- BOTTOM COPYRIGHT FOOTER -->
    <nav class="navbar navbar-expand-lg" style="background-color: #0e4c75">
      <div class="container-fluid justify-content-center">
        <a class="navbar-brand" href="#"
          style="font-family: inter; font-size:12px;font-weight: medium; color: #ffffff">Copyright &copy; 2021 Chotu</a>
      </div>
    </nav>
  
  </section>
  <?php wp_footer()?>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.pkgd.min.js">
  </script>
  <script src="/path/to/flickity.pkgd.min.js"></script>
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
  </script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
  </script>
  <script>
$(document).ready(function(){
	$(document).on('click','.signup-tab',function(e){
		e.preventDefault();
	    $('#signup-taba').tab('show');
	});	
	
	$(document).on('click','.signin-tab',function(e){
	   	e.preventDefault();
	    $('#signin-taba').tab('show');
	});
	    	
	$(document).on('click','.forgetpass-tab',function(e){
	 	e.preventDefault();
	   	$('#forgetpass-taba').tab('show');
	});
});	
</script>
</body>

</html>