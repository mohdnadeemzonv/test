<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php the_title()?></title>
  <?php
    wp_head();
    ?>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Inter&display=swap" rel="stylesheet" />
  <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.css" />
  <link rel="stylesheet" href="/path/to/flickity.css" media="screen" />
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

  <!-- My Stylesheets -->
  <link href="./home.css" rel="stylesheet" />
  <style>
    @media only screen and (min-width: 620px ) and (max-width:1000px){
    .mobileView{
        width: 60%;
   } 
  }
  @media only screen and (min-width: 1001px){
  .mobileView{
      width: 30%;
   } 
  }
  .navbar{
    background: rgba(249, 249, 249, 0.5);
    backdrop-filter: blur(5px);
  }
  .iconify{
    position: absolute;
    height:20px;
    width:20px;
    margin-left: 34px;
    margin-top:34px;
    color:#737373;
  }

  </style>
  <?php
    $localize = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'redirecturl' => '',
            'UserId' => 0,
            'loadingmessage' => __('please wait...', ''),
        );
  ?>

</head>