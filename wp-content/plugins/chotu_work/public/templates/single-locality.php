<?php
get_header();
//echo do_shortcode('[all_captains]');
  $locality_id = get_the_ID();
  $title = get_the_title();
  $locality_address = get_post_meta($locality_id,'locality_address',true);
  $locality_marquee = get_post_meta($locality_id,'locality_marquee',true);
  $attachment_id = get_custom_post_meta($locality_id,'locality_banner_pic');
  $banner_image = get_custom_attachment_image($attachment_id,'');
  if(empty($banner_image) && $banner_image == ''){
    $banner_image = plugins_url().'/chotu_work/assets/images/teamscreen.png';
  }

?>
  <!-- <div >  </div>  -->
    <div class="position-relative w-100">
      <div type="button"><a href="https://api.whatsapp.com/send/?phone&text=<?php echo get_permalink($locality_id);?>" target="_blank"><img src="<?php echo plugins_url()?>/chotu_work/assets/images/Share.png"class="position-absolute share-button-banner end-0 border-0" alt=""></a></div>

      <img class="card-img-top" style="aspect-ratio:3/1; object-fit:cover;filter: brightness(0.5);" src="<?php echo $banner_image; ?>">
      <div class="caption_banner"><p><?php echo strtoupper($title);?></p></div>
    </div>

    <div class="text-center pt-4 pb-4" style="height:auto;background-color:#f9f9f9;">
      <h2>
        
        <div class="fs-6 pt-3">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-geo-alt-fill"
            viewBox="0 0 16 16">
            <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
          </svg>
          <span class="ml-3"><?php echo $locality_address?></span>
        </div>
      </h2>
      <div style="" class="w-75 fs-6 text-center mt-3 m-auto mb-3">
          <?php echo get_post_meta($locality_id,'locality_description',true);?>
      </div>
    </div>
    <!-- <div class="sm-3"> -->
    <?php //echo do_shortcode('[ivory-search id="276" title="Locality"]'); 
    if(isset($locality_marquee) && $locality_marquee !=''){?>
	   <marquee width="100%" direction="left"><?php echo $locality_marquee ?></marquee>
	 <?php  }
   $captains = get_captains_by_localities($locality_id);
   if(!empty($captains)){
	?>
    <!-- cards -->
<?php

echo do_shortcode('[show_brands title="Top Brands" locality_id="'.$locality_id.'" captain_id="" lat_long=""]');
?>

<?php 
echo do_shortcode('[show_captains title="Top Captains" locality_id="'.$locality_id.'" brand_id="" lat_long=""]');
}else{
  echo do_shortcode('[show_brands title="Top Brands" locality_id="" captain_id="" lat_long="26.8743953,80.9981468"]');
}

//echo do_shortcode('[brands_near_me_carousel]');
the_content();
get_footer();