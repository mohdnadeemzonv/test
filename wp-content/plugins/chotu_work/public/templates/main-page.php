<?php
/*
 * Template Name: Main Site Page 
 * description: >-
  Page template without sidebar
 */
get_header();
echo '<p class="fs-6 text-center">
        for better results, please <a class="allow" href="javascript:void(0)" onclick="getLocation()">Allow</a> location access
        
        <br />
        <a href="#brands_list" class="pull-right">Brands Near Me</a></p>';
$page_id = get_the_ID();
global $wpdb;
 if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
        $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
        $lat = $location['lat'];
        $lon = $location['lon'];
      }else{
        $lat = $_COOKIE['lat'];
        $lon = $_COOKIE['lon'];
      }
    $lat_long = $lat.','.$lon;
echo do_shortcode('[show_localities title="Localities Near Me" brand_id="" captain_id="" lat_long="'.$lat_long.'"]');
echo '<br /><a href="#locality_list" class="pull-right">Localities Near Me</a>';
echo do_shortcode('[show_brands title="Brands Near Me" locality_id="" captain_id="" lat_long="'.$lat_long.'"]');
the_content();
?>

    <!-- Normal Card -->
    <p class="mt-5 ms-3 fs-5 font-family-karla font-weight-bold"><?php echo get_field('why_buy_on_chotu_why_buy_on_chotu',$page_id);?></p>
    <div class="carousel overflow-hidden" data-flickity='{"freeScroll":true, "autoPlay":false}'>
      <div style="height:300px; width:100%" class="carousel-item  d-flex justify-content-center align-items-center">
        <div class="w-100 bottom-0 h-75 position-absolute" style="background: #dce8ff"></div>
        <div class="card shadow" style="width: 19.938rem; height:12.625rem;">

          <div class="font-family-karla font-weight-bold mt-3 ms-4 fs-5">
           <?php echo get_field('why_buy_on_chotu_buy_heading',$page_id);?>
          </div>
          <div class="mx-4 w-50 ">
            <small> <?php echo get_field('why_buy_on_chotu_text',$page_id);?></small>
            <div class="my-3 top-0 end-0 position-absolute">
              <img src="<?php echo get_field('why_buy_on_chotu_image',$page_id);?>" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <p class="mt-5 ms-3 fs-5 font-family-karla font-weight-bold"><?php echo get_field('why_sell_on_chotu_why_sell_on_chotu',$page_id);?></p>
    <div class="carousel overflow-hidden" data-flickity='{"freeScroll":true, "autoPlay":false}'>
      <div style="height:300px; width:100%" class="carousel-item  d-flex justify-content-center align-items-center">
        <div class="w-100 bottom-0 h-75 position-absolute" style="background: #FFE9DC"></div>
        <div class="card shadow" style="width: 19.938rem; height:12.625rem;">
          <div class="font-family-karla font-weight-bold mt-3 ms-4 fs-5">
            <?php echo get_field('why_sell_on_chotu_sell_heading',$page_id);?>
          </div>
          <div class="mx-4 w-50">
            <small><?php echo get_field('why_sell_on_chotu_sell_text',$page_id);?></small>
            <div class=" bottom-0 end-0 position-absolute">
              <img src="<?php echo get_field('why_sell_on_chotu_sell_image',$page_id);?>" alt="">
            </div>
          </div>
        </div>
        <p class="position-absolute bottom-0">
          Become a
          <a href="">Captain Now</a>
        </p>
      </div>
    </div>
<?php
do_action('chotu_work_home_after_content');
get_footer();
?>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>
<script type="text/javascript">
   jQuery(".carousel").swipe({
      swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
          if (direction == 'left') jQuery(this).carousel('next');
          if (direction == 'right') jQuery(this).carousel('prev');
      },
      allowPageScroll: "vertical" 
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
     jQuery('.carousel').carousel('pause');
     jQuery(".carousel-inner > .carousel-item:first").addClass("active");
     jQuery(".carousel-inner-brands > .carousel-item:first").addClass("active");
  });


</script>