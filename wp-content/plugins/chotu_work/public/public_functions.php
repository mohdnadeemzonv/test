<?php 
global $blog_id, $wpdb;
$blog_id = get_current_blog_id();

$wpdb->set_blog_id( $blog_id );
//wp_get_attachment_image( absint( $vendor_data['logo'] ), 'full' );

/*
Retrieve post meta of central site from a network site (brand) for a custom post type (locality)
*/
function get_custom_post_meta($post_id,$meta_key){
  global $wpdb;
  $wp_postmeta = "wp_postmeta";
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND post_id = '".$post_id ."')"); 
}
/*
Retrieve post meta of central site from a network site (brand) for a user (captain or buyer)
*/
function get_custom_user_meta($user_id,$meta_key){
  global $wpdb;
  $wp_postmeta = "wp_usermeta";
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND user_id = '".$user_id ."')"); 
}
/*
Retrieve custom taxonomy of central site from a network site (brand) for any term
*/
function get_custom_term($term_id){
  global $wpdb;
  $wp_terms = "wp_terms";
  return $wpdb->get_var("SELECT name FROM $wp_terms WHERE (term_id = '".$term_id ."')");
}

/*
Retrieve the square pic URL of any object in central site from network site (brand)
*/
function get_custom_attachment_image($post_attach_id,$size=''){
  global $wpdb;
  $postmeta = "wp_postmeta";
  $post_thumbnail_metadatas = $post_attach_id ? maybe_unserialize( $wpdb->get_var( "SELECT meta_value FROM $postmeta WHERE meta_key = '_wp_attachment_metadata' AND post_id = {$post_attach_id}" ) ) : NULL;
  if(isset($post_thumbnail_metadatas)){
    // $image = ($post_thumbnail_metadatas[ 'sizes' ][$size]['file'] ? $post_thumbnail_metadatas[ 'sizes' ][$size]['file']: '');
     return get_site_url(0) . '/wp-content/uploads/' . $post_thumbnail_metadatas[ 'file' ];
  }
  return '';
}

/*
Retrieve the square pic attachment ID of any object in central site from network site (brand)
*/
function get_the_post_thumb_id($post_id){
  global $wpdb;
  $wp_postmeta = "wp_postmeta";
  return $post_thumbnail_id = $wpdb->get_var( "SELECT meta_value FROM  $wp_postmeta WHERE meta_key = '_thumbnail_id' AND post_id = $post_id" );
}

/*
  TO BE DONE. Display all the other brands being sold by the captain on the order-received page
*/
add_action('storefront_before_footer','display_all_pins_by_related_team');
function display_all_pins_by_related_team(){
  if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
  if( !is_wc_endpoint_url( 'order-received' ) ) return;
  global $woocommerce, $post;
  $order_id = wc_get_order_id_by_order_key( $_GET['key'] );
  $team_id = get_post_meta($order_id,'team_id',true);
  echo do_shortcode('[pin_by_team team_id="'.$team_id.'"]');
}


/*HOME PAGE: Retrieve locality and brands for the browser lat_long*/
//add_shortcode('home_locality_captain_brand','get_locality_captain_brand_by_location');
function get_locality_captain_brand_by_location($lat,$long){//$lat,$long
  global $wpdb;
  // $geo = file_get_contents('https://api.ipgeolocation.io/ipgeo?apiKey=e6fb148ad74443a6a09a2f5b5c7e8dfe');
  // $geo = json_decode($geo, true);
  // dd($geo);

  $lat_long = '17.488476,78.364062';
  $chotu_master_locations = 'chotu_master_locations';
  $wp_posts = $wpdb->base_prefix.'posts';
  $wp_postmeta = $wpdb->base_prefix.'postmeta';
  $gated_localities =  get_object_by_location($lat_long,'locality','30');
  if(!empty($gated_localities)){
    $ids = implode(",", array_column($gated_localities, 'reference_ID'));
    $localities = $wpdb->get_results("SELECT * FROM $wp_posts WHERE `ID` IN($ids) ");
    echo '<h1><strong>Localities near me</strong></h1>';
    if(!empty($localities)){
      foreach ($localities as $key => $locality) {
        $footer_text = '07 Aug, Sat | min: 40 orders';
        $button_text = 'View';
        $attachment_id = get_custom_post_meta($locality->ID,'locality_banner_pic');
        $banner_image = wp_get_attachment_image_url($attachment_id);
        chotu_work_make_card($banner_image,$locality->post_title,$footer_text,$button_text,'','');
        //echo '<div class="locality"><h3><a href="'.get_permalink($locality->ID).'">'.$locality->post_title.'</a></h3><p>'.$locality->post_content.'</p>';
      }
    }
    $captains = get_captains_by_localities($localities);
    // // dd($captains);
    // echo '<h1><strong>Captains near me</strong></h1>';
    // if(!empty($captains)){
    //   foreach ($captains as $key => $captain) {
    //     $user = get_user_by( 'id', $captain->captain_ID );
    //     echo '<div class="captain"><h3><a href="'.get_author_posts_url($captain->captain_ID).'">'.$user->display_name.'</a></h3>';
    //   }
    // }
    $brands = get_brands_by_captains($captains);
    echo '<h1><strong>Brands near me</strong></h1>';
    if(!empty($brands)){
      foreach ($brands as $key => $brand) {
        $blog = get_blog_details($brand->brand_id);
        $footer_text = '07 Aug, Sat | min: 40 orders';
        $button_text = 'View';
        $attachment_id = get_custom_post_meta($brand->brand_id,'locality_banner_pic');
        $banner_image = get_custom_attachment_image($attachment_id,'');
        chotu_work_make_card($banner_image,$blog->blogname,$footer_text,$button_text,'');
        //echo '<div class="captain"><h3><a href="'.$blog->siteurl.'">'.$blog->blogname.'</a></h3>';
      }     
    }
  }
}

/*LOCALITY PAGE: Retrieve all captains for a locality page - single-locality.php*/
/*
add_shortcode('all_captains','get_all_captain_of_current_locality');
function get_all_captain_of_current_locality(){
  $locality_id = get_the_ID();
  $captains = get_captains_by_localities($locality_id);
  $lat_long = get_post_meta($locality_id,'locality_lat_long',true);
  $locality_marquee = get_post_meta($locality_id,'locality_marquee',true);
    // dd($captains);
  if(isset($locality_marquee) && $locality_marquee !=''){
    echo '<marquee width="100%" direction="left">'.$locality_marquee.'</marquee>';
  }
  
  $user_id = '';
  $delivery_date='';
  $super_captain = get_users( array( 'role__in' => array( 'super-captain' ) ) );
  if(!empty($super_captain)){
    $user_id =$super_captain[0]->ID;
        //display_brands_with_url_param($brand->brand_id,$locality_id,$user_id);
  }

  if(!empty($captains)){
    $brands = get_brands_by_captains($captains);
    echo '<h1><strong>Brands near me</strong></h1>';
    if(!empty($brands)){
      foreach ($brands as $key => $brand) {
        display_brands_with_url_param($brand->brand_id,$locality_id,$user_id);
      }     
    }
    echo '<h1><strong>Captains of these locality</strong></h1>';
    foreach ($captains as $key => $captain) {
      $user = get_user_by( 'id', $captain->captain_ID );
      echo '<div class="captain"><h3><a href="'.get_author_posts_url($captain->captain_ID).'/?l='.$locality_id.'">'.$user->display_name.'</a></h3>';
    }

  }else{
      $user_id =$super_captain[0]->ID;
      echo '<h1><strong>Brands near me</strong></h1>';
      $brands = get_object_by_location($lat_long,'vendor',30);
      if(!empty($brands)){
      foreach ($brands as $key => $brand) {
        display_brands_with_url_param($brand->reference_ID,$locality_id,$user_id);
      }     
    }
  }
}
*/

/*
FINAL FRONTIER: Attach URL parameters to the brand button on home page, captain page and locality page
*/
function display_brands_with_url_param($brand_id,$locality_id,$user_id){
  $blog_details = get_blog_details($brand_id);
  $delivery_date='';
  if(!empty($blog_details)){
    $delivery_order_cycle = get_delivery_date_and_cut_off_time($brand_id,$locality_id,$user_id);
   if(!empty($delivery_order_cycle)){
    $delivery_date = $delivery_order_cycle->delivery_date;
   }
  return $blog_details->siteurl.'/?l='.$locality_id.'&c='.$user_id.'&d='.date('Ymd', strtotime($delivery_date));
  }
}

/*Retrieve all brands coutn handled by a locality*/
function get_brands_count_by_locality($locality_id){
  global $wpdb;
  $chotu_captain_brand = 'chotu_captain_brand';
  return $wpdb->get_row("select COUNT(DISTINCT(c.brand_id)) as brand from chotu_captain_brand as c INNER JOIN wp_blogs AS b ON c.brand_id = b.blog_id WHERE c.captain_ID IN(select captain_ID from chotu_captain_locality WHERE locality_id=$locality_id) AND b.deleted = 0");
}

/*Retrieve all brands handled by a locality*/
function get_brands_by_localities($localities){
  global $wpdb;
  $captain_locality = 'chotu_captain_locality';
  if(is_array($localities)){
    $localities = implode(",", array_column($localities, 'ID'));
  }
  $captains = $wpdb->get_results("SELECT captain_ID FROM $captain_locality WHERE locality_id IN($localities)");
  if(!empty($captains)){
    return get_brands_by_captains($captains);
  }

}
/*Retrieve all localities handled by a brands*/
function get_localities_by_brand($brands){
  global $wpdb;
  if(is_array($brands)){
    $brands = implode(",", array_column($brands, 'ID'));
  }
  $captains = get_captains_by_brands($brands);
  if(!empty($captains)){
    return get_brands_by_captains($captains);
  }
}

/*Retrieve all localities handled by a captain*/
function get_localities_by_captains($captains){
  global $wpdb;
  $captain_locality = 'chotu_captain_locality';
  if(is_array($captains)){
    $captains = implode(",", array_column($captains, 'ID'));
  }
  return $wpdb->get_results("SELECT locality_id from $captain_locality WHERE captain_ID IN($captains)");
}

/*Retrieve all captains in a locality_id*/
/**
 * @param $locality array()
 * */
function get_captains_by_localities($localities){
  global $wpdb;
  $captain_locality = 'chotu_captain_locality';
  if(is_array($localities)){
    $localities = implode(",", array_column($localities, 'ID'));
  }
  return $wpdb->get_results("SELECT captain_ID FROM $captain_locality WHERE locality_id IN($localities)");
}

/*Retrieve all captain handled by a brand*/
function get_captains_by_brands($brands){
  global $wpdb;
  $captain_locality = 'chotu_captain_brand';
  if(is_array($brands)){
    $brands = implode(",", array_column($brands, 'ID'));
  }
  return $wpdb->get_results("SELECT captain_ID from $captain_locality WHERE brand_id IN($brands)");
}

/*Retrieve all brands handled by a captain_id*/
/**
 * @param  $captain_id 
 * type int
 * */
function get_brands_by_captains($captains){
  global $wpdb;
  $captain_brand = 'chotu_captain_brand';
  if(is_array($captains)){
    $captains = implode(",", array_column($captains, 'captain_ID'));
  }
  return $wpdb->get_results("SELECT brand_id from $captain_brand WHERE captain_ID IN($captains)");
}

/*Select count of order done by captain*/
function get_order_count_by_captain($captain_ID){
  global $wpdb;
  $chotu_orders = 'chotu_orders';
  return $wpdb->get_row("SELECT COUNT(DISTINCT(order_ID)) as count FROM $chotu_orders WHERE captain_ID=$captain_ID");
}
/* Retrieve the weekday from the index */
function getDay($number){
    $dowMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    $dow_numeric = date($number);
    return $dowMap[$dow_numeric];
}

/*Retrieve all the order cycles available for a brand*/
function get_all_available_weekday($brand_id){
  global $wpdb;
  $blogmeta = $wpdb->base_prefix.'blogmeta';
  return $wpdb->get_results("SELECT meta_value FROM $blogmeta WHERE `meta_key` LIKE '%brand_weekday%' AND blog_id =$brand_id");
  
}
/*Retrieve the delivery date and cut off time for a given brand_ID. Locality_ID and Captain_ID are here as placeholders, maybe required in future. Right now, only brand_ID suffices.*/
function get_delivery_date_and_cut_off_time($brand_id,$locality_id,$captain_id){//$brand_id,$locality_id,$captain_id
  global $wpdb;
  $blogmeta = $wpdb->base_prefix.'blogmeta';
  $next_delivery_date = date('Y-m-d H:i');
  $order_before_time_sql = "(SELECT CONCAT(meta_value,' ',(SELECT meta_value from wp_blogmeta WHERE  meta_key = 'brand_cut_off_time' AND  `blog_id` = '$brand_id')) as date_and_time from wp_blogmeta WHERE meta_key = 'brand_order_cycle' AND  `blog_id` = '$brand_id' HAVING date_and_time >= '$next_delivery_date' order by date_and_time ASC limit 1)";
return $wpdb->get_row("SELECT DATE_ADD($order_before_time_sql, INTERVAL (SELECT meta_value from wp_blogmeta WHERE  meta_key = 'brand_lead_time' AND  `blog_id` = '$brand_id') DAY) as delivery_date,$order_before_time_sql as order_before_time");
}
add_action('init', 'chotu_work_set_cookie_by_geo_ip');
// my_setcookie() set the cookie on the domain and directory WP is installed on
function chotu_work_set_cookie_by_geo_ip(){
  if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
    $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
    setcookie('lat', $location['lat'], time() + (31536000));
    setcookie('lon', $location['lon'], time() + (31536000));
  }
        
}
