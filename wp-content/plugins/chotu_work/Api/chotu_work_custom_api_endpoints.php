<?php
use \Firebase\JWT\JWT;
function get_latest_post_permission(){
    $obj = new Jwt_Auth_Public('jwt-auth','v1');
    $jwt = $obj->validate_token();
    if(isset($jwt->errors)){
        return $jwt;
    }else{
        return true;
    }
}
add_action( 'rest_api_init', 'my_register_route');
function my_register_route() {
            
      register_rest_route( 'jwt-auth/v1/token/get_pins', 'team_id/(?P<id>\d+)', array(
            'methods' => 'GET',
            'callback' => 'get_pins_by_team',
            'args' => array(
                    'id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                           
                            return is_numeric( $param );
                        }
                    ),
                ),
            // 'permission_callback' => function () {
            //         return true;
            //     }
             'permission_callback' => 'get_latest_post_permission'
        ));
      register_rest_route( 'jwt-auth/v1/token','get_gated_locality', array(
            'methods' => 'GET',
            'callback' => 'get_all_gated_locality',
            'args' => array(
                    'id' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return is_numeric( $param );
                        }
                    ),
                ),
            // 'permission_callback' => function () {
            //         return true;
            //     }
            'permission_callback' => 'get_latest_post_permission'
        ));
}

/*
not required. to be removed
*/
/*
function get_pins_by_team( $data ) {
    global $wpdb;
    $team_id = '';
    $pins = array();
    if( isset( $data[ 'id' ] ) ) {
          $team_id = $data[ 'id' ];
    }
    $wp_central_coupon = 'wp_central_coupon';
    $pins_post_ids =  $wpdb->get_results("SELECT blog_id,post_id,dynamic_url FROM $wp_central_coupon WHERE team_id = ".$team_id);
    if(!empty($pins_post_ids)){
        foreach ($pins_post_ids as $key => $pin) {
            $current_blog_details = get_blog_details($pin->blog_id);
            $vendor_name = str_replace('/', " ", $current_blog_details->path);
            $posts = $wpdb->base_prefix.$pin->blog_id.'_posts';
            $postmeta = $wpdb->base_prefix.$pin->blog_id.'_postmeta';
            $pins[] = $wpdb->get_results("SELECT $posts.ID, $posts.post_title, $postmeta.meta_key, $postmeta.meta_value FROM $posts, $postmeta WHERE $posts.ID = $postmeta.post_ID AND $posts.ID = '$pin->post_id'");
            $pins[$key]['dynamic_url'] = $pin->dynamic_url;
            $pins[$key]['vendor_name'] = $vendor_name;
        }
    }

    wp_reset_postdata();
    return rest_ensure_response( $pins );
}*/

/*
API to fetch all localities with meta data
*/
/*
function get_all_gated_locality(){
    global $wpdb;
    $gated_localitys = array();
    $args = array(
    'post_type'  => 'locality',
    'post_status'  => 'publish',
    );
    $gated_locality  = get_posts($args);

    if(!empty($gated_locality)){
        foreach ($gated_locality as $key=> $value) {
           $gated_locality[$key]->meta = get_post_meta($value->ID);
        }
    }
    return rest_ensure_response( $gated_locality );
}*/