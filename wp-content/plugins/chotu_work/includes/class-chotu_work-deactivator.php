<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_work
 * @subpackage Chotu_work/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Chotu_work
 * @subpackage Chotu_work/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_work_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;
    	$wpdb->query( "DROP TABLE IF EXISTS wp_customer_orders" );
    	$wpdb->query( "DROP TABLE IF EXISTS wp_delivery_routes" );
    	$wpdb->query( "DROP TABLE IF EXISTS wp_captain_delivery_table" );
    	$wpdb->query( "DROP TABLE IF EXISTS wp_central_coupon" );
    	$wpdb->query( "DROP TABLE IF EXISTS wp_master_teams" );
    	$wpdb->query( "DROP TABLE IF EXISTS wp_delivery_planning" );
	}

}
