<?php
date_default_timezone_set('Asia/Kolkata');
add_filter( 'woocommerce_email_headers', 'woocommerce_email_headers_add_recipient', 10, 3);
function woocommerce_email_headers_add_recipient($headers, $mail_id, $order) {
  if($mail_id == 'customer_processing_order') {
    $member = null;
    $memberMail = null;
    foreach($order->get_meta_data() as $item) {
      if ($item->key === 'member_name') {
        $member = $item->value;
        $memberMail = $this->getMemberMail($member);
        $headers .= "BCC: {$member} <{$member_mail}>\r\n";
        break;
      }
    }
  }
  return $headers;
}
function dd($data){
  echo "<pre>";
  print_r($data);
  echo "</pre>";
  die;
}
function get_blog_id_by_blog_path($domain,$path = '/'){
  global $wpdb;
  $blog_id = $wpdb->get_row("SELECT blog_id FROM wp_blogs WHERE `path`='$path'");
  if($blog_id){
    return $blog_id->blog_id;
  }else{
    return false;
  }
}

function get_brand_post_meta($post_id,$meta_key,$blog_id){
  global $wpdb;
  $wp_postmeta = $wpdb->base_prefix.$blog_id.'_postmeta';
  return $wpdb->get_var("SELECT meta_value FROM $wp_postmeta WHERE (meta_key = '$meta_key' AND post_id = '".$post_id ."')"); 
}

/*
add_action('wp_footer','add_back_buttom_in_footer');
function add_back_buttom_in_footer(){
  global $wpdb;
  $blog_id = get_current_blog_id();
  if($blog_id != 1 && is_user_logged_in()){ ?>
    <a href="<?php echo network_site_url(0).'my-account';?>" id="ast-scroll-top" class="ast-scroll-top-icon ast-scroll-to-top-left" data-on-devices="both" style="display: inline;">
      <span class="ast-icon icon-arrow"></span> <span class="screen-reader-texts">All Orders</span>
    </a>
  <?php }else if($blog_id != 1){
    $lat_long = '';
    $location_table_data = get_location_object_data_by_type($blog_id,'brand');
    if(!empty($location_table_data)){
      $lat_long = $location_table_data->lat_long;
    }
    $localities = get_object_by_location($lat_long,'locality',30);
    ?>
    <div class="locality-modal">
    <div class="modal-overlay locality-modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
      <div class="modal-header">
        <button class="modal-close locality-modal-toggle"><svg class="icon-close icon" viewBox="0 0 32 32"><use xlink:href="#icon-close"></use></svg></button>
        <h2 class="modal-heading">Please Select your locality</h2>
      </div>
      <div class="modal-body">
        <div class="modal-content">
          <label> Locality Name</label>
          <select name="locality_id" id="locality_id">
            <?php
            if(!empty($localities)){
              foreach ($localities as $key => $locality) {
                // $post = get_post($locality->reference_ID);
                // dd($post)
                $locality_details = get_localitity_by_id('locality',$locality->reference_ID);
                echo '<option value="'.$locality->reference_ID.'">'.$locality_details->post_title.'</option>';
              }
            }
            ?>
          </select>
          <button class="locality-modal-toggle" id="update_locality_id">Update</button>
        </div>
      </div>
    </div>
  </div>
  <?php
  }
}
*/
function load_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_media_files' );


function update_custom_post_meta($post_id,$blog_id,$meta_key,$meta_value){
  global $wpdb;
  $postmeta = $wpdb->base_prefix.$blog_id.'_postmeta';
  $exist = $wpdb->get_var("SELECT meta_key FROM $postmeta WHERE meta_key = '$meta_key' AND post_id='$post_id'");

  if(isset($exist)){
   $wpdb->query($wpdb->prepare("UPDATE $postmeta SET meta_value='$meta_value' WHERE meta_key='$meta_key' AND post_id=$post_id"));
 }else{
  $wpdb->insert( $postmeta, array('post_id' => $post_id, 'meta_key' => $meta_key, 'meta_value' => $meta_value));

}
return true;
}

/* get custom brand logo url*/
function get_custom_logo_url($blog_id){
  $image_url = '';
  $switched_blog = false;

  if ( is_multisite() && ! empty( $blog_id ) && (int) $blog_id !== get_current_blog_id() ) {
    switch_to_blog( $blog_id );
    $switched_blog = true;
  }
  $custom_logo_id = get_theme_mod( 'custom_logo' );

    // We have a logo. Logo is go.
  if ( $custom_logo_id ) {
    $image = wp_get_attachment_image_src( $custom_logo_id , 'thumb' );
    $image_url = $image[0];
  }

  if ( $switched_blog ) {
    restore_current_blog();
  }

  return $image_url ;
}

/* Return ALL localities without any filter */
function get_localities($post_type = ''){
  global $wpdb;
  $wp_posts = 'wp_posts';
  return $wpdb->get_results("SELECT * FROM $wp_posts WHERE post_type='$post_type' AND post_status='publish'");

}

/* Return all locality details by the ID */
function get_localitity_by_id($post_type = '',$post_id){
  global $wpdb;
  $wp_posts = 'wp_posts';
  return $wpdb->get_row("SELECT * FROM $wp_posts WHERE post_type='$post_type' AND ID='$post_id'");

}

/* when updating the custom locations table, this function chest if the given location/captain/brand exists or not */
function chotu_checkExist($reference_id,$reference_type)
{
  global $wpdb;
  $chotu_master_locations = 'chotu_master_locations';
  $rowcount = $wpdb->get_var("SELECT COUNT(*) FROM $chotu_master_locations WHERE reference_id = '$reference_id' AND reference_type='$reference_type'");
  return $rowcount;
}

/* converts a key array into options
function convert_index_array_to_associative($options){
  if(is_array($options)){
    $aa = array();
    foreach ($options as $key => $option) {
     $aa[$option] = $option;
   }
   return $aa;
 }
}
*/


/* not required, used to retrieve brand logo when it was stored in storefront. now, we store the logo in wp_options
function get_store_front_logo($blog_id){
  global $wpdb;
  $options = $wpdb->base_prefix.$blog_id.'_options';
  $postmeta = $wpdb->base_prefix.$blog_id.'_postmeta';
  $logo = $wpdb->get_row("SELECT * FROM $options WHERE option_name = 'theme_mods_storefront'");
  $logos = unserialize($logo->option_value);
  if(!empty($logos)){
    $logo_url = unserialize($wpdb->get_var( "SELECT meta_value FROM {$postmeta} WHERE meta_key = '_wp_attachment_metadata' AND post_id = {$logos['custom_logo']}" ));
    return get_site_url(0) . '/wp-content/uploads/sites/'.$blog_id.'/' . $logo_url[ 'file' ];
  }else{
    return get_site_url(0).'/wp-content/plugins/chotu_work/public/templates/img/logos.png';
  }
 
}*/

/* Google Maps API
*/
// function chotu_acf_init() {
//   wp_register_script('aa_js_googlemaps_script',  'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAF2HFo3SqE3CC3Yfoi0enTV61S0mcLDFQ'); // 
//   wp_enqueue_script('aa_js_googlemaps_script');
//   acf_update_setting('google_api_key', 'AIzaSyAF2HFo3SqE3CC3Yfoi0enTV61S0mcLDFQ');
// }
// add_action('acf/init', 'chotu_acf_init');


/* not required
function get_allowed_dates($gated_locality_id){
  global $wpdb;
  $all_days=array(0,1,2,3,4,5,6);
  $wp_delivery_routes = 'wp_delivery_routes';
  if(isset($gated_locality_id)){
    $days =  $wpdb->get_row("SELECT GROUP_CONCAT(route_weekdays) as route_weekdays,GROUP_CONCAT(min_order_value) as min_order_value FROM `wp_delivery_routes` WHERE `gated_locality_id`=".$gated_locality_id);
    
    if(!empty($days) && ($days !=null)){
      $days_array = explode(",", $days->route_weekdays);
      $result=array_diff($all_days,$days_array);
      $day = implode(",", $result);

      $min_order_value = explode(",", $days->min_order_value)[0];
      return compact('day','min_order_value');
    }
    return implode(",", $all_days);
  }
  return implode(",", $all_days);
  
}
*/

/* From custom locations table, retrieves the respective object (locality, brand, captain) for a given lat_long and radius*/
function get_object_by_location($lat_long,$reference_type,$radius){
  global $wpdb;
  $lat_long = str_replace(",", ' ', $lat_long);
  $chotu_master_locations = 'chotu_master_locations';
  // if($reference_type == 'locality'){
  //   return $wpdb->get_results("SELECT * , ST_AsText(`lat_long`), ST_Distance(`lat_long`,ST_GeomFromText( 'POINT(".$lat_long.")' ))* 111.139 as `distance` FROM chotu_master_locations as c JOIN wp_posts as p ON c.reference_ID = p.ID HAVING `distance` < '$radius' AND reference_type='$reference_type' AND p.post_status='publish' order by `distance` asc");
  // }
  return $wpdb->get_results("SELECT * , ST_AsText(`lat_long`), ST_Distance(`lat_long`,ST_GeomFromText( 'POINT(".$lat_long.")' ))* 111.139 as `distance` FROM $chotu_master_locations HAVING `distance` < '$radius' AND reference_type='$reference_type' order by `distance` asc");
}

/*PERSISTENT LOGIN for customers on the front-end*/
add_filter('auth_cookie_expiration', 'chotu_persistent_expiration_filter', 99, 3);
function chotu_persistent_expiration_filter($seconds, $user_id, $remember){
  $remember = true;
    //if "remember me" is checked;
  if ( $remember ) {
        //WP defaults to 2 weeks;
        $expiration = 14*24*60*60; //UPDATE HERE;
      } else {
        //WP defaults to 48 hrs/2 days;
        $expiration = 2*24*60*60; //UPDATE HERE;
      }

    //http://en.wikipedia.org/wiki/Year_2038_problem
      if ( PHP_INT_MAX - time() < $expiration ) {
        //Fix to a little bit earlier!
        $expiration =  PHP_INT_MAX - time() - 5;
      }

      return $expiration;
    }

/*change slug for any post type. Here, we change locality slug from /locality to /l */
function chotu_change_slug( $args, $post_type ) {
 /* item post type slug */   
if ( 'locality' === $post_type ) {
  $args['rewrite']['slug'] = 'l';
}
return $args;
}
add_filter( 'register_post_type_args', 'chotu_change_slug', 10, 2 );


/* not required. restrict the minimum length of brnad to 8 characters.
  function chotu_minimum_site_name_length( $length ) {
    return 8;
  }
  add_filter( 'minimum_site_name_length', 'chotu_minimum_site_name_length' );
*/


/*check length of pin code for captain and locality custom fields*/
add_filter('acf/validate_value/name=locality_pin_code', 'chotu_acf_validate_value', 10, 4);
add_filter('acf/validate_value/name=captain_pin_code', 'chotu_acf_validate_value', 10, 4);

function chotu_acf_validate_value( $valid, $value, $field, $input ){
  // bail early if value is already invalid
    if( !$valid ) {
      return $valid;
    }
    if ( strlen($value) != 6 ) {

      $valid = 'Must be 6 digit.';
    } 
  // return
    return $valid;
  }

/* shows all the geography_locality as option in a search select box*/
function chotu_get_locality_select_input($selected_id){
  $posts = get_posts(array(
    'post_type'   => 'locality',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'fields' => 'ids'
  ));
  if(!empty($posts)){
    foreach($posts as $locality){
      if(get_post_meta($locality,"geo_locality",true) != ''){
        if(isset($selected_id) && $selected_id ==  $locality){
          echo '<option value="'.$locality.'" selected>'.get_post_meta($locality,"geo_locality",true).'</option>';
        }else{
          echo '<option value="'.$locality.'">'.get_post_meta($locality,"geo_locality",true).'</option>';
        }
      }
    }
  }
}

/*rewites the author page with the template we have in public/template*/
add_filter ('template_include', 'chotu_redirect_page_template');
function chotu_redirect_page_template ($template) {
 $file = '';

 if ( is_author() ) {
        $file   = 'author.php'; // the name of your custom template
        $find[] = $file;
        $find[] = 'chotu_work/public/templates/' . $file; // name of folder it could be in, in user's theme
      } 
      if ( $file ) {
        $template       = locate_template( array_unique( $find ) );
        if ( ! $template ) { 
            // if not found in theme, will use your plugin version
          $template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/public/templates/' . $file;
        }
      }
      return $template;
    }


/* Filter the single_template with our custom function*/
add_filter('single_template', 'chotu_work_locality_post_template');
function chotu_work_locality_post_template($single) {
  global $post;
  /* Checks for single template by post type */
  if ( $post->post_type == 'locality' ) {
    $file   = 'single-locality.php';
      if ( file_exists( plugin_dir_path( __FILE__ ) ) . '/public/templates/' . $file ) {
          return plugin_dir_path( __FILE__ ) . '/public/templates/' . $file;
      }
  }

  return $single;

}

/* Retrieves all the category-subcategory concat as an option in a search-select field*/
function chotu_get_taxonomy_options($taxonomy,$selected){
      $brand_category =  get_terms( array(
        'taxonomy' => $taxonomy,
        'hide_empty' => false,
    ) );
    if(!empty($brand_category)){
      foreach ($brand_category as $key => $value) {
        if(is_array($selected)){
          if(in_array($value->term_id, $selected)){
          echo '<option value="'.$value->term_id.'" selected>'.$value->name.'</option>';
          }else{
            echo '<option value="'.$value->term_id.'">'.$value->name.'</option>';
          }
        }else{
          if($value->term_id == $selected){
            echo '<option value="'.$value->term_id.'" selected>'.$value->name.'</option>';
          }else{
            echo '<option value="'.$value->term_id.'">'.$value->name.'</option>';
          }
      }
      }
      
    }
}

/* replace the search-select field in ACF with custom options 
function chotu_work_acf_load_field_locality( $field ) {
    $field['required'] = true;
    $posts = get_posts(array(
    'post_type'   => 'locality',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'fields' => 'ids'
  ));
    if(!empty($posts)){
       foreach($posts as $locality){
        if(get_post_meta($locality,"geo_locality",true) != ''){
          $field['choices'][$locality] = get_post_meta($locality,"geo_locality",true);
        }
       }
    }
    return $field;
}
add_filter('acf/load_field/name=locality', 'chotu_work_acf_load_field_locality');
function chotu_work_acf_load_field_brands( $field ) {
    $field['required'] = true;
    $sites = get_sites();
    if(!empty($sites)){
       foreach($sites as $site){
        if($site->blog_id !=1 ){
         $blog_details = get_blog_details($site->blog_id);
          $field['choices'][$site->blog_id] = $blog_details->blogname;
        }
       }
    }
    return $field;
}
add_filter('acf/load_field/name=brands', 'chotu_work_acf_load_field_brands');

*/

/* MAP captain with brand in captain page*/

function update_captain_brand_data($captain_id,$brands){
  global $wpdb;
  $b_values = implode(",", $brands);
  $chotu_captain_brand = 'chotu_captain_brand';
  $wpdb->query("DELETE FROM $chotu_captain_brand WHERE captain_ID='$captain_id'" );
  foreach ($brands as $key => $brand) {
      $wpdb->query("INSERT INTO $chotu_captain_brand (brand_id, captain_ID) VALUES ('$brand', '$captain_id')" );
  }
  return true;
}

/* MAP captain with locality in captain page*/
function update_captain_locality_data($captain_id,$localities){
  global $wpdb;
  $chotu_captain_locality = 'chotu_captain_locality';
  $wpdb->query("DELETE FROM $chotu_captain_locality WHERE captain_ID='$captain_id'" );
  foreach ($localities as $key => $locality) {
      $wpdb->query("INSERT INTO $chotu_captain_locality (locality_id, captain_ID) VALUES ('$locality', '$captain_id')" );
  }
  return true;
}

/* delete ALL brand or locality data for a particular captain*/
function delete_captain_data($captain_id,$type){
global $wpdb;
  $chotu_captain_brand = 'chotu_captain_brand';
  $chotu_captain_locality = 'chotu_captain_locality';
  if($type =='brand'){
    $wpdb->query("DELETE FROM $chotu_captain_brand WHERE  captain_ID='$captain_id'" );
  }
  else if($type =='locality'){
    $wpdb->query("DELETE FROM $chotu_captain_locality WHERE  captain_ID='$captain_id'" );
  }
}

function chotu_work_make_brand_card($brands,$carousel_start,$end_carousel,$locality_id,$user_id,$class){
  $banner_image = '';
  $html ='';
 if(!empty($brands)){
        $sort_brand = array();
        $i=0;
        foreach ($brands as $key => $brand) {
          $delivery_order_cycle = get_delivery_date_and_cut_off_time($brand->brand_id,'','');
          if(isset($delivery_order_cycle->delivery_date)){
            $sort_brand[$i]['brand_id'] = $brand->brand_id;
            $sort_brand[$i]['delivery_date'] = $delivery_order_cycle->delivery_date;
            $sort_brand[$i]['order_before_time'] = $delivery_order_cycle->order_before_time;
            $i++;
          }
        }
        if(!empty($sort_brand)) {
          array_multisort(array_column($sort_brand, 'order_before_time'), SORT_ASC, $sort_brand);
          foreach ($sort_brand as $key => $brand) {
           $html .= $carousel_start;
            $blog = get_blog_details($brand['brand_id']);
            $button_text = 'View';
            $url='';
            if(isset($locality_id) && $locality_id !='' || $user_id){
              $url = display_brands_with_url_param($brand['brand_id'],$locality_id,$user_id);
            }else{
              $url = $blog->siteurl;
            }
            
            $banner_pic_of_brand = unserialize(get_banner_pic_of_brand($brand['brand_id'],'banner_pic'));
            if(!empty($banner_pic_of_brand)){
              $banner_image = $banner_pic_of_brand['banner_pic'];
            }
           $html .=chotu_work_make_card($banner_image,$blog->blogname,$brand['brand_id'],$button_text,$url,$class,'brand');
          //echo '<div class="captain"><h3><a href="'.$blog->siteurl.'">'.$blog->blogname.'</a></h3>';
           $html .=$end_carousel;
          }
        }   
      }
      return $html; 
}
function chotu_work_make_card($banner_image,$header_text,$footer,$button_text,$button_url,$class,$type){//width: 25rem;
  $html = '';
  if($banner_image == ''){
    $banner_image = home_url().'/wp-content/uploads/2021/07/other-small.jpg';
  }
  $html .='<div class="card shadow mb-4 mx-auto" style="width: 80%; aspect-ratio:809/500;">
  <img class="card-img-top" style="aspect-ratio:3/1; object-fit:cover" src="'.$banner_image.'" alt="...">
  <!--button cmponent ->>>> giving border not nec.. -->
  <div class="card-body url_param">
    <span class="card-title">'.substr($header_text,0,40).'</span>';
    if($type == 'locality'){
      $html .='<p class="card-text text-muted pt-2"><small>'.$footer.'</small>
    <a href="'.$button_url.'" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 '.$class.'">'.$button_text.'</a></p>';
    }
    if($type == 'brand'){
      $delivery_order_cycle = get_delivery_date_and_cut_off_time($footer,'','');
      $delivery_date = '';
      if(!empty($delivery_order_cycle)){
        $delivery_date = $delivery_order_cycle->delivery_date;
        $order_before_time = $delivery_order_cycle->order_before_time;
      }
      $html .='<p class="card-text text-muted pt-2 order_before_time_card" data-card="'.$order_before_time.'"><small><img src="'.plugins_url().'/chotu_work/assets/images/truck-border.png" alt="">  '.date('j M, D',strtotime($delivery_date)).' | <img src="'.plugins_url().'/chotu_work/assets/images/timer-border.png" alt=""> <span class="timer"> </span></small><a href="'.$button_url.'" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 '.$class.'">'.$button_text.'</a></p>';
    }
    if($type == 'captain'){

      $html .='<p class="card-text text-muted pt-2"><small>'.$footer.'</small>
    <a href="'.$button_url.'" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 '.$class.'">'.$button_text.'</a></p>';
    }
  $html .='</div></div>';
return $html;
//     echo '<div class="card shadow mb-5" style="">
//   <img class="card-img-top" style="width:100%; height:40vh; object-fit: cover;" src="https://tiktik.in/wp-content/uploads/2021/07/other-small.jpg" alt="...">
//   <!--button cmponent ->>>> giving border not nec.. -->
//   <div class="card-body url_param">
//     <span class="card-title">'.substr($header_text,0,40).'</span>
//     <p class="card-text my-3 text-muted"><small>footer</small>
//     <a href="'.$button_url.'" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 '.$class.'">'.$button_text.'</a></p>
//   </div>
// </div>';
}


function get_banner_pic_of_brand($brand_id,$key_name){
  global $wpdb;
  $wp_options = $wpdb->base_prefix.$brand_id.'_options';
  return $wpdb->get_var("SELECT option_value FROM $wp_options WHERE `option_value` LIKE '%$key_name%'");
 
}

add_shortcode('localities_near_me_carousel','chotu_work_localities_near_me_carousel');
function chotu_work_localities_near_me_carousel(){
  $lat=$lon='';
  $html ='';
  $bg_color = '#FAFFDC';
    if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
        $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
        $lat = $location['lat'];
        $lon = $location['lon'];
      }else{
        $lat = $_COOKIE['lat'];
        $lon = $_COOKIE['lon'];
      }
    $lat_long = $lat.','.$lon;
    $localities =  get_object_by_location($lat_long,'locality','30');
    $gated_localities = implode(",", array_column($localities, 'reference_ID'));
    $captains = get_captains_by_localities($gated_localities);
    $brands = get_brands_by_captains($captains);
    $html .='';
     // $brand .='<div class="carousel overflow-hidden" data-flickity="{"freeScroll":true, "autoPlay":false}">';
  if(!empty($localities)){
          foreach ($localities as $key => $locality) {
            $s_locality = get_post($locality->reference_ID);
            if(!empty($s_locality)){
              $footer_text = '';
              $box_start = '<div class="carousel overflow-hidden ">
            <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 position-absolute" style="background: '.$bg_color.'; width:100%; "></div>';// '';
             $box_end = '</div></div>';
               // $brand .= '<div style="height:300px; width:100%" class="carousel-cell  d-flex justify-content-center align-items-center"><div class="w-100 bottom-0 h-75 position-absolute" style="background:'.$bg_color.'"></div>';
              
              $brand_count = get_brands_count_by_locality($locality->reference_ID);
              // $brand_count = ($b_count) ? $b_count : 0;
              if($s_locality->post_status =='publish'){
                $product_count = (strlen($s_locality->post_title) * ($brand_count->brand))*10;
                $footer_text = $brand_count->brand. ' Brands | '.$product_count.' Products';
                
              }else{
                $footer_text = 'Comming Soon';
              }
              $button_text = 'View';
              $attachment_id = get_custom_post_meta($locality->reference_ID,'locality_banner_pic');
              $banner_image = wp_get_attachment_image_url($attachment_id,'banner-image');

              
              
              // $html .='<div class="carousel-item"><div class="row"><div class="col-xs-4 mb-3">';
              $html .=$box_start;
              $html .= chotu_work_make_card($banner_image,$s_locality->post_title,$footer_text,$button_text,get_permalink($locality->reference_ID),'','locality'); 
              $html .=$box_end;
              // $html .='</div></div></div>';
              if( $key  === array_key_last($localities)) {
                $html .=$box_start;
                $html .= chotu_work_make_card_add_locality('locality','','');
                $html .=$box_end;
              } 
            }
            
          }
        }else{
          $html .='<div class="row"><div class="col-xs-4 mb-3">';
          $html .='<div class="card shadow mb-4 mx-auto" style="width: 80%; aspect-ratio:809/500;">
            <img class="card-img-top" style="aspect-ratio:3/1; object-fit:cover" src="'.plugins_url().'/chotu_work/assets/images/locality.png" alt="...">
            <!--button cmponent ->>>> giving border not nec.. -->
            <div class="card-body">
              <span class="card-title">Add Locality</span>
              <p class="card-text text-muted pt-2"><small>Text</small>
              <a href="'.home_url().'/add-locality" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 ">Add My locality</a></p>';
            $html .='</div></div>';
            $html .='</div></div></div>';

        }
        $html .='</div></div>';
        // $brand .= '</div>';
        return $html;
 }

add_shortcode('brands_near_me_carousel','chotu_work_brands_near_me_carousel');
function chotu_work_brands_near_me_carousel(){
  $lat=$lon='';
  $html='';
  $bg_color = '#FFF3DC';
    if(!isset($_COOKIE['lat']) && (!isset($_COOKIE['lon']))){
        $location = unserialize(file_get_contents('http://ip-api.com/php/'.$_SERVER['REMOTE_ADDR']));
        $lat = $location['lat'];
        $lon = $location['lon'];
      }else{
        $lat = $_COOKIE['lat'];
        $lon = $_COOKIE['lon'];
      }
    $lat_long = $lat.','.$lon;
    $localities =  get_object_by_location($lat_long,'locality','30');
    $gated_localities = implode(",", array_column($localities, 'reference_ID'));
    $captains = get_captains_by_localities($gated_localities);
    $brands = get_brands_by_captains($captains);

    $box_start = '<div class="ms-3 fs-3 font-family-karla font-weight-bold">'.$title.'</div><div class="carousel overflow-hidden ">
      <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 position-absolute" style="background: '.$bg_color.'; width:100%; "></div>';// '';
       $box_end = '</div></div>';
    $html .= $box_start; 
    $html .= chotu_work_make_brand_card($brands,'','','','','');     
    $html .= $box_end;

    return $html;
}



add_shortcode('show_localities','chotu_work_localities_show_localities');
function chotu_work_localities_show_localities($attrs){
  $html ='';
  if(isset($attrs['title'])){
    $title = $attrs['title'];
  }
  $brand_id_set =0; $captain_id_set =0; $lat_long_set =0;
  $locality_id ='';
  $captain_id = '';
  $lat_long = '';
  $localities='';

  if(isset($attrs['brand_id']) && $attrs['brand_id'] !=''){
    $brand_id = $attrs['brand_id'];
    $localities = get_localities_by_brand($brand_id);
    $brand_id_set = 1;
  }
  if(isset($attrs['captain_id']) && $attrs['captain_id'] !=''){
    $captain_id = $attrs['captain_id'];
    $localities = get_localities_by_captains($captain_id);
    $captain_id_set = 1;
  }
  if(isset($attrs['lat_long']) && $attrs['lat_long'] !=''){
    $lat_long = $attrs['lat_long'];
    $lat_long_set = 1;
    $localities =  get_object_by_location($lat_long,'locality','30');
  }
  if($brand_id_set+$captain_id_set+$lat_long_set > 1){
      echo '<div id="message" class="error"><p>Something went wrong.</p></div>';
      return ;
  }
  $bg_color = '#FAFFDC';
    $html .='<div id="locality_list" class="ms-3 fs-3 mb-3 font-family-karla font-weight-bold">'.$title.'</div>';
     // $brand .='<div class="carousel overflow-hidden" data-flickity="{"freeScroll":true, "autoPlay":false}">';
  if(!empty($localities)){
          foreach ($localities as $key => $locality) {
            if(isset($locality->reference_ID)){
              $locality_id = $locality->reference_ID;
            }else{
              $locality_id = $locality->locality_id;
            }
            $s_locality = get_post($locality_id);
            if(!empty($s_locality)){
              $footer_text = '';
              $box_start = '<div class="carousel overflow-hidden ">
            <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 position-absolute" style="background: '.$bg_color.'; width:100%; "></div>';// '';
             $box_end = '</div></div>';
               // $brand .= '<div style="height:300px; width:100%" class="carousel-cell  d-flex justify-content-center align-items-center"><div class="w-100 bottom-0 h-75 position-absolute" style="background:'.$bg_color.'"></div>';
              
              $brand_count = get_brands_count_by_locality($locality_id);
              // $brand_count = ($b_count) ? $b_count : 0;
              if($s_locality->post_status =='publish'){
                $product_count = (strlen($s_locality->post_title) * ($brand_count->brand))*10;
                $footer_text = $brand_count->brand. ' Brands | '.$product_count.' Products';
                
              }else{
                $footer_text = 'Comming Soon';
              }
              $button_text = 'View';
              $attachment_id = get_custom_post_meta($locality_id,'locality_banner_pic');
              $banner_image = wp_get_attachment_image_url($attachment_id,'banner-image');

              
              
              // $html .='<div class="carousel-item"><div class="row"><div class="col-xs-4 mb-3">';
              $html .=$box_start;
              $html .= chotu_work_make_card($banner_image,$s_locality->post_title,$footer_text,$button_text,get_permalink( $locality_id),'','locality'); 
              $html .=$box_end;
              // $html .='</div></div></div>';
              if( $key  === array_key_last($localities)) {
                $html .=$box_start;
                $html .= chotu_work_make_card_add_locality('locality','','');
                $html .=$box_end;
              } 
            }
            
          }
        }else{
          $html .='<div class="row"><div class="col-xs-4 mb-3">';
          $html .='<div class="card shadow mb-4 mx-auto" style="width: 80%; aspect-ratio:809/500;">
            <img class="card-img-top" style="aspect-ratio:3/1; object-fit:cover" src="'.plugins_url().'/chotu_work/assets/images/locality.png" alt="...">
            <!--button cmponent ->>>> giving border not nec.. -->
            <div class="card-body">
              <span class="card-title">Add Locality</span>
              <p class="card-text text-muted pt-2"><small>Text</small>
              <a href="'.home_url().'/add-locality" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 ">Add My locality</a></p>';
            $html .='</div></div>';
            $html .='</div></div></div>';

        }
        $html .='</div></div>';
        // $brand .= '</div>';
        return $html;
}
add_shortcode('show_brands','chotu_work_get_brand_swipe');
function chotu_work_get_brand_swipe($attrs){
  //dd($attrs);
  if(isset($attrs['title'])){
    $title = $attrs['title'];
  }
  $locality_id_set =0; $captain_id_set =0; $lat_long_set =0;
  $locality_id='';
  $captain_id = '';
  $lat_long = '';
  $captains='';

  if(isset($attrs['locality_id']) && $attrs['locality_id'] !=''){
    $locality_id = $attrs['locality_id'];
    $captains = get_captains_by_localities($locality_id);
    $locality_id_set = 1;
  }
  if(isset($attrs['captain_id']) && $attrs['captain_id'] !=''){
    $captain_id = $attrs['captain_id'];
    $captains = $captain_id;
    $captain_id_set = 1;
  }
  if(isset($attrs['lat_long']) && $attrs['lat_long'] !=''){
    $lat_long = $attrs['lat_long'];
    $lat_long_set = 1;
    $lat_long = $lat_long;

  }
  if($locality_id_set+$captain_id_set+$lat_long_set > 1){
      echo '<div id="message" class="error"><p>Something went wrong.</p></div>';
      return ;
  }
  $bg_color = '';
  $html  = '';
  $html .='<div id="brands_list" class="ms-3 fs-3 mb-3 font-family-karla font-weight-bold">'.$title.'</div>';
  
  $user_id = '';
  $delivery_date='';
  $super_captain = get_users( array( 'role__in' => array( 'super-captain' ) ) );
  if(!empty($super_captain)){
    $user_id =$super_captain[0]->ID;
        //display_brands_with_url_param($brand->brand_id,$locality_id,$user_id);
  }
  $box_start = '<div class="carousel overflow-hidden ">
      <div style="width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 position-absolute" style="background: '.$bg_color.'; width:100%; "></div>';// '';
       $box_end = '</div></div>'; //
          if(empty($captains) || $captains ==''){
             $brands_lat = get_object_by_location($lat_long,'brand',30);
             $brands = chotu_work_change_key($brands_lat,'reference_ID','brand_id');
            if(!empty($brands)){
              $html .= $box_start;
              $html .= chotu_work_make_brand_card($brands,'','',$locality_id,$user_id,'');
              $html .=  $box_end; 
            }else{
              $html .= ' <div class="ms-3 mb-3 text-center"><h3>Coming Soon</h3></div>';
            }
        }else{
          $brands = get_brands_by_captains($captains);
          if(!empty($brands)){
           $html .=  $box_start;
            $html .= chotu_work_make_brand_card($brands,'','',$locality_id,$user_id,'');
            $html .=  $box_end;     
          }
        }
      return $html;
}
add_shortcode('show_captains','chotu_work_get_captains_swipe');
function chotu_work_get_captains_swipe($attrs){
  $bg_color = '';
  $html ='';
   $locality_id_set =0; $brand_id_set =0; $lat_long_set =0;
  $locality_id='';
  $captain_id = '';
  $lat_long = '';
  $captains='';
  if(isset($attrs['title'])){
    $title = $attrs['title'];
  }
 
  if(isset($attrs['locality_id']) && $attrs['locality_id'] !=''){
    $locality_id = $attrs['locality_id'];
    $captains = get_captains_by_localities($locality_id);
    $locality_id_set = 1;
  }
  if(isset($attrs['brand_id']) && $attrs['brand_id'] !=''){
    $brand_id = $attrs['brand_id'];
    $captains = get_captains_by_brands($brand_id);
    $brand_id_set = 1;
  }
  if(isset($attrs['lat_long']) && $attrs['lat_long'] !=''){
    $lat_long = $attrs['lat_long'];
    $lat_long_set = 1;
    $captains = get_object_by_location($lat_long,'captain',30);
  }

  if($locality_id_set+$brand_id_set+$lat_long_set > 1){
      echo '<div id="message" class="error"><p>Something went wrong.</p></div>';
      return ;
  }
  $user_id = '';
  $delivery_date='';
  $super_captain = get_users( array( 'role__in' => array( 'super-captain' ) ) );
  if(!empty($super_captain)){
    $user_id =$super_captain[0]->ID;
        //display_brands_with_url_param($brand->brand_id,$locality_id,$user_id);
  }
  $box_start = '<div class="mt-5 ms-3 fs-3 mb-3 font-family-karla font-weight-bold">'.$title.'</div><div class="carousel overflow-hidden ">
      <div style=" width:100%" class="carousel-item flex-column d-flex justify-content-center align-items-center"><div class="bottom-0 position-absolute" style="background: '.$bg_color.'; width:100%; "></div>';// '';
       $box_end = '</div></div>';
  $html .=  $box_start;
  foreach ($captains as $key => $captain) {
    $captain_ID ='';
    if(isset($captain->reference_ID)){
      $captain_ID = $captain->reference_ID;
    }else{
        $captain_ID = $captain->captain_ID;
    }
    $user = get_user_by( 'id', $captain_ID );
    $attachment_id = get_the_author_meta('captain_banner_pic',$captain_ID );
    $captain_rating = get_the_author_meta('captain_rating',$captain_ID );
    $order_count = get_order_count_by_captain($captain_ID);
    $banner_image = get_custom_attachment_image($attachment_id);
    $footer_text = 'Rating: '.$captain_rating.' star | '.$order_count->count.' orders delivered';
    if(empty($banner_image) && $banner_image == ''){
      $banner_image = plugins_url().'/chotu_work/assets/images/teamscreen.png';
    }
    $url = get_author_posts_url($captain_ID).'?l='.$locality_id;
    $html .=chotu_work_make_card($banner_image,$user->display_name,$footer_text,'View',$url,'','captain');
  }
  $html .=  $box_end;
  return  $html;
}

function chotu_work_wp_nav_menu_args( $args = '' ) {
if( is_user_logged_in() ) { 
    $args['menu'] = 'logged-in';
} else { 
    $args['menu'] = 'logged-out';
} 
    return $args;
}
add_filter( 'wp_nav_menu_args', 'chotu_work_wp_nav_menu_args' );

add_action('after_setup_theme', 'chotu_work_remove_admin_bar');
function chotu_work_remove_admin_bar() {
  if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
  }
}
add_filter( 'logout_redirect', function() {
    return esc_url( home_url() );
} );

function chotu_work_make_card_add_locality($locality,$button_text,$button_link){
  $html='';
  $html .='<div class="card shadow mb-4 mx-auto" style="width: 80%; aspect-ratio:809/500;">
  <img class="card-img-top" style="aspect-ratio:3/1; object-fit:cover" src="'.plugins_url().'/chotu_work/assets/images/locality.png" alt="...">
  <!--button cmponent ->>>> giving border not nec.. -->
  <div class="card-body">
    <span class="card-title">Add Locality</span>';
    if($locality == 'locality'){
      $html .='<p class="card-text text-muted pt-2"><small></small>
    <a href="'.home_url().'/add-locality" class="btn btn-primary btn-sm border-0 position-absolute end-0 mx-3 ">Add My locality</a></p>';
    }
  $html .='</div></div>';
return $html;
}

add_action('wp_head','chotu_work_og_meta_details');
function chotu_work_og_meta_details(){
  if ( is_single() && 'locality' == get_post_type() ) {
    $locality_id = get_the_ID();
    $location = explode(",", get_post_meta($locality_id,'locality_lat_long',true)) ;

  ?>
   <meta property="og:title" content="<?php echo the_title()?>"/>
   <meta property="og:description" content="<?php echo get_post_meta($locality_id,'locality_description',true);?>"/>
   <meta property="og:url" content="<?php echo get_permalink($locality_id)?>"/>
   <meta property="og:image" content="<?php echo wp_get_attachment_image_url(get_post_meta($locality_id,'locality_square_pic',true));?>"/>
   <meta property="place:location:longitude" content = "<?php echo $location[0];?>">
   <meta property="place:location:latitude" content = "<?php echo $location[1] ?>">

  <?php }
  if(is_author()){
    global $wp;
    $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
    $user_id = $author->ID;
    if(in_array('captain',$author->roles)){
      $current_url = home_url( add_query_arg( array(), $wp->request ) );
      $location = explode(",", get_user_meta($user_id,'captain_lat_long',true));
      ?>
     <meta property="og:title" content="<?php echo $author->user_nicename.' '.get_user_meta($user_id,'first_name',true)?>"/>
     <meta property="og:description" content="<?php echo $author->user_description;?>"/>
     <meta property="og:url" content="<?php echo $current_url?>"/>
     <meta property="og:image" content="<?php echo wp_get_attachment_image_url(get_user_meta($user_id,'captain_square_pic',true));?>"/>
     <meta property="place:location:longitude" content = "<?php echo $location[0];?>">
     <meta property="place:location:latitude" content = "<?php echo $location[1] ?>">
    <?php }
    }
}

function chotu_work_change_key( $array, $old_key, $new_key ) {
    // $data =  new stdClass();
  $data = array();
    foreach ($array as $key => $value) {
      $obj = [];
      $obj[$new_key] = $value->$old_key; 
      $data[$key] = (object) $obj;
    }
    return $data;
  } 
add_action('wp_logout','chotu_work_auto_redirect_after_logout');

function chotu_work_auto_redirect_after_logout(){
  wp_safe_redirect( home_url() );
  exit;
}
function change_menu($items){
  foreach($items as $item){
    if( $item->title == "Logout"){
         $item->url = $item->url . "&_wpnonce=" . wp_create_nonce( 'log-out' );
    }
  }
  return $items;

}
add_filter('wp_nav_menu_objects', 'change_menu');