<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_woo_network {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Chotu_woo_network_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CHOTU_WOO_NETWORK_VERSION' ) ) {
			$this->version = CHOTU_WOO_NETWORK_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'chotu_woo_network';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Chotu_woo_network_Loader. Orchestrates the hooks of the plugin.
	 * - Chotu_woo_network_i18n. Defines internationalization functionality.
	 * - Chotu_woo_network_Admin. Defines all hooks for the admin area.
	 * - Chotu_woo_network_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-chotu_woo_network-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-chotu_woo_network-i18n.php';
		
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-chotu_woo_network-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'functions.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-chotu_woo_network-public.php';

		$this->loader = new Chotu_woo_network_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Chotu_woo_network_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Chotu_woo_network_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Chotu_woo_network_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'woocommerce_coupon_options',$plugin_admin, 'woo_newtwork_add_coupon_captain_id_field', 10 );
		$this->loader->add_action( 'woocommerce_coupon_options_save',$plugin_admin,'woo_newtwork_save_coupon_captain_id_field', 10, 2 );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Chotu_woo_network_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'init', $plugin_public, 'chotu_woo_network_check_url_param' );
		$this->loader->add_action('woocommerce_checkout_order_processed',$plugin_public,'chotu_woo_network_send_success_order_data_to_api');
		$this->loader->add_action('woocommerce_thankyou', $plugin_public, 'chotu_woo_network_update_customer_woocommerce_order');
		$this->loader->add_action( 'woocommerce_payment_complete', $plugin_public,  'chotu_woo_network_update_payment_complete' );
		$this->loader->add_action('woocommerce_thankyou',  $plugin_public,'chotu_woo_network_update_order_status_to_customer_order');
		$this->loader->add_action('woocommerce_order_status_pending',  $plugin_public,'chotu_woo_network_update_order_status_to_customer_order');
		$this->loader->add_action('woocommerce_order_status_on-hold',  $plugin_public,'chotu_woo_network_update_order_status_to_customer_order');
		$this->loader->add_action('woocommerce_order_status_processing',  $plugin_public,'chotu_woo_network_update_order_status_to_customer_order');
		$this->loader->add_action('woocommerce_order_status_completed',  $plugin_public,'chotu_woo_network_update_order_status_to_customer_order');
		
		$this->loader->add_action( 'wp_ajax_nopriv_set_session_of_lcd', $plugin_public, 'chotu_woo_network_set_session_of_lcd' );
		$this->loader->add_action( 'wp_ajax_set_session_of_lcd', $plugin_public, 'chotu_woo_network_set_session_of_lcd' );
		$this->loader->add_action('wp_footer',$plugin_public,'chotu_woo_network_add_script_open_modal');


	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Chotu_woo_network_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
