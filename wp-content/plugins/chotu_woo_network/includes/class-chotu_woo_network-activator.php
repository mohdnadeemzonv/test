<?php

/**
 * Fired during plugin activation
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/includes
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_woo_network_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
