<?php 
// function chotu_woo_network_validate_add_cart_item( $passed, $product_id, $quantity, $variation_id = '', $variations= '' ) {
// 	if ( WC()->cart->get_cart_contents_count() == 0 ) {
// 	    if ( !WC()->session->set('l') ||  !WC()->session->set('c') || !WC()->session->set('d')){
//         echo 'l'.WC()->session->set('l');
//         echo 'c'.WC()->session->set('c');
//         echo 'd'.WC()->session->set('d');
//         dd('yes');
// 	        $passed = false;
// 	        wc_add_notice( __( '<button type="button" class="btn btn-info btn-lg locality-modal-toggle">Select Your Locality	</button>', 'textdomain' ), 'error' );
// 	    }
// 	}
//     return $passed;

// }
// add_filter( 'woocommerce_add_to_cart_validation', 'chotu_woo_network_validate_add_cart_item', 10, 5 );

add_action('storefront_before_header','chotu_woo_network_add_captain_banner_image_header');
function chotu_woo_network_add_captain_banner_image_header(){
global $wpdb;
    $url='';
    //echo '<a href="'.network_home_url(0).'">Home</a>';
    $blog_id = get_current_blog_id();
    if( is_shop() ){
      if (isset($_SESSION['l']) && isset($_SESSION['c']) && isset($_SESSION['d'])){
        if($_SESSION['l'] !=''){
          $title = get_localitity_by_id('locality',$_SESSION['l']);
          $horizontal_pic = get_custom_post_meta($_SESSION['l'],'locality_banner_pic');
          if(isset($horizontal_pic) && $horizontal_pic !=''){
            $url = get_custom_attachment_image($horizontal_pic,'banner-image');
          }else{
            $url = home_url().'/wp-content/uploads/2021/07/9e69a59c38ca00c68311d9ecf19eb078.jpg';
          }
          //echo '<div id="header-image" '.$_SESSION['l'].'>'.$title->post_title.'<img width="100%" height="150px"  alt="" class="header-image" src="'.$url.'">Delivered only to: <strong>'.$title->post_title.'</strong></div>';
         
          }
        }
    }
}
add_action('storefront_content_top','chotu_woo_network_display_next_delivery_date');
function chotu_woo_network_display_next_delivery_date(){
  if ( !class_exists( 'woocommerce' ) ) { return false; }
  global $wpdb;
    $title ='';
    $blog_id = get_current_blog_id();
    $delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,'','');
   
      $delivery_date = '';
      if(!empty($delivery_order_cycle)){
        $delivery_date = $delivery_order_cycle->delivery_date;
        $order_before_time = $delivery_order_cycle->order_before_time;
        if(isset($_SESSION['l']) &&  $_SESSION['l']!=''){
          $locality = get_localitity_by_id('locality',$_SESSION['l']);
          $title = 'Hello '.$locality->post_title.',';
        }
        //echo '<a href="javascript:;" data-toggle="modal" data-target="#locality_select" class="btn login font-family-inter font-weight-normal btn-primary">Change Locality</a>';
        echo '<marquee width="100%" direction="left">'. $title.' Next Delivery Date: '.date('j M, D',strtotime($delivery_date)).'</marquee>';
   }
}

add_filter( 'woocommerce_checkout_fields' , 'chotu_work_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function chotu_work_override_checkout_fields( $fields ) {
 // unset($fields['billing']['billing_first_name']);
 unset($fields['billing']['billing_last_name']);
 unset($fields['billing']['billing_company']);
 unset($fields['billing']['billing_address_1']);
 unset($fields['billing']['billing_address_2']);
 unset($fields['billing']['billing_city']);
 unset($fields['billing']['billing_postcode']);
 unset($fields['billing']['billing_state']);
 unset($fields['billing']['billing_country']);
 unset($fields['billing']['billing_email']);
 // unset($fields['billing']['billing_phone']);

 unset($fields['shipping']['shipping_first_name']);
 unset($fields['shipping']['shipping_last_name']);
 unset($fields['shipping']['shipping_company']);
 unset($fields['shipping']['shipping_address_1']);
 unset($fields['shipping']['shipping_address_2']);
 unset($fields['shipping']['shipping_city']);
 unset($fields['shipping']['shipping_postcode']);
 unset($fields['shipping']['shipping_state']);
 unset($fields['shipping']['shipping_email']);
 unset($fields['shipping']['shipping_phone']);
  $fields['billing']['billing_first_name']['label'] = 'Name';
 return $fields;
}


add_filter( 'woocommerce_after_checkout_billing_form' , 'chotu_woo_network_override_checkout_fields' );
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
// Our hooked in function - $fields is passed via the filter!
function chotu_woo_network_override_checkout_fields( $checkout ) {
  if(isset($_SESSION['c'])){
  $gated_locality = get_custom_post_meta($_SESSION['l'],'locality_address_syntax',true);
  $address_syntax = json_decode($gated_locality,true);
  //   if(is_user_logged_in()){
  //   $user_id = get_current_user_id();
  //   $user_role = unserialize(get_custom_user_meta($user_id,'wp_capabilities'));
  //   $user = new WP_User( $user_id );
  //   if (in_array( 'captain',$user_role )) {
  //       woocommerce_form_field( 'customer_billing_phone', array(
  //           'type'  => 'text',
  //           'required' => true,
  //           'label' => 'Customer Phone Number',
  //           'placeholder' => '',
  //           'class' => array('custom-field form-row-wide'),                          
  //       ) );
  //   }
  // }
  if(!empty($address_syntax)){
    $i=1;
    foreach ($address_syntax as $key => $value) {
      $name = strtolower(str_replace(' ', '_', $value['level']));
      $name = strtolower(str_replace('.', '', $name));
      woocommerce_form_field( 'level_label_'.$i, array(
            'type'  => 'hidden',
            'placeholder' => $value['level'],
            'class' => array('custom-field form-row-wide'),
            'label' => ucfirst($value['level']),                           
            'default' => ucfirst($value['level']),                           
        ), $checkout->get_value( $value['level']) );
      if(trim($value['type']) == 'text'){
         woocommerce_form_field( 'level_'.$i, array(
            'type'  => 'text',
            'required' => true,
            'placeholder' => $value['level'],
            'class' => array('custom-field form-row-wide'),
            'label' => ucfirst($value['level']),                           
        ), $checkout->get_value(  'level_'.$i) );
      }
        if(trim($value['type']) == 'option'){
          $option = explode(',', $value['options']);
          $options = convert_index_array_to_associative($option);
          woocommerce_form_field( 'level_'.$i, array(
            'type'          => 'select',
            'class'         => array($name),
            'label'         =>__(ucfirst($value['level']), 'woocommerce'),
            'required'    => true,
            'options'     => $options,
            ),$checkout->get_value('level_'.$i ));
          }
          $i++;
        }
        woocommerce_form_field( 'billing_country', array(
            'type'  => 'hidden',
            'required' => true,
            'placeholder' => '',
            'class' => array('custom-field form-row-wide'),                          
        ), $checkout->get_value('IN') );

      }
    }
  }
  

add_action( 'woocommerce_after_checkout_billing_form', 'chotu_woo_network_odisplay_extra_fields_after_billing_address' , 10, 1 );

function chotu_woo_network_odisplay_extra_fields_after_billing_address() {
  if(isset($_SESSION['l'])){
    global $wpdb;
    $blog_id = get_current_blog_id();
    $user = new WP_User($_SESSION['c']);
    $phone_number = get_custom_user_meta($_SESSION['c'],'phone_number');
    $location_table_data = get_location_object_data_by_type($blog_id,'brand');
    $captain_table_data = get_location_object_data_by_type($_SESSION['c'],'captain');
    if(!empty($location_table_data)){
      _e( "<span class='address_cl'>Locality Address :</span> ".$location_table_data->full_address.', '.$location_table_data->pincode);
    echo '<br>';
  _e( "<span class='address_cl'>Pickup Address :</span> ".$captain_table_data->full_address.', '.$captain_table_data->pincode );
    }
    echo '<br>';
    _e( "<span class='address_cl captain_name' title=".$user->display_name."><strong>Delivered By :</strong></span> ".$user->display_name.' / '. $phone_number);
    }
  }
add_action( 'woocommerce_checkout_update_order_meta', 'chotu_woo_network_field_update_order_meta' );
function chotu_woo_network_field_update_order_meta( $order_id ) {
  
   $order = wc_get_order( $order_id );
   $total = $order->get_total();
 
    update_post_meta($order_id,'level_label_1',$_POST['level_label_1']);
    if(!empty($_POST['level_label_2'])){
       update_post_meta($order_id,'level_label_2',$_POST['level_label_2']);
    }else{
       update_post_meta($order_id,'level_label_2','N/A');
    }
    if(!empty($_POST['level_label_3'])){
       update_post_meta($order_id,'level_label_3',$_POST['level_label_3']);
    }else{
       update_post_meta($order_id,'level_label_3','N/A');
    }
    /* update values */
    update_post_meta($order_id,'level_1',$_POST['level_1']);
    if(!empty($_POST['level_2'])){
       update_post_meta($order_id,'level_2',$_POST['level_2']);
    }else{
       update_post_meta($order_id,'level_2','N/A');
    }
    if(!empty($_POST['level_3'])){
       update_post_meta($order_id,'level_3',$_POST['level_3']);
    }else{
       update_post_meta($order_id,'level_3','N/A');
    }
    if($_SESSION['l']){
      update_post_meta( $order_id, 'locality_id', $_SESSION['l']);
    }
    if($_SESSION['c']){
      update_post_meta( $order_id, 'captain_id', $_SESSION['c']);
    }
    if($_SESSION['d']){
      update_post_meta( $order_id, 'date_of_delivery', $_SESSION['d']);
    }
    // if(isset($_POST['customer_billing_phone'])){
    //   update_post_meta( $order_id, 'customer_billing_phone', $_POST['customer_billing_phone']);
    // }
    
 }

add_action('woocommerce_before_thankyou','chotu_woo_network_share_button_on_order_recieve_page');
function chotu_woo_network_share_button_on_order_recieve_page($order_id){
  echo '<a class="btn btn-success" href="https://api.whatsapp.com/send/?phone&text='.home_url().'" target="_blank">Share</a>';
}

add_action('woocommerce_after_order_details','chotu_woo_network_display_all_locality_brands');
function chotu_woo_network_display_all_locality_brands($order){
  echo '<header class="entry-header"><h1 class="entry-title">Other Brands</h1></header>';
  global $woocommerce, $post;
  $order_id = $order->get_id();
  $captain_id = get_post_meta($order_id,'captain_id',true);
  $locality_id = get_post_meta($order_id,'locality_id',true);
  echo do_shortcode('[card_by_captain locality_id="'.$locality_id.'" captain_id="'.$captain_id.'" ]');
}

add_shortcode('card_by_captain','chotu_woo_network_get_all_pin_by_team');
function chotu_woo_network_get_all_pin_by_team($_atts){
  global $wpdb,$post;
  global $wpdb;
  $current_blog_id = get_current_blog_id();
  $defaults = array(
        'captain_id' => '',
        'locality_id' => '',
    );
  $atts = shortcode_atts( $defaults, $_atts );
  $atts['captain_id'] = absint( $atts['captain_id'] );
  $atts['locality_id'] = absint( $atts['locality_id'] );
    if(!empty($atts['captain_id'])){
      $brands = get_brands_by_captains($atts['captain_id']);
       if(!empty($brands)){
        foreach ($brands as $key => $brand) {
            $blog = get_blog_details($brand->brand_id);
            if($current_blog_id !=$brand->brand_id){
              $url = display_brands_with_url_param($brand->brand_id,$atts['locality_id'],$atts['captain_id']);
              // $footer_text = '07 Aug, Sat | min: 40 orders';
              $button_text = 'View';
              $banner_image = '';
              $banner_pic_of_brand = unserialize(get_banner_pic_of_brand($brand->brand_id,'banner_pic'));
              if(!empty($banner_pic_of_brand)){
                $banner_image = $banner_pic_of_brand['banner_pic'];
              }
              echo chotu_work_make_card($banner_image,$blog->blogname,$brand->brand_id,$button_text,$url,'','brand');
            }
            
          }
            
        }
    }
    
  }

function chotu_woo_network_get_users_details_by_user_role($user_role){
  global $wpdb;
  $wp_postmeta = "wp_postmeta";
  if($user_role == 'super-captain'){
      return $wpdb->get_row("SELECT wp_users.ID, wp_users.user_nicename FROM wp_users INNER JOIN wp_usermeta 
  ON wp_users.ID = wp_usermeta.user_id WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%$user_role%' ORDER BY wp_users.user_nicename"); 
  }
  return $wpdb->get_results("SELECT wp_users.ID, wp_users.user_nicename FROM wp_users INNER JOIN wp_usermeta 
ON wp_users.ID = wp_usermeta.user_id WHERE wp_usermeta.meta_key = 'wp_capabilities' AND wp_usermeta.meta_value LIKE '%$user_role%' ORDER BY wp_users.user_nicename"); 
}

// define the woocommerce_coupon_is_valid callback 
function chotu_woo_network_filter_woocommerce_coupon_is_valid( $valid, $instance ) { 
    // make filter magic happen here... 
    $coupon_post_obj = get_page_by_title($instance->code, OBJECT, 'shop_coupon');
    if(!empty($coupon_post_obj)){
        $captain_id = get_post_meta($coupon_post_obj->ID,'captain_id',true);
        if($_SESSION['c'] != $captain_id){
          if($captain_id == 0){
             $valid = 1;
            }else{
              $valid = 0;
            }
        }
      return $valid;
  }
}
         
// add the filter 
add_filter( 'woocommerce_coupon_is_valid', 'chotu_woo_network_filter_woocommerce_coupon_is_valid', 10, 2 );

function convert_index_array_to_associative($options){
  if(is_array($options)){
    $aa = array();
    foreach ($options as $key => $option) {
       $aa[$option] = $option;
    }
    return $aa;
  }
}


add_action('init', 'chotu_woo_network_start_session', 1);
function chotu_woo_network_start_session() {
if(!session_id()) {
  session_start();
}
if(isset($_SESSION['expired'])){
  if(time() > $_SESSION['expired'])
    {  
      $_SESSION['l'] ='';
      $_SESSION['c'] ='';
      $_SESSION['d'] ='';
      $_SESSION['expired'] = '';
    }
}
}
function chotu_woo_network_check_url_param_validation($locality_id,$captain_id,$date_of_delivery){
  global $wpdb;
  $blog_id = get_current_blog_id();
  if(isset($locality_id) && isset($captain_id) && isset($date_of_delivery)){
    $delivery_order_cycle = get_delivery_date_and_cut_off_time($blog_id,$locality_id,$captain_id);
    $super_captain = chotu_woo_network_get_users_details_by_user_role('super-captain');
    $captain_brand = chotu_woo_network_check_captain_brand_mapping($blog_id,$captain_id);
    $captain_locality = chotu_woo_network_check_captain_locality_mapping($captain_id,$locality_id);
    if(!empty($delivery_order_cycle)){
      if(date('Ymd',strtotime($delivery_order_cycle->delivery_date)) != $date_of_delivery){
        return false;
      }
    }
    if(!empty($super_captain)){
        if($super_captain->ID == $captain_id){
          if(chotu_woo_network_check_locality_exist($locality_id) < 1){
            return false;
        }else{
          return true;
        }
      }
    }

  if($captain_brand < 1  || $captain_locality < 1 ){
    return false;
  }
  }
  return true;
}
function chotu_woo_network_check_captain_brand_mapping($blog_id,$captain_id){
  global $wpdb;
  $captain_brand = "chotu_captain_brand";
  return $wpdb->get_var("SELECT COUNT(*) FROM $captain_brand WHERE brand_id = '$blog_id' AND captain_ID='$captain_id'");
}
function chotu_woo_network_check_captain_locality_mapping($captain_id,$locality_id){
  global $wpdb;
  $chotu_captain_locality = "chotu_captain_locality";
  return $wpdb->get_var("SELECT COUNT(*) FROM $chotu_captain_locality WHERE locality_id = '$locality_id' AND captain_ID='$captain_id'");
}
function chotu_woo_network_check_locality_exist($locality_id){
  global $wpdb;
  $wp_posts = "wp_posts";
  return $wpdb->get_var("SELECT COUNT(*) FROM $wp_posts WHERE ID = '$locality_id' AND post_type='locality'");
}

add_filter('storefront_handheld_footer_bar_links','chotu_woo_network_change_my_account_menu_link_in_footer',10,1);
function chotu_woo_network_change_my_account_menu_link_in_footer($links){
  if(!empty($links)){
    foreach ($links as $key => $link) {
      if($key == 'my-account'){

        $links[$key]['priority']=20;
        $links[$key]['callback']='chotu_storefront_handheld_footer_bar_account_link';
      }
      if($key == 'search'){
        $links[$key]['priority']=10;
      }
    }
  }
  krsort($links);
return $links;
}
function chotu_storefront_handheld_footer_bar_account_link(){
  echo'<style>.storefront-handheld-footer-bar ul li.my-account>a::before {
      content: "" !important;
      background-image: url("'.network_home_url(0).'/wp-content/uploads/2021/08/Webp.net-resizeimage.png");
      background-position-x: center;
      background-repeat: repeat-y;
  }</style>';
  echo '<a href="'.network_home_url(0).'">My Account</a>';
} 
add_filter('woocommerce_available_payment_gateways','chotu_woo_network_filter_gateways',1);
function chotu_woo_network_filter_gateways($gateways){
    global $woocommerce;
    $blog_id = get_current_blog_id();
    if(isset($_SESSION['l']) && isset($_SESSION['c']) && $_SESSION['c'] !='' && $_SESSION['l'] !=''){
      $b_mycred_balance = get_site_meta($blog_id,'mycred_balance','single');
      $b_cod = get_site_meta($blog_id,'cod','single');
      $b_prepaid_razorpay = get_site_meta($blog_id,'prepaid_razorpay','single');

      $l_mycred_balance = get_custom_post_meta($_SESSION['l'],'locality_payment_type_mycred_balance');
      $l_payment_type_cod = get_custom_post_meta($_SESSION['l'],'locality_payment_type_cod');
      $l_prepaid_razorpay = get_custom_post_meta($_SESSION['l'],'locality_payment_type_prepaid_razorpay');

      $c_mycred_balance = get_custom_user_meta($_SESSION['c'],'payment_type_mycred_balance');
      $c_payment_type_cod = get_custom_user_meta($_SESSION['c'],'payment_type_cod');
      $c_prepaid_razorpay = get_custom_user_meta($_SESSION['c'],'payment_type_prepaid_razorpay');

        if($b_mycred_balance == 0 || $l_mycred_balance == 0 || $c_mycred_balance == 0){
          unset($gateways['mycred']);
        }
        if($b_prepaid_razorpay == 0 || $l_prepaid_razorpay == 0 || $c_prepaid_razorpay == 0){
          unset($gateways['razorpay']);
        }
        if($b_cod == 0 || $l_payment_type_cod == 0 || $c_payment_type_cod == 0){
          unset($gateways['cod']);
        }
    }

    
    return $gateways;
}