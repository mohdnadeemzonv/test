<?php
/*Disallow virtual and downloadable products for network sites with wooCommerce*/
add_filter( 'product_type_options', function( $options ) {
  if( isset( $options[ 'virtual' ] ) ) {
    unset( $options[ 'virtual' ] );
  }
   // remove "Downloadable" checkbox
  if( isset( $options[ 'downloadable' ] ) ) {
    unset( $options[ 'downloadable' ] );
  }
  return $options;
} );
/*Disallow external products for network sites with wooCommerce*/
add_filter( 'product_type_selector', 'chotu_work_remove_external_product' );

function chotu_work_remove_external_product ( $type ) {
  // Key should be exactly the same as in the class product_type
  unset($type['external']);
  return $type;
}

/*
add_action('admin_footer','disabled_home_page_settings');
function disabled_home_page_settings(){
  if(get_current_blog_id() != 1){
    echo '<script>setTimeout(function() { jQuery("#page_on_front").attr("disabled",true)}, 200);</script>';  
   
  }
}
*/
