<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.zonvoir.com
 * @since      1.0.0
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chotu_woo_network
 * @subpackage Chotu_woo_network/admin
 * @author     Mohd Nadeem <mohdnadeemzonv@gmail.com>
 */
class Chotu_woo_network_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_woo_network_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_woo_network_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chotu_woo_network-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chotu_woo_network_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chotu_woo_network_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chotu_woo_network-admin.js', array( 'jquery' ), $this->version, false );

	}
	// Add a custom field to Admin coupon settings pages
	
	public function woo_newtwork_add_coupon_captain_id_field() {
	    $captain_id = get_post_meta(get_the_ID(), 'captain_id', true);
	     woocommerce_wp_select( array(
	        'id'      => 'captain_id',
	        'label'   => __( 'Captain ', 'woocommerce' ),
	        'options' =>  $this->get_all_captains(), //this is where I am having trouble
	        'value'   => $captain_id,
	    ) );
		}

	// Save the custom field value from Admin coupon settings pages
	
	public function woo_newtwork_save_coupon_captain_id_field( $post_id, $coupon ) {
	    if( isset( $_POST['captain_id'] ) ) {
	        $coupon->update_meta_data( 'captain_id', sanitize_text_field( $_POST['captain_id'] ) );
	        $coupon->save();
	    }
	}
	public function get_all_captains(){
		$all_captain = chotu_woo_network_get_users_details_by_user_role('captain');
		$options = array('0'=>'');
		if(!empty($all_captain)){
			foreach ($all_captain as $key => $captain) {
				$phone_number = get_custom_user_meta($captain->ID,'phone_number');
				$options[$captain->ID] = $captain->user_nicename.' '.$phone_number;
			}
		}
		return $options;
		}

}
