<?php
/**
 * The template for displaying Author Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); 

global $post;
$author_id = get_the_ID();

// to get  nicename
$nicename =  get_the_author_meta( 'nicename', $author_id );
?>
<h1><?php echo $nicename;?></h1>

<?php 
// to get  email
$email = get_the_author_meta( 'email', $author_id );
?>
<h2><?php echo $email;?></h2>
<?php
 
// to get  url
echo get_the_author_meta( 'url', $author_id );
 
// to get  status
echo get_the_author_meta( 'status', $author_id );
 get_footer(); ?>