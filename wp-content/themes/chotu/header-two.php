<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chotu
 */
?>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <?php wp_head(); ?>
  <link href="https://fonts.googleapis.com/css?family=Inter&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.css" />
  <!-- <link rel="stylesheet" href="/path/to/flickity.css" media="screen" /> -->
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" />

  <!-- My Stylesheets -->
  <!-- <link href="./home.css" rel="stylesheet" /> -->
  <?php
    $localize = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'redirecturl' => '',
            'UserId' => 0,
            'loadingmessage' => __('please wait...', ''),
        );
  ?>

</head>
<body>
  <section class="mobileView m-auto">
  <?php
    do_action('chotu_before_header');
  ?>

