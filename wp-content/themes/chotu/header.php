<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
  

	<?php wp_head();?>
  <link href="https://fonts.googleapis.com/css?family=Inter&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.css" />
  <!-- <link rel="stylesheet" href="/path/to/flickity.css" media="screen" /> -->
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" />
  <!-- My Stylesheets -->
  <!-- <link href="./home.css" rel="stylesheet" /> -->
  <?php
    $localize = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'redirecturl' => '',
            'UserId' => 0,
            'loadingmessage' => __('please wait...', ''),
        );
      ?>
</head>
<body>
  <section class="mobileView m-auto">
  <?php
  	do_action('chotu_before_header');
  ?>
<nav class="navbar navbar-dark sticky-top">
      <!-- toggler button -->

      <button class="navbar-toggler p-0 px-2" type="button" data-toggle="collapse" data-target="#Toggler"
        aria-controls="Toggler" aria-expanded="true" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "><img src="<?php echo plugins_url()?>/chotu_work/assets/images/drawer.png" alt="" /></span>
      </button>
      <?php 
      the_custom_logo();
      if (!is_user_logged_in()) {
      ?>
      <a href="javascript:;" data-toggle="modal" data-target="#loginModal" class="login font-family-inter font-weight-normal px-3">Signup</a>
      <?php 
        }else{
          //echo '<a href="javascript:;">Login</a>';
          $balance = get_user_meta(get_current_user_id(),'mycred_default',true);
          echo '<span><strong>Balance:</strong> ₹'.$balance.'</span>';
        }
      ?>
      <div class="collapse bg-white navbar-collapse" id="Toggler">
        <?php
          wp_nav_menu(
            array(
              'theme_location' => 'menu-1',
              'menu_id'        => 'primary-menu',
              'menu_class' => 'navbar-nav mx-auto text-center',    
            )
          );
          ?>
      </div>
    </nav>
    <!-- Heading Description -->
    <!-- Heading Description -->
    <?php 
      if(is_home() || is_front_page()){
        ?>
    <div style="background-color: #f9f9f9; height:11rem;"  class="text-center font-family-karla mb-5">
      <p class="font-weight-bold fs-3"><?php echo get_option('blogdescription'); ?></p>
      <p class="font-family-inter"><?php echo get_post_meta(get_the_ID(),'top_tags',true)?></p>
      <!-- search Bar -->     
    <?php 
     echo do_shortcode('[wpdreams_ajaxsearchlite]');
      // echo do_shortcode('[ivory-search id="274" title="Default Search Form"]');
    } ?>
  </div>
    <?php
    
    do_action('chotu_after_main_content');