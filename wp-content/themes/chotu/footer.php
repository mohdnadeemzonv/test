<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package chotu
 */
do_action('chotu_before_footer');
?>

<footer class="py-4" style="background-color: #f9f9f9">
      <div class="container sticky-bottom">
        <nav class="d-flex justify-content-center flex-column align-items-center pt-5">
          <img src="<?php echo plugins_url()?>/chotu_work/assets/images/chotu.svg" alt="Logo" />
           <div class="mt-3">
           	<p class=" text-color-dark justify-content-center">
	          <?php
	          $menu_list='';
	          $locations = get_nav_menu_locations();
	          $menu_items = wp_get_nav_menu_items($locations['footer_menu']);
	          if(!empty($menu_items)){
	          	$lastElement = end($menu_items);
	          	foreach ($menu_items as $key => $menu_item) {
				$title = $menu_item->title;
				$url = $menu_item->url;
				if($menu_item != $lastElement) {
					$menu_list .= "\t\t\t\t\t". '<a class="text-reset" href="'. $url .'">&nbsp;'. $title .'&nbsp;</a>|' ."\n";
				}else{
					$menu_list .= "\t\t\t\t\t". '<a class="text-reset" href="'. $url .'">&nbsp;'. $title .'&nbsp;</a>' ."\n";
				}
				
				}
				echo $menu_list;
	          }
	          
	          ?>
      	</p>
          </div>
        </nav>
      </div>
   </footer>
    
    <!-- BOTTOM COPYRIGHT FOOTER -->
    <nav class="navbar navbar-expand-lg" style="background-color: #0e4c75">
      <div class="container-fluid justify-content-center">
        <a class="navbar-brand" href="#"
          style="font-family: inter; font-size:12px;font-weight: medium; color: #ffffff">Copyright &copy; 2021 Chotu</a>
      </div>
    </nav>
  
  </section>
  <?php wp_footer()?>
  
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.2/flickity.pkgd.min.js">
  </script>
  
  <!-- <script src="/path/to/flickity.pkgd.min.js"></script> -->
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script>
$(document).ready(function(){
	$(document).on('click','.signup-tab',function(e){
		e.preventDefault();
	    $('#signup-taba').tab('show');
	});	
	
	$(document).on('click','.signin-tab',function(e){
	   	e.preventDefault();
	    $('#signin-taba').tab('show');
	});
	    	
	$(document).on('click','.forgetpass-tab',function(e){
	 	e.preventDefault();
	   	$('#forgetpass-taba').tab('show');
	});
});	

</script>
<script type="text/javascript">
  	var x = document.getElementById("demo");
	var latitude;
	var longitude;
	function getLocation() {
	  if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(showPosition);
	  } else { 
	    x.innerHTML = "Geolocation is not supported by this browser.";
	  }
	}

	function showPosition(position) {
	console.log(position);
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	get_details_by_lat_long(latitude,longitude);
	}

	function get_details_by_lat_long(latitude,longitude){
		$.ajax({
             type : "POST",
             url : idehweb_lwp.ajaxurl,
             data : {'latitude':latitude,'longitude':longitude,action: "get_object_by_actual_location"},
             success: function(response) {
             	location.reload();
             	console.log(response);
            }
        });
	}
  </script>
</body>

</html>
